<?php

namespace Tests\Unit;

use App\User;
use App\Forum;
use Auth;
use Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ForumTest extends TestCase
{
    use WithFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testViewForum()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('GET', '/get-forum');
        $response->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateForum()
    {
        $response = $this->json('POST', '/forum',
            ['forum' => $this->faker->firstName(),
             'scholarship_id' => '1']);

        $response->assertStatus(200);
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateForumfailed()
    {
        $response = $this->json('POST', '/forum',
            ['scholarship_id' => '1']);

            $response->assertJsonValidationErrors(['name']);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDeleteChat()
    {
        $user = User::first();
        Auth::login($user);
        $id_forum = Forum::get()->random()->id;

        $response = $this->json('delete', '/forum/'.$id_forum);

        $response->assertStatus(200);

        $this->assertTrue(true);

    }

}

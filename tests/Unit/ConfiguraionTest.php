<?php

namespace Tests\Unit;

use App\User;
use Auth;
use Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ConfiguraionTest extends TestCase
{
    use WithFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testViewConfiguration()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('GET', '/get-configurations');
        $response->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testEditConfiguration()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('put', '/configuration/2',
            ['name' => $this->faker->firstName(),
             'value' => 0.94,
             'description' => $this->faker->text(),
             'of' => 'threshold',
             ]);

        $response->assertStatus(200);
        // $this->assertTrue(true);
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testEditConfigurationFail()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('put', '/configuration/2',
            [
                // 'name' => $this->faker->firstName(),
             'description' => $this->faker->text(),
            //  'of' => 'threshold',
            'value' => $this->faker->text(),

             ]);

        $response->assertJsonValidationErrors(['name']);
        $this->assertTrue(true);
    }
}

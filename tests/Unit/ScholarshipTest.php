<?php

namespace Tests\Unit;

use App\User;
use App\Scholarship;
use Auth;
use Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ScholarshipTest extends TestCase
{
    use WithFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testViewScholarship()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('GET', '/get-scholarships');
        $response->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateScholarship()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('post', '/scholarship',
            ['name' => $this->faker->firstName(),
             'uri' => $this->faker->text(),
             'picture' => '1',
             'website_id' => '1',
             'description' => '1',
             ]);

        $response->assertStatus(200);
        // $this->assertTrue(true);

    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateScholarshipFail()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('post', '/scholarship',
            ['name' => $this->faker->firstName(),
             'picture' => '1',
             'website_id' => '1',
             'description' => '1',
             ]);

        $response->assertJsonValidationErrors(['uri']);
        // $this->assertTrue(true);

    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDeleteScholarship()
    {
        $user = User::first();
        Auth::login($user);
        $id_scholarship = Scholarship::get()->random()->id;


        $response = $this->json('delete', '/scholarship/'.$id_scholarship);

        $response->assertStatus(200);

        // $this->assertTrue(true);

    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testEditScholarship()
    {
        $user = User::first();
        Auth::login($user);
        $id_scholarship = Scholarship::get()->random()->id;

        $response = $this->json('put', '/scholarship/'.$id_scholarship,
            ['name' => $this->faker->firstName(),
             'uri' => $this->faker->text(),
             'picture' => 'picture',
             'website_id' => '1',
             'description' => '1',
             ]);

        $response->assertStatus(200);
        // $this->assertTrue(true);
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testEditScholarshipFail()
    {
        $user = User::first();
        Auth::login($user);
        $id_scholarship = Scholarship::get()->random()->id;

        $response = $this->json('put', '/scholarship/'.$id_scholarship,
            ['name' => $this->faker->firstName(),
             'picture' => 'picture',
             'website_id' => '1',
             'description' => '1',
             ]);

        $response->assertJsonValidationErrors(['uri']);
    }



}

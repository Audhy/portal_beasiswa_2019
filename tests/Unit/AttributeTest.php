<?php

namespace Tests\Unit;

use App\User;
use App\Scholarship_attribute;
use Auth;
use Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AttributeTest extends TestCase
{
    use WithFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testViewAttribute()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('GET', '/get-scholarshipattribute');
        $response->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateAttribute()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('POST', '/scholarship-attribute',
            ['website' => '6',
             'name' => $this->faker->text(),
             'url' => '1',
             'attribute1' =>$this->faker->text(),
             'attribute2' =>$this->faker->text(),
             'title' =>$this->faker->text(),
             'description' =>$this->faker->text(),
             'picture' =>$this->faker->text(),
             'registration' =>$this->faker->text(),
             'period' =>$this->faker->text(),
             ]);

        $response->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateAttributeFail()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('POST', '/scholarship-attribute',
            ['website' => '6',
             'name' => $this->faker->text(),
             'attribute1' =>$this->faker->text(),
             'attribute2' =>$this->faker->text(),
             'title' =>$this->faker->text(),
             'description' =>$this->faker->text(),
             'picture' =>$this->faker->text(),
             'registration' =>$this->faker->text(),
             'period' =>$this->faker->text(),
             ]);

        $response->assertJsonValidationErrors(['url']);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDeteleteAttribute()
    {
        $user = User::first();
        Auth::login($user);
        $id_attribute = Scholarship_attribute::get()->random()->id;

        $response = $this->json('delete', '/scholarship-attribute/'.$id_attribute);

        $response->assertStatus(200);
        // $this->assertTrue(true);

    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testEditAttribute()
    {
        $user = User::first();
        Auth::login($user);
        $id_attribute = Scholarship_attribute::get()->random()->id;


        $response = $this->json('PUT', '/scholarship-attribute/'.$id_attribute.'/',
            ['website' => '6',
             'name' => $this->faker->text(),
             'url' => '1',
             'attribute1' =>$this->faker->text(),
             'attribute2' =>$this->faker->text(),
             'title' =>$this->faker->text(),
             'description' =>$this->faker->text(),
             'picture' =>$this->faker->text(),
             'registration' =>$this->faker->text(),
             'period' =>$this->faker->text(),
             ]);

        $response->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testEditAttributeFail()
    {
        $user = User::first();
        Auth::login($user);
        $id_attribute = Scholarship_attribute::get()->random()->id;


        $response = $this->json('PUT', '/scholarship-attribute/'.$id_attribute.'/',
            ['website' => '6',
             'url' => '1',
             'attribute1' =>$this->faker->text(),
             'attribute2' =>$this->faker->text(),
             'title' =>$this->faker->text(),
             'description' =>$this->faker->text(),
             'picture' =>$this->faker->text(),
             'registration' =>$this->faker->text(),
             'period' =>$this->faker->text(),
             ]);

        $response->assertJsonValidationErrors(['name']);
    }

}

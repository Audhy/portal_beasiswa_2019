<?php

namespace Tests\Unit;

use App\User;
use App\Website;
use Auth;
use Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DashboardTest extends TestCase
{
    use WithFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAnonimous()
    {
        $response = $this->json('GET', '/home');
        $response->assertStatus(401);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLoginotValid()
    {
        $response = $this->call('POST', 'login', [
           'email' => 'audhy.vk0@gmail.com',
            'password' => '12345678',
            '_token' => csrf_token()
        ]);

        $response->assertRedirect('/');
        // $this->assertTrue(true);

    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLoginotBlank()
    {
        $response = $this->call('POST', 'login', [
           'email' => '',
            'password' => '',
            '_token' => csrf_token()
        ]);

        $response->assertRedirect('/');
        // $response->assertRedirect('/home');
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLogin()
    {
        $response = $this->call('POST', 'login', [
            'email' => 'audhy.vk01@gmail.com',
            'password' => '1234567890',
            '_token' => csrf_token()
        ]);

        $response->assertRedirect('/home');
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLog()
    {
        $user = User::first();
        Auth::login($user);
        $response = $this->json('GET', '/log');
        $response->assertStatus(200);
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testSuggestion()
    {
        $user = User::first();
        Auth::login($user);
        $response = $this->json('GET', '/suggestion');
        $response->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReplyContact()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('post', '/reply',
            ['id' => '2',
            'email' => 'Aiden@gmail.com',
            'reply' => $this->faker->text(),
            'message' => $this->faker->text(),
            ]);

        $response->assertStatus(200);

    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReplyContactFailed()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('post', '/reply',
            ['id' => '2',
            'email' => 'Aiden@gmail.com',
            'message' => $this->faker->text(),
            ]);

        $response->assertStatus(200);

    }

        /**
     * A basic test example.
     *
     * @return void
     */
    public function testignore()
    {
        $id_website = Website::get()->random()->id;

        $response = $this->json('GET', '/ignore/'.$id_website);
        $response->assertStatus(401);
    }

        /**
     * A basic test example.
     *
     * @return void
     */
    public function testAggreSuggestion()
    {
        $user = User::first();
        Auth::login($user);
        $id_website = Website::where('status',1)->get()->random()->id;


        $response = $this->json('PUT', '/website/'.$id_website,
            ['name' => $this->faker->firstName(),
             'url' => $this->faker->firstName().'.com',
             'status' => '2']);

        $response->assertStatus(200);
    }




}

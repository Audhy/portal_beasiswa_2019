<?php

namespace Tests\Unit;

use App\User;
use App\Scholarship;
use Auth;
use Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClientTest extends TestCase
{
    use WithFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testViewHome()
    {
        $response = $this->call('GET', '/');
        $response->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testViewDetailScholarship()
    {
        $id_scholarship = Scholarship::get()->random()->id;
        $response = $this->call('GET', '/wel/'.$id_scholarship);
        $response->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testContactAdministrator()
    {
        $response = $this->json('POST', '/message',
            ['email' => $this->faker->firstName().'@gmail.com',
             'message' => $this->faker->text()]);

        $response->assertStatus(302);

    }

}

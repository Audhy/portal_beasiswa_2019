<?php

namespace Tests\Unit;

use App\User;
use Auth;
use Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SubscriberTest extends TestCase
{
    use WithFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testViewSubscriber()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('GET', '/get-subscribers');
        $response->assertStatus(200);
    }

     /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateSubscriber()
    {
        $response = $this->json('POST', '/subscriber',
            ['email' => $this->faker->firstName().'@gmail.com',]);

        $response->assertStatus(302);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    // public function testDeleteSubscriber()
    // {
        // $user = User::first();
        // Auth::login($user);

        // $response = $this->json('delete', '/subscriber/2');

        // $response->assertStatus(200);

    //     $this->assertTrue(true);

    // }

}

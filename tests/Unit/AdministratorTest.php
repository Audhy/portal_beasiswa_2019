<?php

namespace Tests\Unit;

use App\User;
use Auth;
use Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdministratorTest extends TestCase
{
    use WithFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testViewAdministrator()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('GET', '/get-administrators');
        $response->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateAdministrator()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('POST', '/administrator',
            ['name' => $this->faker->firstName().'_'.$this->faker->lastName(),
             'email' => $this->faker->firstName().$this->faker->lastName().'@gmail.com',
             'password' => '1234567890']);

        $response->assertStatus(200);

    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateAdministratorFail()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('POST', '/administrator',
            ['name' => $this->faker->firstName().'_'.$this->faker->lastName(),
             'password' => '1234567890']);

        $response->assertJsonValidationErrors(['email']);

    }

    /**
     * A basic test example.
     *
     * @return void
     */
    // public function testDeleteAdministrator()
    // {

    //         $user = User::first();
    //         Auth::login($user);
    //         $id_admin = User::get()->random()->id;

    //         $response = $this->json('delete', '/administrator/'.$id_admin.'/');

    //         $response->assertStatus(200);
    //         $this->assertTrue(true);

    // }

    /**
     * A basic test example.
     *
     * @return void
     */
    // public function testEditAdministrator()
    // {
    //     $user = User::first();
    //     Auth::login($user);
    //     $id_admin = User::get()->random()->id;


    //     $response = $this->json('PUT', '/administrator/'.$id_admin.'/',
    //         ['name' => $this->faker->firstName(),
    //          'email' => $this->faker->firstName().'@gmail.com',
    //          'password' => '1234567890']);

    //     $response->assertStatus(200);
    // }

    /**
     * A basic test example.
     *
     * @return void
     */
    // public function testEditAdministratorFail()
    // {
    //     $user = User::first();
    //     Auth::login($user);
    //     $id_admin = User::get()->random()->id;


    //     $response = $this->json('PUT', '/administrator/'.$id_admin.'/',
    //         ['name' => $this->faker->firstName(),
    //          'email' => $this->faker->firstName().'@gmail.com']
    //         );

    //         $response->assertJsonValidationErrors(['password']);
    //     }



}

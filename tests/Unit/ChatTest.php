<?php

namespace Tests\Unit;

use App\User;
use App\Chat;
use Auth;
use Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ChatTest extends TestCase
{
    use WithFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testViewChat()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('GET', '/get-chat/1');
        $response->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateChat()
    {
        $response = $this->json('POST', '/chat',
            ['email' => $this->faker->firstName().'@gmail.com',
             'chat' => $this->faker->text(),
             'forum_id' => '1']);

        $response->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateChatFailed()
    {
        $response = $this->json('POST', '/chat',
            ['email' => $this->faker->firstName().'@gmail.com',
             'chat' => $this->faker->text()]);

             $response->assertJsonValidationErrors(['forum_id']);

        // $response->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDeleteChat()
    {
        $user = User::first();
        Auth::login($user);
        $id_chat = Chat::get()->random()->id;

        $response = $this->json('delete', '/chat/'.$id_chat);

        $response->assertStatus(200);

        $this->assertTrue(true);

    }

}

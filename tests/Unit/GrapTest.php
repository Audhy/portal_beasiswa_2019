<?php

namespace Tests\Unit;

use App\User;
use Auth;
use Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GrapTest extends TestCase
{
    use WithFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGoogle()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('GET', '/scraper-suggestion');
        $response->assertStatus(200);
    }

        /**
     * A basic test example.
     *
     * @return void
     */
    public function testScholarship()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('GET', '/scraper-web');
        $response->assertStatus(200);
        // $this->assertTrue(true);

    }
        /**
     * A basic test example.
     *
     * @return void
     */
    public function testBroadcast()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('GET', '/subscriber-mail');
        $response->assertStatus(200);
        // $this->assertTrue(true);

    }
}

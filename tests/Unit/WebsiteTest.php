<?php

namespace Tests\Unit;

use App\Website;
use App\User;
use Auth;
use Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WebsiteTest extends TestCase
{
    use WithFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testViewWebsite()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('GET', '/get-websites');

        $response->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateWebsite()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('post', '/website',
            ['name' => $this->faker->firstName(),
             'url' => $this->faker->firstName().'.com',
             'type' => '1']);

        $response->assertStatus(200);
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateWebsiteFail()
    {
        $user = User::first();
        Auth::login($user);

        $response = $this->json('post', '/website',
            ['name' => $this->faker->firstName(),
             'type' => '1']);

        $response->assertJsonValidationErrors(['url']);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testdeleteWebsite()
    {
        $user = User::first();
        Auth::login($user);
        $id_website = Website::get()->random()->id;


        $response = $this->json('delete', '/website/'.$id_website);

        $response->assertStatus(200);
        // $this->assertTrue(true);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testEditWebsite()
    {
        $user = User::first();
        Auth::login($user);
        $id_website = Website::get()->random()->id;


        $response = $this->json('PUT', '/website/'.$id_website,
            ['name' => $this->faker->firstName(),
             'url' => $this->faker->firstName().'.com',
             'type' => '1']);

        $response->assertStatus(200);
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testEditWebsiteFail()
    {
        $user = User::first();
        Auth::login($user);
        $id_website = Website::get()->random()->id;


        $response = $this->json('PUT', '/website/'.$id_website,
            ['name' => $this->faker->firstName(),
             'url' => $this->faker->firstName().'@gmail.com']
            );

        $response->assertJsonValidationErrors(['type']);
    }

}

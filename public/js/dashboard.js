// $(document).ready(function () {
//     suggestion = function () {
//         $.ajax({
//             headers: {
//                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//             },
//             type: 'get',
//             url: '/suggestion',
//             success: function (data) {
//                 $('#suggestion-div').html(data);

//             }
//         });
//     };

//     suggestion();
// });

// $(document).ready(function () {
//     messagesToday = function () {
//         $.ajax({
//             headers: {
//                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//             },
//             type: 'get',
//             url: '/message-today',
//             success: function (data) {
//                 $('#massage-today-div').html(data);

//             }
//         });
//     };

//     messagesToday();
// });

// $(document).ready(function () {
//     messagesAll = function () {
//         $.ajax({
//             headers: {
//                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//             },
//             type: 'get',
//             url: '/message-all',
//             success: function (data) {
//                 $('#massage-all-div').html(data);

//             }
//         });
//     };

//     messagesAll();
// });

// $(document).ready(function() {
//     graptLogs = function() {
//         $.ajax({
//             headers: {
//                 "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
//             },
//             type: "get",
//             url: "/grapt-log",
//             success: function(data) {
//                 $("#grapt-log-div").html(data);
//             }
//         });
//     };

//     graptLogs();
// });

// var edit = $('.m-widget4').on('click', '.edit', function () {
//     $('#button').show();
//     $('#simpan').text('Perbarui');

//     var triggerid = $(this).data('id');

//     $.ajax({
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         },
//         type: 'get',
//         url: '/website/' + triggerid + '/edit',
//         success: function (data) {
//             $.ajax({
//                 headers: {
//                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                 },
//                 type: 'get',
//                 url: '/update-button',
//                 success: function (data) {
//                     $('#button-div').html(data);

//                 }
//             });

//             document.getElementById('id').value = data.id;
//             document.getElementById('url').value = data.url;
//             document.getElementById('name').value = data.name;
//             document.getElementById('attribute1').value = data.attribute1;
//             document.getElementById('attribute2').value = data.attribute2;
//         },
//         error: function (jqXhr, json, errorThrown) {
//             var errorsHtml = '';
//             var errors = jqXhr.responseJSON;

//             $.each(errors.errors, function (index, value) {
//                 $('#website-error').html(value);
//             });
//         }
//     });
// });

var show = $(".m-widget4").on("click", ".mail-show", function() {
    $("#button").show();
    $("#simpan").text("Perbarui");

    var triggerid = $(this).data("id");

    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        type: "get",
        url: "/message-reply/" + triggerid + "/",
        success: function(data) {
            $.ajax({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                type: "get",
                url: "/update-button",
                success: function(data) {
                    $("#button-div").html(data);
                }
            });

            document.getElementById("id").value = data.id;
            document.getElementById("email_show").value = data.email;
            document.getElementById("message_show").value = data.message;
            document.getElementById("reply_show").value = data.reply;
        },
        error: function(jqXhr, json, errorThrown) {
            var errorsHtml = "";
            var errors = jqXhr.responseJSON;

            $.each(errors.errors, function(index, value) {
                $("#scholarship-error").html(value);
            });
        }
    });
});

var reply = $(".m-widget4").on("click", ".mail-reply", function() {
    $("#button").show();
    $("#simpan").text("Perbarui");

    var triggerid = $(this).data("id");

    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        type: "get",
        url: "/message-reply/" + triggerid + "/",
        success: function(data) {
            $.ajax({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                type: "get",
                url: "/update-button",
                success: function(data) {
                    $("#button-div").html(data);
                }
            });

            document.getElementById("id").value = data.id;
            document.getElementById("email").value = data.email;
            document.getElementById("message").value = data.message;
        },
        error: function(jqXhr, json, errorThrown) {
            var errorsHtml = "";
            var errors = jqXhr.responseJSON;

            $.each(errors.errors, function(index, value) {
                $("#scholarship-error").html(value);
            });
        }
    });
});

// var addeeee = $('.website').on('click', '.add', function () {
//     alert('klik');
// });

var suggestion = $(".suggestion").on("click", ".edit", function() {
    var triggerid = $(this).data("idweb");
    // alert(triggerid);

    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        type: "get",
        url: "/website/" + triggerid + "/edit",
        success: function(data) {
            $.ajax({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                type: "get",
                url: "/update-button",
                success: function(data) {
                    $("#button-div").html(data);
                }
            });

            document.getElementById("id").value = data.id;
            document.getElementById("url").value = data.url;
            document.getElementById("name").value = data.name;
        },
        error: function(jqXhr, json, errorThrown) {
            var errorsHtml = "";
            var errors = jqXhr.responseJSON;

            $.each(errors.errors, function(index, value) {
                $("#website-error").html(value);
            });
        }
    });
});

var update = $(".modal-footer").on("click", ".update", function() {
    var triggerid = $("input[name=id]").val();

    $("#url-error").html("");
    $("#name-error").html("");
    $("#type-error").html("");

    $("#button").show();
    $("#simpan").text("Perbarui");

    var url = $("input[name=url]").val();
    var name = $("input[name=name]").val();
    var type = $('input[name="type"]:checked').val();
    // var attribute1 = $('input[name=attribute1]').val();
    // var attribute2 = $('input[name=attribute2]').val();
    // var triggerid = $('input[name=id]').val();
    // var title = $('input[name=title]').val();
    // var picture = $('input[name=picture]').val();
    // var description = $('input[name=description]').val();
    // var registration = $('input[name=registration]').val();
    // var period = $('input[name=period]').val();

    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        type: "put",
        url: "/website/" + triggerid,
        data: {
            _token: $("input[name=_token]").val(),
            url: url,
            name: name,
            type: type
            // attribute1: attribute1,
            // attribute2: attribute2,
            // title: title,
            // picture: picture,
            // description: description,
            // registration: registration,
            // period: period,
        },
        success: function(data) {
            if (data.errors) {
                if (data.errors.url) {
                    $("#url-error").html(data.errors.url[0]);
                }
                if (data.errors.name) {
                    $("#name-error").html(data.errors.name[0]);
                }

                if (data.errors.type) {
                    $("#type-error").html(data.errors.type[0]);
                }

                document.getElementById("url").value = url;
                document.getElementById("name").value = name;
                if (type == 2) {
                    document.getElementById("type2").checked = true;
                } else if (type == 3) {
                    document.getElementById("type3").checked = true;
                }
            } else {
                $("#modal_website").modal("hide");

                toastr.success("Data berhasil disimpan.", "Sukses", {
                    timeOut: 5000
                });

                // var table = $('.m_datatable').mDatatable();

                // table.originalDataSet = [];
                // table.reload();

                $("#url-error").html("");
                $("#name-error").html("");
                $("#type-error").html("");
                $("#attribute1-error").html("");
                $("#attribute2-error").html("");
                location.reload();
            }
        }
    });
});
// var suggestion = $('.modal-footer').on('click', '.add', function () {
//     $('#url-error').html('');
//     $('#name-error').html('');
//     $('#type-error').html('');

//     $('#simpan').text('Simpan');

//     var registerForm = $('#CustomerForm');
//     var url = $('input[name=url]').val();
//     var name = $('input[name=name]').val();
//     var type = $('input[name="type"]:checked').val();

//     $.ajax({
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         },
//         type: 'post',
//         url: '/website/suggestion',
//         data: {
//             _token: $('input[name=_token]').val(),
//             url: url,
//             name: name,
//             type: type,
//        },
//         success: function (data) {
//             if (data.errors) {
//                 if (data.errors.url) {
//                     $('#url-error').html(data.errors.url[0]);

//                 }
//                 if (data.errors.name) {
//                     $('#name-error').html(data.errors.name[0]);

//                 }

//                 if (data.errors.type) {
//                     $('#type-error').html(data.errors.type[0]);

//                 }

//                 document.getElementById('url').value = url;
//                 document.getElementById('name').value = name;
//                 if(type == 2){
//                     document.getElementById("type2").checked = true;
//                 }
//                 else if(type == 3){
//                     document.getElementById("type3").checked = true;
//                 }

//             } else {
//                 $('#modal_website').modal('hide');

//                 toastr.success('Data berhasil disimpan.', 'Sukses', {
//                     timeOut: 5000
//                 });

//                 $('#url-error').html('');
//                 $('#name-error').html('');
//                 $('#type-error').html('');
//                 $('#attribute1-error').html('');
//                 $('#attribute2-error').html('');

//                 var table = $('.m_datatable').mDatatable();

//                 table.originalDataSet = [];
//                 table.reload();
//             }
//         }
//     });
// });

var sent = $(".sent-mail").on("click", ".sent", function() {
    $("#simpan").text("Simpan");
    var id = $("input[name=id]").val();
    var email = $("input[name=email]").val();
    var message = $("#message").val();
    var reply = $("#reply").val();

    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        type: "post",
        url: "/reply",
        data: {
            // poster :poster
            _token: $("input[name=_token]").val(),
            id: id,
            email: email,
            message: message,
            reply: reply
        },
        success: function(data) {
            if (data.errors) {
                if (data.errors.reply) {
                    $("#reply-error").html(data.errors.reply[0]);

                    document.getElementById("id").value = id;
                    document.getElementById("email").value = email;
                    document.getElementById("message").value = message;
                    document.getElementById("reply").value = reply;
                }
            } else {
                $("#modal_reply").modal("hide");

                toastr.success("Your Reply Send.", "Sukses", {
                    timeOut: 5000
                });

                $("#reply-error").html("");
            }
        }
    });
});

// var edit = $('.m-widget4').on('click', '.ignore', function () {
//     alert('ignore');
// });

$(document).ready(function () {
    // ForumFunc = function () {
    var forum2 = $('.m-portlet__head').on('click', '.forum', function () {
        var triggerid = $(this).data('id');
        $.ajax({
            url: '/forum-wel/' + triggerid,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                if (data == "") {
                    // alert('kosong')
                    $('#div-forum').empty()
                    $('#div-forum').append(
                        '<div class="m-timeline-2__item m--margin-top-30">' +
                        '<span class="m-timeline-2__item-time"></span>	' +
                        '<div class="m-timeline-2__item-cricle">' +
                        '<i class="fa fa-genderless m--font-success"></i>' +
                        '</div>' +
                        '<div class="m-timeline-2__item-text m-timeline-2__item-text--bold">' +
                        'Create Forum :  <br>' +
                        '<input type="hidden" value="' + triggerid + '" name="id" id="id">' +
                        '<input type=text class="form-control m-input" name="forum" id="forum">' +
                        '<div class="form-control-feedback text-danger" id="fotum-error"></div><br>' +
                        '<button type="reset" name="submmit" class="btn btn-success btn-md add" >' +
                        '<span><i class="fa fa-save"></i><span>Submit</span></span>' +
                        '</button>' +
                        '</div>' +
                        '</div>'
                    )
                } else {
                    $('#div-forum-footer').empty()
                    $('#div-forum').empty()
                    for (var i = 0; i < data.length; i++) {
                        var obj = data[i];
                        // alert(obj.id);
                        $('#div-forum').append(
                            '<div class="m-timeline-2__item m--margin-top-30">' +
                            '<span class="m-timeline-2__item-time"></span>	' +
                            '<div class="m-timeline-2__item-cricle">' +
                            '<i class="fa fa-genderless m--font-success"></i>' +
                            '</div>' +
                            '<div class="m-timeline-2__item-text m-timeline-2__item-text--bold">' +
                            // '<p class="chat">'+obj.name+'</p>'+
                            '<a href="#" class="chat" data-id="' + obj.id + '">' + obj.name + '</a>' +
                            // '<a href="/forum/'+obj.id+'">'+obj.name+'</a>'+
                            '</div>' +
                            '</div>'
                        )
                    };
                    $('#div-forum-footer').append(
                        '<div class="m-timeline-2__item m--margin-top-30">' +
                        '<span class="m-timeline-2__item-time"></span>	' +
                        '<div class="m-timeline-2__item-cricle">' +
                        '<i class="fa fa-genderless m--font-success"></i>' +
                        '</div>' +
                        '<div class="m-timeline-2__item-text m-timeline-2__item-text--bold">' +
                        'Create Forum :  <br>' +
                        '<input type="hidden" value="' + triggerid + '" name="id" id="id">' +
                        '<input type=text class="form-control m-input" name="forum" id="forum">' +
                        '<div class="form-control-feedback text-danger" id="fotum-error"></div><br>' +
                        '<button type="reset" name="submmit" class="btn btn-success btn-md add" >' +
                        '<span><i class="fa fa-save"></i><span>Submit</span></span>' +
                        '</button>' +
                        '</div>' +
                        '</div>'
                    )
                }
            }
        });
    });
    // };
    // ForumFunc();
});

var chat = $('.m-timeline-2').on('click', '.chat', function () {
    var triggerid = $(this).data('id');
    $.ajax({
        url: '/chat-wel/' + triggerid,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            if (data == "") {
                $('#div-forum-footer').empty()

                $('#div-forum').empty()
                $('#div-forum2').remove();
                $('#div-chat').remove();

                $('#div-chat-footer').empty()
                $('#div-chat-footer').append(
                    '<div class="m-timeline-2__item m--margin-top-30" ' +
                    '<span class="m-timeline-2__item-time"></span>	' +
                    '<div class="m-timeline-2__item-cricle">' +
                    '</div>' +
                    '<div class="m-timeline-2__item-text m-timeline-2__item-text--bold">' +
                    'Email :  <br>' +
                    '<input type="hidden" value="' + triggerid + '" name="id" id="id">' +
                    '<input type=text class="form-control m-input" name="email" id="email">' +
                    '<div class="form-control-feedback text-danger" id="email-error"></div><br>' +
                    'Comment :  <br>' +
                    '<textarea class="form-control m-input m-input--air" rows="5" name="chat" id="chat"></textarea>'+
                    '<div class="form-control-feedback text-danger" id="chat-error"></div><br>' +
                    '<button type="reset" name="submmit" class="btn btn-success btn-md add-chat" >' +
                    '<span><span>Sent</span></span>' +
                    '</button>' +
                    '</div>' +
                    '</div>'
                )

            }
            else{
                $('#div-chat-footer').empty()

                $('#div-forum-footer').empty()

                $('#div-forum').empty()
                for (var i = 0; i < data.length; i++) {
                    var obj = data[i];
                    // alert(obj.id);
                    // document.getElementById("div-forum").innerHTML = "";
                    $('#div-forum2').remove();
                    $('#div-chat').append(

                        '<div class="tab-content" style="margin-left:-1000px">' +
                        '<div class="tab-pane active" id="m_widget2_tab1_content" aria-expanded="true">' +
                        '<div class="m-timeline-1 m-timeline-1--fixed">' +
                        '<div class="m-timeline-1__items">' +
                        '<div class="m-timeline-1__marker"></div>' +
                        '<div class="m-timeline-1__item m-timeline-1__item--right m-timeline-1__item--first">' +
                        '<div class="m-timeline-1__item-circle"><div class="m--bg-danger"></div></div>' +
                        '<div class="m-timeline-1__item-arrow"></div>' +
                        '<span class="m-timeline-1__item-time m--font-brand">'+obj.created_at+'</span>' +
                        '<div class="m-timeline-1__item-content">' +
                        '<div class="m-timeline-1__item-title">' +
                        '<p>'+obj.email+'</p>' +
                        '</div>' +
                        '<div class="m-timeline-1__item-body">' +
                        '<div class="m-list-pics">' +
                        '</div>' +
                        '<div class="m-timeline-1__item-body m--margin-top-15">' +
                        '<p>'+obj.chat+'</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +

                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>'


                    )
                };

                $('#div-chat-footer').append(
                    '<div class="m-timeline-2__item m--margin-top-30">' +
                    '<span class="m-timeline-2__item-time"></span>	' +
                    '<div class="m-timeline-2__item-cricle">' +
                    '</div>' +
                    '<div class="m-timeline-2__item-text m-timeline-2__item-text--bold">' +
                    'Email :  <br>' +
                    '<input type="hidden" value="' + triggerid + '" name="id" id="id">' +
                    '<input type=text class="form-control m-input" name="email" id="email">' +
                    '<div class="form-control-feedback text-danger" id="email-error"></div><br>' +
                    'Comment :  <br>' +
                    '<textarea class="form-control m-input m-input--air" rows="5" name="chat" id="chat"></textarea>'+
                    '<div class="form-control-feedback text-danger" id="chat-error"></div><br>' +
                    '<button type="reset" name="submmit" class="btn btn-success btn-md add-chat" >' +
                    '<span><span>Sent</span></span>' +
                    '</button>' +
                    '</div>' +
                    '</div>'
                )
            }



        }
    });
});


var simpan = $('.m-timeline-2').on('click', '.add', function () {
    $('#simpan').text('Simpan');
    // alert('cklik');
    var registerForm = $('#CustomerForm');
    var forum = $('input[name=forum]').val();
    var id = $('input[name=id]').val();
    var formData = registerForm.serialize();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'post',
        url: '/forum',
        data: {
            _token: $('input[name=_token]').val(),
            forum: forum,
            scholarship_id: id,
        },
        success: function (data) {
            if (data.errors) {
                if (data.errors.forum) {
                    $('#forum-error').html(data.errors.forum[0]);


                    document.getElementById('forum').value = forum;
                }


            } else {
                // window.location.reload(false);
                toastr.success('Data berhasil disimpan.', 'Sukses', {
                    timeOut: 5000
                });
                $('#div-chat').empty()
                $('#div-forum-footer').empty()



                // ForumFunc(id);
                $('#forum-error').html('');

                $('#div-chat').remove();

                $('#div-chat-footer').empty()
                $('#div-chat-footer').append(
                    '<div class="m-timeline-2__item m--margin-top-30" ' +
                    '<span class="m-timeline-2__item-time"></span>	' +
                    '<div class="m-timeline-2__item-cricle">' +
                    '</div>' +
                    '<div class="m-timeline-2__item-text m-timeline-2__item-text--bold">' +
                    'Email :  <br>' +
                    '<input type="hidden" value="' + data.id + '" name="id" id="id">' +
                    '<input type=text class="form-control m-input" name="email" id="email">' +
                    '<div class="form-control-feedback text-danger" id="email-error"></div><br>' +
                    'Comment :  <br>' +
                    '<textarea class="form-control m-input m-input--air" rows="5" name="chat" id="chat"></textarea>'+
                    '<div class="form-control-feedback text-danger" id="chat-error"></div><br>' +
                    '<button type="reset" name="submmit" class="btn btn-success btn-md add-chat" >' +
                    '<span><span>Sent</span></span>' +
                    '</button>' +
                    '</div>' +
                    '</div>'
                )
            }
        }
    });
});

var simpan = $('.chat').on('click', '.add-chat', function () {
    $('#simpan').text('Simpan');
    // alert('klik2');
    var registerForm = $('#CustomerForm');
    var email = $('input[name=email]').val();
    var chat = $('#chat').val();

    var id = $('input[name=id]').val();
    var formData = registerForm.serialize();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'post',
        url: '/chat',
        data: {
            _token: $('input[name=_token]').val(),
            chat: chat,
            email: email,
            forum_id: id,
        },
        success: function (data) {
            if (data.errors) {
                if (data.errors.email) {
                    $('#email-error').html(data.errors.email[0]);
                    document.getElementById('email').value = email;
                    document.getElementById('chat').value = chat;
                }
                if (data.errors.chat) {
                    $('#chat-error').html(data.errors.forum[0]);
                    document.getElementById('email').value = email;
                    document.getElementById('chat').value = chat;
                }


            } else {

                // window.location.reload(false);
                toastr.success('Data berhasil disimpan.', 'Sukses', {
                    timeOut: 5000
                });

                document.getElementById('email').value = "";
                document.getElementById('chat').value = "";


                $('#div-chat2').append(

                    '<div class="tab-content" style="margin-left:-1000px">' +
                    '<div class="tab-pane active" id="m_widget2_tab1_content" aria-expanded="true">' +
                    '<div class="m-timeline-1 m-timeline-1--fixed">' +
                    '<div class="m-timeline-1__items">' +
                    '<div class="m-timeline-1__marker"></div>' +
                    '<div class="m-timeline-1__item m-timeline-1__item--right m-timeline-1__item--first">' +
                    '<div class="m-timeline-1__item-circle"><div class="m--bg-danger"></div></div>' +
                    '<div class="m-timeline-1__item-arrow"></div>' +
                    '<span class="m-timeline-1__item-time m--font-brand">Recently</span>' +
                    '<div class="m-timeline-1__item-content">' +
                    '<div class="m-timeline-1__item-title">' +
                    '<p>'+email+'</p>' +
                    '</div>' +
                    '<div class="m-timeline-1__item-body">' +
                    '<div class="m-list-pics">' +
                    '</div>' +
                    '<div class="m-timeline-1__item-body m--margin-top-15">' +
                    '<p>'+chat+'</p>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +

                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>'


                )

                // $('#div-chat').empty()
                // $('#div-forum-footer').empty()



                // ForumFunc(id);
                // $('#forum-error').html('');
            }
        }
    });
});

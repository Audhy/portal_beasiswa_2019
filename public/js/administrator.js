var Administrator = {
    init: function () {
        $('.m_datatable').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'GET',
                        url: '/get-administrators',
                        map: function (raw) {
                            var dataSet = raw;

                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }

                            return dataSet;
                        }
                    }
                },
                pageSize: 10,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: !0
            },
            layout: {
                theme: 'default',
                class: '',
                scroll: false,
                footer: !1
            },
            sortable: !0,
            filterable: !1,
            pagination: !0,
            search: {
                input: $('#generalSearch')
            },
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50, 100]
                    }
                }
            },
            columns: [
                {
                    field: 'id',
                    title: '#',
                    sortable: 'asc',
                    filterable: !1,
                    width: 60
                },
                {
                    field: 'name',
                    title: 'Name',
                    sortable: 'asc',
                    filterable: !1,
                    width: 250
                },
                {
                    field: 'email',
                    title: 'Email',
                    sortable: 'asc',
                    filterable: !1,
                    width: 350,
                },
                // {
                //     field: 'Actions',
                //     width: 110,
                //     title: 'Actions',
                //     sortable: !1,
                //     overflow: 'visible',
                //     template: function (t, e, i) {
                //         return (
                //             '<button data-toggle="modal" data-target="#modal_administrator" type="button" href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill edit" title="Edit" data-id=' +
                //             t.id +
                //             '>\t\t\t\t\t\t\t<i class="la la-pencil"></i>\t\t\t\t\t\t</button>\t\t\t\t\t\t'
                            // '\t\t\t\t\t\t\t<a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill  delete" href="#" data-id=' +
                            // t.id +
                            // ' title="Delete"><i class="la la-trash"></i> </a>\t\t\t\t\t\t\t'
                //         );
                //     }
                // }
            ]
        });


        var save = $('.align-items-center').on('click', '.btn-primary', function () {
                        $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'get',
                url: '/submit-button',
                success: function (data) {
                    $('#button-div').html(data);

                }
            });

            document.getElementById('email').value = "";
            document.getElementById('name').value = "";


        });


        var simpan = $('.modal-footer').on('click', '.add', function () {
            $('#name-error').html("");
            $('#email-error').html("");
            $('#password-error').html("");

            $('#simpan').text('Simpan');

            var registerForm = $('#CustomerForm');
            var email = $('input[name=email]').val();
            var name = $('input[name=name]').val();
            var password = $('input[name=password]').val();
            var confirm = $('input[name=confirm]').val();
            var formData = registerForm.serialize();
            if(password == confirm){
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'post',
                    url: '/administrator',
                    data: {
                        _token: $('input[name=_token]').val(),
                        email: email,
                        name: name,
                        password: password
                    },
                    success: function (data) {
                        if (data.errors) {
                            if (data.errors.email) {
                                $('#email-error').html(data.errors.email[0]);
                            }
                            if (data.errors.name) {
                                $('#name-error').html(data.errors.name[0]);
                            }
                            if (data.errors.password) {
                                $('#password-error').html(data.errors.password[0]);
                            }
                            document.getElementById('email').value = email;
                            document.getElementById('name').value = name;
                    } else {
                            $('#modal_administrator').modal('hide');

                            toastr.success('Data berhasil disimpan.', 'Sukses', {
                                timeOut: 5000
                            });

                            $('#email-error').html('');
                            $('#name-error').html('');
                            var table = $('.m_datatable').mDatatable();

                            table.originalDataSet = [];
                            table.reload();
                        }
                    }
                });
            }
            else{
                alert('Password difference');
                // document.getElementById('email').value = email;
                // document.getElementById('name').value = name;
            }

        });

        var edit = $('.m_datatable').on('click', '.edit', function () {
            $('#button').show();
            $('#simpan').text('Perbarui');

            var triggerid = $(this).data('id');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'get',
                url: '/administrator/' + triggerid + '/edit',
                success: function (data) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'get',
                        url: '/update-button',
                        success: function (data) {
                            $('#button-div').html(data);

                        }
                    });

                    document.getElementById('id').value = data.id;
                    document.getElementById('name').value = data.name;
                    document.getElementById('email').value = data.email;
                },
                error: function (jqXhr, json, errorThrown) {
                    var errorsHtml = '';
                    var errors = jqXhr.responseJSON;

                    $.each(errors.errors, function (index, value) {
                        $('#edit-error').html(value);
                    });
                }
            });
        });

        var update = $('.modal-footer').on('click', '.update', function () {
            $('#name-error').html("");
            $('#email-error').html("");
            $('#password-error').html("");

            $('#button').show();
            $('#name-error').html('');
            $('#simpan').text('Perbarui');

            var email = $('input[name=email]').val();
            var name = $('input[name=name]').val();
            var password = $('input[name=password]').val();
            var confirm = $('input[name=confirm]').val();
            var triggerid = $('input[name=id]').val();
            if(password == confirm){
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'put',
                    url: '/administrator/' + triggerid,
                    data: {
                        _token: $('input[name=_token]').val(),
                        email: email,
                        name: name,
                        password: password,
                    },
                    success: function (data) {
                        if (data.errors) {
                            if (data.errors.email) {
                                $('#email-error').html(data.errors.email[0]);
                            }
                            if (data.errors.name) {
                                $('#name-error').html(data.errors.name[0]);
                            }
                            if (data.errors.password) {
                                $('#password-error').html(data.errors.password[0]);
                            }
                            document.getElementById('email').value = email;
                            document.getElementById('name').value = name;

                        } else {
                            $('#modal_administrator').modal('hide');

                            toastr.success('Data berhasil disimpan.', 'Sukses', {
                                timeOut: 5000
                            });

                            var table = $('.m_datatable').mDatatable();

                            table.originalDataSet = [];
                            table.reload();

                            $('#email-error').html('');
                            $('#name-error').html('');
                        }
                    }
                });
            }
            else{
                alert('Password difference');
            }

        });

        var remove = $('.m_datatable').on('click', '.delete', function () {
            var triggerid = $(this).data('id');

            swal({
                title: 'Are you sure?',
                text: 'You will not be able to recover this imaginary file!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, keep it'
            }).then(result => {
                if (result.value) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr(
                                'content'
                            )
                        },
                        type: 'DELETE',
                        url: '/administrator/' + triggerid,
                        success: function (data) {
                            toastr.success(
                                'Data berhasil dihapus.',
                                'Sukses', {
                                    timeOut: 5000
                                }
                            );

                            var table = $('.m_datatable').mDatatable();

                            table.originalDataSet = [];
                            table.reload();
                        },
                        error: function (jqXhr, json, errorThrown) {
                            var errorsHtml = '';
                            var errors = jqXhr.responseJSON;

                            $.each(errors.errors, function (index, value) {
                                $('#delete-error').html(value);
                            });
                        }
                    });
                    swal(
                        'Deleted!',
                        'Your imaginary file has been deleted.',
                        'success'
                    );
                } else {
                    swal(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });

        $('#modal_customer').on('hidden.bs.modal', function (e) {
            $(this).find('#CustomerForm')[0].reset();

            $('#name-error').html('');
        });
    }
};

jQuery(document).ready(function () {
    Administrator.init();
});

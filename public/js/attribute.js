var Attribute = {
    init: function () {
        $('.m_datatable').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'GET',
                        url: '/get-scholarshipattribute',
                        map: function (raw) {
                            var dataSet = raw;

                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }

                            return dataSet;
                        }
                    }
                },
                pageSize: 10,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: !0
            },
            layout: {
                theme: 'default',
                class: '',
                scroll: false,
                footer: !1
            },
            sortable: !0,
            filterable: !1,
            pagination: !0,
            search: {
                input: $('#generalSearch')
            },
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50, 100]
                    }
                }
            },
            columns: [{
                    field: 'id',
                    title: '#',
                    sortable: 'asc',
                    filterable: !1,
                    width: 60
                },
                {
                    field: 'name',
                    title: 'Name',
                    sortable: 'asc',
                    filterable: !1,
                    width: 150
                },
                {
                    field: 'url',
                    title: 'URL',
                    sortable: 'asc',
                    filterable: !1,
                    width: 150,
                },
                {
                    field: 'Actions',
                    width: 110,
                    title: 'Actions',
                    sortable: !1,
                    overflow: 'visible',
                    template: function (t, e, i) {
                        return (
                            '<button data-toggle="modal" data-target="#modal_attribute" type="button" href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill show" title="Details" data-id=' +
                            t.id +
                            '>\t\t\t\t\t\t\t<i class="la la-search"></i>\t\t\t\t\t\t</button>\t\t\t\t\t\t'+
                            '<button data-toggle="modal" data-target="#modal_attribute" type="button" href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill edit" title="Edit" data-id=' +
                            t.id +
                            '>\t\t\t\t\t\t\t<i class="la la-pencil"></i>\t\t\t\t\t\t</button>\t\t\t\t\t\t' +
                            '\t\t\t\t\t\t\t<a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill  delete" href="#" data-id=' +
                            t.id +
                            ' title="Delete"><i class="la la-trash"></i> </a>\t\t\t\t\t\t\t'
                        );
                    }
                }
            ]
        });

        var simpan = $('.modal-footer').on('click', '.add', function () {
            $('#simpan').text('Simpan');

            var registerForm = $('#CustomerForm');
            var url = $('input[name=url]').val();
            var name = $('input[name=name]').val();
            // var type = $('input[name="type"]:checked').val();
            // var type = $('#type').val();
            var attribute1 = $('input[name=attribute1]').val();
            // var attribute2 = $('input[name=attribute2]').val();
            var title = $('input[name=title]').val();
            var picture = $('input[name=picture]').val();
            var description = $('#description').val();
            var registration = $('input[name=registration]').val();
            var period = $('input[name=period]').val();
            var website_id = $('#website2').val();
            var attribute_id = $('#attribute_id').val();
            var additional1 = $('input[name=additional1]').val();
            var additional2 = $('input[name=additional2]').val();
            var additional3 = $('input[name=additional3]').val();
            var additional4 = $('input[name=additional4]').val();
            var additional5 = $('input[name=additional5]').val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'post',
                url: '/scholarship-attribute',
                data: {
                    _token: $('input[name=_token]').val(),
                    url: url,
                    name: name,
                    attribute_id: attribute_id,
                    website_id: website_id,
                    attribute1: attribute1,
                    // attribute2: attribute2,
                    title: title,
                    picture: picture,
                    description: description,
                    registration: registration,
                    period: period,
                    attribute_additional1: additional1,
                    attribute_additional2: additional2,
                    attribute_additional3: additional3,
                    attribute_additional4: additional4,
                    attribute_additional5: additional5,
                },
                success: function (data) {
                    if (data.errors) {
                        if (data.errors.url) {
                            $('#url-error').html(data.errors.url[0]);

                        }
                        if (data.errors.name) {
                            $('#name-error').html(data.errors.name[0]);

                        }
                        if (data.errors.website_id) {
                            $('#website_id-error').html(data.errors.website_id[0]);

                        }
                        if (data.errors.attribute1) {
                            $('#attribute1-error').html(data.errors.attribute1[0]);

                        }

                        document.getElementById('url').value = url;
                        document.getElementById('name').value = name;

                        document.getElementById('attribute1').value = attribute1;
                        // document.getElementById('attribute2').value = attribute2;
                        // document.getElementById('picture').value = attribute_picture;
                        // document.getElementById('title').value = attribute_title;
                        // document.getElementById('description').value = attribute_description;
                        // document.getElementById('registration').value = attribute_registration;
                        // document.getElementById('period').value = attribute_period;

                    } else {
                        $('#modal_attribute').modal('hide');

                        toastr.success('Data berhasil disimpan.', 'Sukses', {
                            timeOut: 5000
                        });

                        $('#url-error').html('');
                        $('#name-error').html('');
                        $('#type-error').html('');
                        $('#attribute1-error').html('');
                        $('#attribute2-error').html('');

                        var table = $('.m_datatable').mDatatable();

                        table.originalDataSet = [];
                        table.reload();
                    }
                }
            });
        });


        var testing = $('.modal-footer').on('click', '.try', function () {
            var testing = $('input[name="try"]:checked').val();
            // alert(testing);
            var attr = $('input[name=attribute1]').val();

            if (testing == 2) {
                // dd('2');
                var attribute = $('input[name=title]').val();
            } else if (testing == 3) {
                // dd('3');
                var attribute = $('input[name=description]').val();
            } else if (testing == 4) {
                // dd('4');
                var attribute = $('input[name=picture]').val();
            } else if (testing == 5) {
                // dd('5');
                var attribute = $('input[name=period]').val();
            } else if (testing == 6) {
                // dd('6');
                var attribute = $('input[name=registration]').val();
            } else if (testing == 7) {
                // dd('6');
                var attribute = $('input[name=additional1]').val();
            } else if (testing == 8) {
                // dd('6');
                var attribute = $('input[name=additional2]').val();
            } else if (testing == 9) {
                // dd('6');
                var attribute = $('input[name=additional3]').val();
            } else if (testing == 10) {
                // dd('6');
                var attribute = $('input[name=additional4]').val();
            } else if (testing == 11) {
                // dd('6');
                var attribute = $('input[name=additional5]').val();
            }

            // alert(attribute);
            // alert(attribute);
            // else{
            //     var attribute = "";
            // }


            // $('#simpan').text('Simpan');

            var url = $('input[name=url]').val();
            // var name = $('input[name=name]').val();
            // // var type = $('input[name="type"]:checked').val();
            // // var type = $('#type').val();
            // var attribute1 = $('input[name=attribute1]').val();
            // var attribute2 = $('input[name=attribute2]').val();
            // var title = $('input[name=title]').val();
            // var picture = $('input[name=picture]').val();
            // var description = $('input[name=description]').val();
            // var registration = $('input[name=registration]').val();
            // var period = $('input[name=period]').val();

            // var formData = registerForm.serialize();
            // // alert(type);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'post',
                url: '/tes',
                data: {
                    _token: $('input[name=_token]').val(),
                    url: url,
                    attr: attr,
                    attribute: attribute
                    // name: name,
                    // type: type,
                    // attribute1: attribute1,
                    // attribute2: attribute2,
                    // title: title,
                    // picture: picture,
                    // description: description,
                    // registration: registration,
                    // period: period,

                },
                success: function (data) {

                    if (data.errors) {
                        alert('Error please try again');
                        // if (data.errors.url) {
                        //     $('#url-error').html(data.errors.code[0]);


                        //     document.getElementById('url').value = url;
                        //     document.getElementById('name').value = name;
                        //     if(type == 2){
                        //         document.getElementById("type2").checked = true;
                        //     }
                        //     else{
                        //         document.getElementById("type3").checked = true;
                        //     }
                        //     document.getElementById('attribute1').value = attribute1;
                        //     document.getElementById('attribute2').value = attribute2;
                        // }


                    } else {

                        document.getElementById("wait").remove();
                        $.each(data, function (key, value) {
                            // alert(value);
                            if (value == 'failed') {
                                $('#testing').append(
                                    '<h5>Failed</h5>'
                                );

                            } else {
                                $('#testing').append(
                                    '<h5>' + value + '</h5>'
                                );

                            }
                        });

                        toastr.success('Done.', 'Sukses', {
                            timeOut: 5000
                        });

                        $('#name-error').html('');

                    }
                }
            });
        });

        var edit = $('.m_datatable').on('click', '.show', function () {
            $('#button').show();
            $('#simpan').text('Perbarui');

            var x = document.getElementById("foo");
            // if (x.style.display === "none") {
            //   x.style.display = "block";
            // } else {
              x.style.display = "none";
            // }

            var triggerid = $(this).data('id');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'get',
                url: '/scholarship-attribute/' + triggerid + '/edit',
                success: function (data) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'get',
                        url: '/update-button',
                        success: function (data) {
                            // $('#button-div').html(data);

                        }
                    });

                    document.getElementById('id').value = data.id;
                    document.getElementById('url').value = data.url;
                    document.getElementById("url").readOnly = true;
                    document.getElementById('name').value = data.name;
                    document.getElementById("name").readOnly = true;
                    // if(data.status == 2){
                    //     document.getElementById("type2").checked = true;
                    // }
                    // else{
                    //     document.getElementById("type3").checked = true;
                    // }
                    document.getElementById('attribute1').value = data.attribute1;
                    document.getElementById("url").readOnly = true;

                    // document.getElementById('attribute2').value = data.attribute2;
                    document.getElementById('title').value = data.attribute_title;
                    document.getElementById("title").readOnly = true;

                    document.getElementById('picture').value = data.attribute_picture;
                    document.getElementById("picture").readOnly = true;
                    document.getElementById('description').value = data.attribute_description;
                    document.getElementById("description").readOnly = true;
                    document.getElementById('registration').value = data.attribute_registration;
                    document.getElementById("registration").readOnly = true;
                    document.getElementById('period').value = data.attribute_period;
                    document.getElementById("period").readOnly = true;
                    document.getElementById('additional1').value = data.attribute_additional_1;
                    document.getElementById("additional1").readOnly = true;
                    document.getElementById('additional2').value = data.attribute_additional_2;
                    document.getElementById("additional2").readOnly = true;
                    document.getElementById('additional3').value = data.attribute_additional_3;
                    document.getElementById("additional3").readOnly = true;
                    document.getElementById('additional4').value = data.attribute_additional_4;
                    document.getElementById("additional4").readOnly = true;
                    document.getElementById('additional5').value = data.attribute_additional_5;
                    document.getElementById("additional5").readOnly = true;
                    // alert(data.attribute_id);
                    $.ajax({
                        url: '/get-fill-attributes/',
                        type: 'GET',
                        dataType: 'json',
                        success: function (attribute) {
                            let angka = 1;

                            $('select[name="attribute_id"]').empty();
                            $('select[name="attribute_id"]').append(
                                '<option value="0" selected> None </option>'
                            );
                            $.each(attribute, function (key, value) {
                                if (key == data.attribute_id) {
                                    $('select[name="attribute_id"]').append(
                                        '<option value="' + key + '" selected>' + value + '</option>'
                                    );

                                    document.getElementById("name").setAttribute("disabled", "disabled");


                                } else {
                                    $('select[name="attribute_id"]').append(
                                        '<option value="' + key + '">' + value + '</option>'
                                    );
                                }
                            });
                        }
                    });

                    $('#attribute_id')
                    .attr("disabled", true);

                    $.ajax({
                        url: '/get-fill-websites/',
                        type: 'GET',
                        dataType: 'json',
                        success: function (unit) {
                            $('select[name="website2"]').empty();

                            $.each(unit, function (key, value) {
                                if (key == data.website_id) {
                                    $('select[name="website2"]').append(
                                        '<option value="' + key + '" selected>' + value + '</option>'
                                    );
                                } else {
                                    $('select[name="website2"]').append(
                                        '<option value="' + key + '">' + value + '</option>'
                                    );
                                }
                            });
                        }
                    });

                    $('#website2')
                    .attr("disabled", true);


                },
                error: function (jqXhr, json, errorThrown) {
                    var errorsHtml = '';
                    var errors = jqXhr.responseJSON;

                    $.each(errors.errors, function (index, value) {
                        $('#website-error').html(value);
                    });
                }
            });
        });
        var edit = $('.m_datatable').on('click', '.edit', function () {
            $('#button').show();
            $('#simpan').text('Perbarui');

            var triggerid = $(this).data('id');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'get',
                url: '/scholarship-attribute/' + triggerid + '/edit',
                success: function (data) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'get',
                        url: '/update-button',
                        success: function (data) {
                            // $('#button-div').html(data);

                        }
                    });

                    document.getElementById('id').value = data.id;
                    document.getElementById('url').value = data.url;
                    document.getElementById('name').value = data.name;
                    // if(data.status == 2){
                    //     document.getElementById("type2").checked = true;
                    // }
                    // else{
                    //     document.getElementById("type3").checked = true;
                    // }
                    document.getElementById('attribute1').value = data.attribute1;
                    // document.getElementById('attribute2').value = data.attribute2;
                    document.getElementById('title').value = data.attribute_title;
                    document.getElementById('picture').value = data.attribute_picture;
                    document.getElementById('description').value = data.attribute_description;
                    document.getElementById('registration').value = data.attribute_registration;
                    document.getElementById('period').value = data.attribute_period;
                    document.getElementById('additional1').value = data.attribute_additional_1;
                    document.getElementById('additional2').value = data.attribute_additional_2;
                    document.getElementById('additional3').value = data.attribute_additional_3;
                    document.getElementById('additional4').value = data.attribute_additional_4;
                    document.getElementById('additional5').value = data.attribute_additional_5;
                    // alert(data.attribute_id);
                    $.ajax({
                        url: '/get-fill-attributes/',
                        type: 'GET',
                        dataType: 'json',
                        success: function (attribute) {
                            let angka = 1;

                            $('select[name="attribute_id"]').empty();
                            $('select[name="attribute_id"]').append(
                                '<option value="0" selected> None </option>'
                            );
                            $.each(attribute, function (key, value) {
                                if (key == data.attribute_id) {
                                    $('select[name="attribute_id"]').append(
                                        '<option value="' + key + '" selected>' + value + '</option>'
                                    );

                                    document.getElementById("name").setAttribute("disabled", "disabled");


                                } else {
                                    $('select[name="attribute_id"]').append(
                                        '<option value="' + key + '">' + value + '</option>'
                                    );
                                }
                            });
                        }
                    });

                    $.ajax({
                        url: '/get-fill-websites/',
                        type: 'GET',
                        dataType: 'json',
                        success: function (unit) {
                            $('select[name="website2"]').empty();

                            $.each(unit, function (key, value) {
                                if (key == data.website_id) {
                                    $('select[name="website2"]').append(
                                        '<option value="' + key + '" selected>' + value + '</option>'
                                    );
                                } else {
                                    $('select[name="website2"]').append(
                                        '<option value="' + key + '">' + value + '</option>'
                                    );
                                }
                            });
                        }
                    });

                },
                error: function (jqXhr, json, errorThrown) {
                    var errorsHtml = '';
                    var errors = jqXhr.responseJSON;

                    $.each(errors.errors, function (index, value) {
                        $('#website-error').html(value);
                    });
                }
            });
        });

        var update = $('.modal-footer').on('click', '.update', function () {
            $('#button').show();
            $('#name-error').html('');
            $('#simpan').text('Perbarui');

            var url = $('input[name=url]').val();
            var name = $('input[name=name]').val();
            var type = $('input[name="type"]:checked').val();
            var attribute1 = $('input[name=attribute1]').val();
            // var attribute2 = $('input[name=attribute2]').val();
            var triggerid = $('input[name=id]').val();
            var title = $('input[name=title]').val();
            var picture = $('input[name=picture]').val();
            var description = $('input[name=description]').val();
            var registration = $('input[name=registration]').val();
            var period = $('input[name=period]').val();
            var website_id = $('#website2').val();
            var attribute_id = $('#attribute_id').val();
            var additional1 = $('input[name=additional1]').val();
            var additional2 = $('input[name=additional2]').val();
            var additional3 = $('input[name=additional3]').val();
            var additional4 = $('input[name=additional4]').val();
            var additional5 = $('input[name=additional5]').val();


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'put',
                url: '/scholarship-attribute/' + triggerid,
                data: {
                    _token: $('input[name=_token]').val(),
                    url: url,
                    name: name,
                    type: type,
                    attribute_id: attribute_id,
                    attribute1: attribute1,
                    // attribute2: attribute2,
                    title: title,
                    picture: picture,
                    description: description,
                    registration: registration,
                    period: period,
                    website_id: website_id,
                    attribute_additional1: additional1,
                    attribute_additional2: additional2,
                    attribute_additional3: additional3,
                    attribute_additional4: additional4,
                    attribute_additional5: additional5,

                },
                success: function (data) {
                    if (data.errors) {
                        if (data.errors.url) {
                            $('#url-error').html(data.errors.code[0]);

                        }
                        if (data.errors.name) {
                            $('#name-error').html(data.errors.name[0]);

                        }

                        if (data.errors.attribute1) {
                            $('#attribute1-error').html(data.errors.type[0]);

                        }

                        if (data.errors.attribute2) {
                            $('#attribute2-error').html(data.errors.level[0]);

                        }

                        if (data.errors.type) {
                            $('#type-error').html(data.errors.description[0]);

                        }

                        document.getElementById('url').value = url;
                        document.getElementById('name').value = name;
                        if (type == 2) {
                            document.getElementById("type2").checked = true;
                        } else {
                            document.getElementById("type3").checked = true;
                        }
                        document.getElementById('attribute1').value = attribute1;
                        // document.getElementById('attribute2').value = attribute2;
                        document.getElementById('picture').value = attribute_picture;
                        document.getElementById('title').value = attribute_title;
                        document.getElementById('description').value = attribute_description;
                        document.getElementById('registration').value = attribute_registration;
                        document.getElementById('period').value = attribute_period;

                    } else {
                        $('#modal_attribute').modal('hide');

                        toastr.success('Data berhasil disimpan.', 'Sukses', {
                            timeOut: 5000
                        });

                        var table = $('.m_datatable').mDatatable();

                        table.originalDataSet = [];
                        table.reload();

                        $('#url-error').html('');
                        $('#name-error').html('');
                        $('#type-error').html('');
                        $('#attribute1-error').html('');
                        $('#attribute2-error').html('');
                    }
                }
            });
        });

        var remove = $('.m_datatable').on('click', '.delete', function () {
            var triggerid = $(this).data('id');

            swal({
                title: 'Are you sure?',
                text: 'You will not be able to recover this imaginary file!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, keep it'
            }).then(result => {
                if (result.value) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr(
                                'content'
                            )
                        },
                        type: 'DELETE',
                        url: '/scholarship-attribute/' + triggerid + '',
                        success: function (data) {
                            toastr.success(
                                'Data berhasil dihapus.',
                                'Sukses', {
                                    timeOut: 5000
                                }
                            );

                            var table = $('.m_datatable').mDatatable();

                            table.originalDataSet = [];
                            table.reload();
                        },
                        error: function (jqXhr, json, errorThrown) {
                            var errorsHtml = '';
                            var errors = jqXhr.responseJSON;

                            $.each(errors.errors, function (index, value) {
                                $('#delete-error').html(value);
                            });
                        }
                    });
                    swal(
                        'Deleted!',
                        'Your imaginary file has been deleted.',
                        'success'
                    );
                } else {
                    swal(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });


    }
};

jQuery(document).ready(function () {
    Attribute.init();
});

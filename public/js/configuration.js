var Configuration = {
    init: function () {
        $('.m_datatable').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'GET',
                        url: '/get-configurations',
                        map: function (raw) {
                            var dataSet = raw;

                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }

                            return dataSet;
                        }
                    }
                },
                pageSize: 10,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: !0
            },
            layout: {
                theme: 'default',
                class: '',
                scroll: false,
                footer: !1
            },
            sortable: !0,
            filterable: !1,
            pagination: !0,
            search: {
                input: $('#generalSearch')
            },
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50, 100]
                    }
                }
            },
            columns: [{
                    field: 'id',
                    title: '#',
                    sortable: 'asc',
                    filterable: !1,
                    width: 60
                },
                {
                    field: 'name',
                    title: 'Name',
                    sortable: 'asc',
                    filterable: !1,
                    width: 150
                },
                {
                    field: 'value',
                    title: 'Value',
                    sortable: 'asc',
                    filterable: !1,
                    width: 150,
                },
                {
                    field: 'description',
                    title: 'Description',
                    sortable: 'asc',
                    filterable: !1,
                    width: 150
                },
                {
                    field: 'of',
                    title: 'Of',
                    sortable: 'asc',
                    filterable: !1,
                    width: 150
                },
                {
                    field: 'Actions',
                    width: 110,
                    title: 'Actions',
                    sortable: !1,
                    overflow: 'visible',
                    template: function (t, e, i) {
                        return (
                            '<button data-toggle="modal" data-target="#modal_configuration" type="button" href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill edit" title="Edit" data-id=' +
                            t.id +
                            '>\t\t\t\t\t\t\t<i class="la la-pencil"></i>\t\t\t\t\t\t</button>\t\t\t\t\t\t' +
                            '\t\t\t\t\t\t\t<a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill  delete" href="#" data-id=' +
                            t.id +
                            ' title="Delete"><i class="la la-trash"></i> </a>\t\t\t\t\t\t\t'
                        );
                    }
                }
            ]
        });

        var save = $('.align-items-center').on('click', '.btn-primary', function () {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'get',
                url: '/submit-button',
                success: function (data) {
                    $('#button-div').html(data);

                }
            });

            document.getElementById('value').value = "";
            document.getElementById('name').value = "";
            document.getElementById('description').value = "";

        });


        var simpan = $('.modal-footer').on('click', '.add', function () {
            $('#simpan').text('Simpan');

            var registerForm = $('#CustomerForm');
            var name = $('input[name=name]').val();
            var value = $('input[name=value]').val();
            var description = $('#description').val();
            var formData = registerForm.serialize();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'post',
                url: '/configuration',
                data: {
                    _token: $('input[name=_token]').val(),
                    name: name,
                    value: value,
                    description: description
                },
                success: function (data) {
                    if (data.errors) {
                        if (data.errors.name) {
                            $('#name-error').html(data.errors.name[0]);


                            document.getElementById('value').value = value;
                            document.getElementById('name').value = name;

                            document.getElementById('description').value = description;
                        }


                        if (data.errors.value) {
                            $('#value-error').html(data.errors.value[0]);

                            document.getElementById('value').value = value;
                            document.getElementById('name').value = name;

                            document.getElementById('description').value = description;

                        }

                        if (data.errors.description) {
                            $('#description-error').html(data.errors.description[0]);

                            document.getElementById('value').value = value;
                            document.getElementById('name').value = name;

                            document.getElementById('description').value = description;
                        }

                    } else {
                        $('#modal_configuration').modal('hide');

                        toastr.success('Data berhasil disimpan.', 'Sukses', {
                            timeOut: 5000
                        });

                        $('#value-error').html('');
                        $('#name-error').html('');
                        $('#description-error').html('');

                        var table = $('.m_datatable').mDatatable();

                        table.originalDataSet = [];
                        table.reload();
                    }
                }
            });
        });

        var edit = $('.m_datatable').on('click', '.edit', function () {
            $('#button').show();
            $('#simpan').text('Perbarui');

            var triggerid = $(this).data('id');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'get',
                url: '/configuration/' + triggerid + '/edit',
                success: function (data) {

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'get',
                        url: '/update-button',
                        success: function (data) {
                            $('#button-div').html(data);

                        }
                    });

                    document.getElementById('id').value = data.id;
                    document.getElementById('value').value = data.value;
                    document.getElementById('name').value = data.name;
                    document.getElementById('description').value = data.description;
                },
                error: function (jqXhr, json, errorThrown) {
                    var errorsHtml = '';
                    var errors = jqXhr.responseJSON;

                    $.each(errors.errors, function (index, value) {
                        $('#configuration-error').html(value);
                    });
                }
            });
        });

        var update = $('.modal-footer').on('click', '.update', function () {
            $('#button').show();
            $('#name-error').html('');
            $('#simpan').text('Perbarui');

            var name = $('input[name=name]').val();
            var value = $('input[name=value]').val();
            var description = $('#description').val();
            var triggerid = $('input[name=id]').val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'put',
                url: '/configuration/' + triggerid,
                data: {
                    _token: $('input[name=_token]').val(),
                    name: name,
                    value: value,
                    description: description
                },
                success: function (data) {
                    if (data.errors) {
                        if (data.errors.value) {
                            $('#value-error').html(data.errors.value[0]);

                            document.getElementById('value').value = value;
                            document.getElementById('name').value = name;

                            document.getElementById('description').value = description;
                        }

                        if (data.errors.name) {
                            $('#name-error').html(data.errors.name[0]);

                            document.getElementById('value').value = value;
                            document.getElementById('name').value = name;

                            document.getElementById('description').value = description;
                        }

                        if (data.errors.description) {
                            $('#description-error').html(data.errors.description[0]);

                            document.getElementById('value').value = value;
                            document.getElementById('name').value = name;

                            document.getElementById('description').value = description;
                        }

                    } else {
                        $('#modal_configuration').modal('hide');

                        toastr.success('Data berhasil disimpan.', 'Sukses', {
                            timeOut: 5000
                        });

                        var table = $('.m_datatable').mDatatable();

                        table.originalDataSet = [];
                        table.reload();

                        $('#value-error').html('');
                        $('#name-error').html('');
                        $('#description-error').html('');
                    }
                }
            });
        });

        var remove = $('.m_datatable').on('click', '.delete', function () {
            var triggerid = $(this).data('id');

            swal({
                title: 'Are you sure?',
                text: 'You will not be able to recover this imaginary file!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, keep it'
            }).then(result => {
                if (result.value) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr(
                                'content'
                            )
                        },
                        type: 'DELETE',
                        url: '/configuration/' + triggerid + '',
                        success: function (data) {
                            toastr.success(
                                'Data berhasil dihapus.',
                                'Sukses', {
                                    timeOut: 5000
                                }
                            );

                            var table = $('.m_datatable').mDatatable();

                            table.originalDataSet = [];
                            table.reload();
                        },
                        error: function (jqXhr, json, errorThrown) {
                            var errorsHtml = '';
                            var errors = jqXhr.responseJSON;

                            $.each(errors.errors, function (index, value) {
                                $('#delete-error').html(value);
                            });
                        }
                    });
                    swal(
                        'Deleted!',
                        'Your imaginary file has been deleted.',
                        'success'
                    );
                } else {
                    swal(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });

        $('#modal_customer').on('hidden.bs.modal', function (e) {
            $(this).find('#CustomerForm')[0].reset();

            $('#name-error').html('');
        });
    }
};

jQuery(document).ready(function () {
    Configuration.init();
});

$(document).ready(function () {
    website = function () {
        $.ajax({
            url: '/get-fill-websites/',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                let angka = 1;

                $('select[name="website"]').empty();

                $.each(data, function (key, value) {
                    if (angka == 1) {
                        $('select[name="website"]').append(
                            '<option value="0"> None </option>'
                        );

                        angka = 0;
                    }

                    $('select[name="website"]').append(
                        '<option value="' + key + '">' + value + '</option>'
                    );
                });
            }
        });
    };

    website();
});

$(document).ready(function () {
    website2 = function () {
        $.ajax({
            url: '/get-fill-websites/',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                let angka = 1;

                $('select[name="website2"]').empty();

                $.each(data, function (key, value) {
                    if (angka == 1) {
                        $('select[name="website2"]').append(
                            '<option> Select a Website</option>'
                        );

                        angka = 0;
                    }

                    $('select[name="website2"]').append(
                        '<option value="' + key + '">' + value + '</option>'
                    );
                });
            }
        });
    };

    website2();
});

$(document).ready(function () {
    website3 = function () {
        $.ajax({
            url: '/get-fill-websites/',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                let angka = 1;

                $('select[name="website_id"]').empty();

                $.each(data, function (key, value) {
                    if (angka == 1) {
                        $('select[name="website_id"]').append(
                            '<option> Select a Website</option>'
                        );

                        angka = 0;
                    }

                    $('select[name="website_id"]').append(
                        '<option value="' + key + '">' + value + '</option>'
                    );
                });
            }
        });
    };

    website3();
});

$(document).ready(function () {
    attribute = function () {
        $.ajax({
            url: '/get-fill-attributes/',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                let angka = 1;

                $('select[name="attribute_id"]').empty();

                $.each(data, function (key, value) {
                    if (angka == 1) {
                        $('select[name="attribute_id"]').append(
                            '<option value="0"> None </option>'
                        );

                        angka = 0;
                    }

                    $('select[name="attribute_id"]').append(
                        '<option value="' + key + '">' + value + '</option>'
                    );
                });
            }
        });
    };

    attribute();
});

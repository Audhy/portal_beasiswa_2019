var Scholarship = {
    init: function () {
        function strtrunc(str, max, add){
            add = add || '...';
            return (typeof str === 'string' && str.length > max ? str.substring(0, max) + add : str);
         };

        $('.m_datatable').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'GET',
                        url: '/get-scholarships',
                        map: function (raw) {
                            var dataSet = raw;

                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }

                            return dataSet;
                        }
                    }
                },
                pageSize: 10,
                serverPaging: !1,
                serverFiltering: !0,
                serverSorting: !0
            },
            layout: {
                theme: 'default',
                class: '',
                scroll: false,
                footer: !1
            },
            sortable: !0,
            filterable: !1,
            pagination: !0,
            search: {
                input: $('#generalSearch')
            },
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50, 100]
                    }
                }
            },
            columns: [
                {
                    field: 'id',
                    title: '#',
                    sortable: 'asc',
                    filterable: !1,
                    width: 60
                },
                {
                    field: 'name',
                    title: 'Name',
                    sortable: 'asc',
                    filterable: !1,
                    width: 150
                },
                {
                    field: 'website_id',
                    title: 'Website',
                    sortable: 'asc',
                    filterable: !1,
                    width: 150,
                },
                {
                    field: 'uri',
                    title: 'URI',
                    sortable: 'asc',
                    filterable: !1,
                    width: 150
                },
                {
                    field: 'description',
                    title: 'Description',
                    sortable: 'asc',
                    filterable: !1,
                    width: 150,
                    template: function (t) {
                        data = strtrunc(t.description, 50);
                        return(
                            // {!!str_limit($scholarship->description, 355,' ...')!!};
                            '<p>'+data+'</p>'
                        );
                    }
                    // {!!str_limit($scholarship->description, 355,' ...')!!}
                },
                {
                    field: 'Actions',
                    width: 110,
                    title: 'Actions',
                    sortable: !1,
                    overflow: 'visible',
                    template: function (t, e, i) {
                        return (
                            '<button data-toggle="modal" data-target="#modal_scholarship" type="button" href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill edit" title="Edit" data-id=' +
                            t.id +
                            '>\t\t\t\t\t\t\t<i class="la la-pencil"></i>\t\t\t\t\t\t</button>\t\t\t\t\t\t' +
                            '\t\t\t\t\t\t\t<a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill  delete" href="#" data-id=' +
                            t.id +
                            ' title="Delete"><i class="la la-trash"></i> </a>\t\t\t\t\t\t\t'
                        );
                    }
                }
            ]
        });

        var save = $('.align-items-center').on('click', '.btn-primary', function () {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'get',
                url: '/submit-button',
                success: function (data) {
                    $('#button-div').html(data);

                }
            });

            document.getElementById('uri').value = "";
            document.getElementById('name').value = "";
            document.getElementById('description').value = "";

        });


        var simpan = $('.modal-footer').on('click', '.add', function () {
            $('#name-error').html("");
            $('#uri-error').html("");
            $('#website_id-error').html("");

            $('#simpan').text('Simpan');
            var uri = $('input[name=uri]').val();
            var name = $('input[name=name]').val();
            var period = $('input[name=period]').val();
            var description = $('#description').val();
            var website_id = $('#website2').val();


            var fd = new FormData();
            fd.append( "fileInput", $("#poster")[0].files[0]);
            fd.append("uri", uri);
            fd.append("name", name);
            fd.append("description", description);
            fd.append("website_id",website_id);
            fd.append("period",period);
            var registerForm = $('#CustomerForm');
            var formData = registerForm.serialize();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                processData: false,
                contentType: false,
                type: 'post',
                url: '/scholarship',
                data : fd,
                // data: {
                //     // poster :poster
                //     _token: $('input[name=_token]').val(),
                //     uri: uri,
                //     name: name,
                //     poster: poster,
                //     description: description
                // },
                success: function (data) {
                    if (data.errors) {
                        if (data.errors.name) {
                            $('#name-error').html(data.errors.name[0]);

                        }

                        if (data.errors.uri) {
                            $('#uri-error').html(data.errors.uri[0]);

                        }
                        if (data.errors.period) {
                            $('#period-error').html(data.errors.period[0]);

                        }
                        if (website_id == 'Select a Website') {
                            $('#website_id-error').html('The website2 field is required.');

                        }

                        document.getElementById('uri').value = uri;
                        document.getElementById('name').value = name;
                        document.getElementById('period').value = period;

                        document.getElementById('description').value = description;

                    } else {
                        $('#modal_scholarship').modal('hide');

                        toastr.success('Data berhasil disimpan.', 'Sukses', {
                            timeOut: 5000
                        });

                        $('input[type=file]').val("") ;
                        document.getElementById('poster-label').innerHTML = '';
                        $('#uri-error').html('');
                        $('#name-error').html('');
                        $('#description-error').html('');

                        var table = $('.m_datatable').mDatatable();

                        table.originalDataSet = [];
                        table.reload();
                    }
                }
            });
        });

        var edit = $('.m_datatable').on('click', '.edit', function () {

            $('#button').show();
            $('#simpan').text('Perbarui');

            var triggerid = $(this).data('id');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'get',
                url: '/scholarship/' + triggerid + '/edit',
                success: function (data) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'get',
                        url: '/update-button',
                        success: function (data) {
                            $('#button-div').html(data);

                        }
                    });

                    $('select[name="website2"]').empty();

                    $.ajax({
                        url: '/get-fill-websites/',
                        type: 'GET',
                        dataType: 'json',
                        success: function (data) {
                            let angka = 1;

                            $('select[name="website2"]').empty();

                            $.each(data, function (key, value) {
                                if(key == data.website_id){
                                    $('select[name="website2"]').append(
                                        '<option value="' + key + '" selected>' + value + '</option>'
                                    );
                                }
                                else{
                                    $('select[name="website2"]').append(
                                        '<option value="' + key + '">' + value + '</option>'
                                    );
                                }

                            });
                        }
                    });
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'get',
                        url: '/period/' + triggerid + '/edit/',
                        success: function (data) {
                            document.getElementById('period').value = data.period;
                        },
                    });

                    document.getElementById('id').value = data.id;
                    document.getElementById('uri').value = data.uri;
                    document.getElementById('name').value = data.name;
                    document.getElementById('description').value = data.description;
                },
                error: function (jqXhr, json, errorThrown) {
                    var errorsHtml = '';
                    var errors = jqXhr.responseJSON;

                    $.each(errors.errors, function (index, value) {
                        $('#scholarship-error').html(value);
                    });
                }
            });

        });

        var update = $('.modal-footer').on('click', '.update', function () {
            $('#button').show();
            $('#name-error').html("");
            $('#uri-error').html("");
            $('#website_id-error').html("");
            $('#simpan').text('Perbarui');

            var uri = $('input[name=uri]').val();
            var name = $('input[name=name]').val();
            var period = $('input[name=period]').val();
            var description = $('#description').val();
            var website_id = $('#website2').val();
            var triggerid = $('input[name=id]').val();

            var fd = new FormData();
            // fd.append('_method', 'PATCH');
            fd.append( "fileInput", $("#poster")[0].files[0]);
            fd.append("uri", uri);
            fd.append("name", name);
            fd.append("description", description);
            fd.append("website_id",website_id);
            fd.append("period",period);
            // var registerForm = $('#CustomerForm');
            // var formData = registerForm.serialize();

            // console.log(fd);
            // for (var value of fd.values()) {
            //     console.log(value);
            //  }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                processData: false,
                contentType: false,
                type: 'POST',
                url: '/scholarship-edit/'+triggerid,
                data : fd,

                // headers: {
                //     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                // },
                // processData: false,
                // contentType: false,
                // type: 'put',
                // url: '/scholarship/' + triggerid,
                // data : fd,
                // data: {
                //     _token: $('input[name=_token]').val(),
                //     uri: uri,
                //     name: name,
                //     period: period,
                //     website_id: website_id,
                //     description: description
                // },
                success: function (data) {
                    if (data.errors) {
                        if (data.errors.name) {
                            $('#name-error').html(data.errors.name[0]);

                        }

                        if (data.errors.uri) {
                            $('#uri-error').html(data.errors.uri[0]);

                        }

                        if (data.errors.website2) {
                            $('#website_id-error').html(data.errors.website2[0]);

                        }
                        if (data.errors.period) {
                            $('#period-error').html(data.errors.period[0]);

                        }


                        if (website_id == 'Select a Website') {
                            $('#website_id-error').html('The website2 field is required.');

                        }
                        document.getElementById('uri').value = uri;
                        document.getElementById('name').value = name;
                        document.getElementById('period').value = period;

                        document.getElementById('description').value = description;

                    } else {
                        $('#modal_scholarship').modal('hide');

                        toastr.success('Data berhasil disimpan.', 'Sukses', {
                            timeOut: 5000
                        });

                        var table = $('.m_datatable').mDatatable();

                        table.originalDataSet = [];
                        table.reload();

                        $('#uri-error').html('');
                        $('#name-error').html('');
                        $('#description-error').html('');
                    }
                }
            });
        });

        var remove = $('.m_datatable').on('click', '.delete', function () {
            var triggerid = $(this).data('id');

            swal({
                title: 'Are you sure?',
                text: 'You will not be able to recover this imaginary file!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, keep it'
            }).then(result => {
                if (result.value) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr(
                                'content'
                            )
                        },
                        type: 'DELETE',
                        url: '/scholarship/' + triggerid + '',
                        success: function (data) {
                            toastr.success(
                                'Data berhasil dihapus.',
                                'Sukses', {
                                    timeOut: 5000
                                }
                            );

                            var table = $('.m_datatable').mDatatable();

                            table.originalDataSet = [];
                            table.reload();
                        },
                        error: function (jqXhr, json, errorThrown) {
                            var errorsHtml = '';
                            var errors = jqXhr.responseJSON;

                            $.each(errors.errors, function (index, value) {
                                $('#delete-error').html(value);
                            });
                        }
                    });
                    swal(
                        'Deleted!',
                        'Your imaginary file has been deleted.',
                        'success'
                    );
                } else {
                    swal(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });

        $('#modal_customer').on('hidden.bs.modal', function (e) {
            $(this).find('#CustomerForm')[0].reset();

            $('#name-error').html('');
        });
    }
};

jQuery(document).ready(function () {
    Scholarship.init();
});

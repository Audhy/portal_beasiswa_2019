var Website = {
    init: function () {
        $('.m_datatable').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'GET',
                        url: '/get-websites',
                        map: function (raw) {
                            var dataSet = raw;

                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }

                            return dataSet;
                        }
                    }
                },
                pageSize: 10,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: !0
            },
            layout: {
                theme: 'default',
                class: '',
                scroll: false,
                footer: !1
            },
            sortable: !0,
            filterable: !1,
            pagination: !0,
            search: {
                input: $('#generalSearch')
            },
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50, 100]
                    }
                }
            },
            columns: [
                {
                    field: 'id',
                    title: '#',
                    sortable: 'asc',
                    filterable: !1,
                    width: 60
                },
                {
                    field: 'name',
                    title: 'Name',
                    sortable: 'asc',
                    filterable: !1,
                    width: 150
                },
                {
                    field: 'url',
                    title: 'URL',
                    sortable: 'asc',
                    filterable: !1,
                    width: 150,
                },
                {
                    field: 'status',
                    title: 'Type',
                    sortable: 'asc',
                    filterable: !1,
                    width: 150
                },
                {
                    field: 'Actions',
                    width: 110,
                    title: 'Actions',
                    sortable: !1,
                    overflow: 'visible',
                    template: function (t, e, i) {
                        return (
                            '<button data-toggle="modal" data-target="#modal_website" type="button" href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill edit" title="Edit" data-id=' +
                            t.id +
                            '>\t\t\t\t\t\t\t<i class="la la-pencil"></i>\t\t\t\t\t\t</button>\t\t\t\t\t\t' +
                            '\t\t\t\t\t\t\t<a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill  delete" href="#" data-id=' +
                            t.id +
                            ' title="Delete"><i class="la la-trash"></i> </a>\t\t\t\t\t\t\t'
                        );
                    }
                }
            ]
        });

        // var save = $('.align-items-center').on('click', '.btn-primary', function () {
        //     $.ajax({
        //         headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         },
        //         type: 'get',
        //         url: '/submit-button',
        //         success: function (data) {
        //             $('#button-div').html(data);

        //         }
        //     });

        //     document.getElementById('url').value = "";
        //     document.getElementById('name').value = "";
        //     document.getElementById('type').value = "";

        // });


        var simpan = $('.modal-footer').on('click', '.add', function () {
            $('#url-error').html('');
            $('#name-error').html('');
            $('#type-error').html('');

            $('#simpan').text('Simpan');

            var registerForm = $('#CustomerForm');
            var url = $('input[name=url]').val();
            var name = $('input[name=name]').val();
            var type = $('input[name="type"]:checked').val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'post',
                url: '/website',
                data: {
                    _token: $('input[name=_token]').val(),
                    url: url,
                    name: name,
                    type: type,
               },
                success: function (data) {
                    if (data.errors) {
                        if (data.errors.url) {
                            $('#url-error').html(data.errors.url[0]);

                        }
                        if (data.errors.name) {
                            $('#name-error').html(data.errors.name[0]);

                        }

                        if (data.errors.type) {
                            $('#type-error').html(data.errors.type[0]);

                        }


                        document.getElementById('url').value = url;
                        document.getElementById('name').value = name;
                        if(type == 2){
                            document.getElementById("type2").checked = true;
                        }
                        else if(type == 3){
                            document.getElementById("type3").checked = true;
                        }

                    } else {
                        $('#modal_website').modal('hide');

                        toastr.success('Data berhasil disimpan.', 'Sukses', {
                            timeOut: 5000
                        });

                        $('#url-error').html('');
                        $('#name-error').html('');
                        $('#type-error').html('');
                        $('#attribute1-error').html('');
                        $('#attribute2-error').html('');

                        var table = $('.m_datatable').mDatatable();

                        table.originalDataSet = [];
                        table.reload();
                    }
                }
            });
        });


        var simpan = $('.modal-footer').on('click', '.try', function () {
            $('#simpan').text('Simpan');

            var registerForm = $('#CustomerForm');
            var url = $('input[name=url]').val();
            var name = $('input[name=name]').val();
            var type = $('input[name="type"]:checked').val();
            // var type = $('#type').val();
            var attribute1 = $('input[name=attribute1]').val();
            var attribute2 = $('input[name=attribute2]').val();
            var title = $('input[name=title]').val();
            var picture = $('input[name=picture]').val();
            var description = $('input[name=description]').val();
            var registration = $('input[name=registration]').val();
            var period = $('input[name=period]').val();

            var formData = registerForm.serialize();
            // alert(type);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'post',
                url: '/testing',
                data: {
                    _token: $('input[name=_token]').val(),
                    url: url,
                    name: name,
                    type: type,
                    attribute1: attribute1,
                    attribute2: attribute2,
                    title: title,
                    picture: picture,
                    description: description,
                    registration: registration,
                    period: period,

               },
                success: function (data) {
                    if (data.errors) {
                        if (data.errors.url) {
                            $('#url-error').html(data.errors.code[0]);


                            document.getElementById('url').value = url;
                            document.getElementById('name').value = name;
                            if(type == 2){
                                document.getElementById("type2").checked = true;
                            }
                            else{
                                document.getElementById("type3").checked = true;
                            }
                            document.getElementById('attribute1').value = attribute1;
                            document.getElementById('attribute2').value = attribute2;
                        }


                    } else {
                        $('#modal_website').modal('hide');

                        toastr.success('Data berhasil disimpan.', 'Sukses', {
                            timeOut: 5000
                        });

                        $('#name-error').html('');
                        $('#attribute1-error').html('');
                        $('#attribute2-error').html('');

                        // var table = $('.m_datatable').mDatatable();

                        // table.originalDataSet = [];
                        // table.reload();
                    }
                }
            });
        });

        var edit = $('.m_datatable').on('click', '.edit', function () {
            $('#button').show();
            $('#simpan').text('Perbarui');

            var triggerid = $(this).data('id');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'get',
                url: '/website/' + triggerid + '/edit',
                success: function (data) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'get',
                        url: '/update-button',
                        success: function (data) {
                            $('#button-div').html(data);

                        }
                    });

                    document.getElementById('id').value = data.id;
                    document.getElementById('url').value = data.url;
                    document.getElementById('name').value = data.name;
                    if(data.status == 2){
                        document.getElementById("type2").checked = true;
                    }
                    else{
                        document.getElementById("type3").checked = true;
                    }
                },
                error: function (jqXhr, json, errorThrown) {
                    var errorsHtml = '';
                    var errors = jqXhr.responseJSON;

                    $.each(errors.errors, function (index, value) {
                        $('#website-error').html(value);
                    });
                }
            });
        });
        var reset = $('.modal-footer').on('click', '.reset', function () {
            // alert('reset');
            // $('#url-error').html('');
            // $('#name-error').html('');
            // $('#type-error').html('');
            // document.getElementById('url').value = "";
            // document.getElementById('name').value = "";
            // document.getElementById("type2").checked = false;
            // document.getElementById("type3").checked = false;

        });
        var update = $('.modal-footer').on('click', '.update', function () {
            $('#url-error').html('');
            $('#name-error').html('');
            $('#type-error').html('');

            $('#button').show();
            $('#simpan').text('Perbarui');

            var url = $('input[name=url]').val();
            var name = $('input[name=name]').val();
            var type = $('input[name="type"]:checked').val();
            var triggerid = $('input[name=id]').val();
            // var attribute1 = $('input[name=attribute1]').val();
            // var attribute2 = $('input[name=attribute2]').val();
            // var triggerid = $('input[name=id]').val();
            // var title = $('input[name=title]').val();
            // var picture = $('input[name=picture]').val();
            // var description = $('input[name=description]').val();
            // var registration = $('input[name=registration]').val();
            // var period = $('input[name=period]').val();


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'put',
                url: '/website/' + triggerid,
                data: {
                    _token: $('input[name=_token]').val(),
                    url: url,
                    name: name,
                    type: type,
                    // attribute1: attribute1,
                    // attribute2: attribute2,
                    // title: title,
                    // picture: picture,
                    // description: description,
                    // registration: registration,
                    // period: period,

                },
                success: function (data) {
                    if (data.errors) {
                        if (data.errors.url) {
                            $('#url-error').html(data.errors.url[0]);

                        }
                        if (data.errors.name) {
                            $('#name-error').html(data.errors.name[0]);

                        }

                        if (data.errors.type) {
                            $('#type-error').html(data.errors.type[0]);

                        }


                        document.getElementById('url').value = url;
                        document.getElementById('name').value = name;
                        if(type == 2){
                            document.getElementById("type2").checked = true;
                        }
                        else if(type == 3){
                            document.getElementById("type3").checked = true;
                        }

                    } else {
                        $('#modal_website').modal('hide');

                        toastr.success('Data berhasil disimpan.', 'Sukses', {
                            timeOut: 5000
                        });

                        var table = $('.m_datatable').mDatatable();

                        table.originalDataSet = [];
                        table.reload();

                        $('#url-error').html('');
                        $('#name-error').html('');
                        $('#type-error').html('');
                        $('#attribute1-error').html('');
                        $('#attribute2-error').html('');
                    }
                }
            });
        });

        var remove = $('.m_datatable').on('click', '.delete', function () {
            var triggerid = $(this).data('id');

            swal({
                title: 'Are you sure?',
                text: 'You will not be able to recover this imaginary file!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, keep it'
            }).then(result => {
                if (result.value) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr(
                                'content'
                            )
                        },
                        type: 'DELETE',
                        url: '/website/' + triggerid + '',
                        success: function (data) {
                            toastr.success(
                                'Data berhasil dihapus.',
                                'Sukses', {
                                    timeOut: 5000
                                }
                            );

                            var table = $('.m_datatable').mDatatable();

                            table.originalDataSet = [];
                            table.reload();
                        },
                        error: function (jqXhr, json, errorThrown) {
                            var errorsHtml = '';
                            var errors = jqXhr.responseJSON;

                            $.each(errors.errors, function (index, value) {
                                $('#delete-error').html(value);
                            });
                        }
                    });
                    swal(
                        'Deleted!',
                        'Your imaginary file has been deleted.',
                        'success'
                    );
                } else {
                    swal(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });

    }
};

jQuery(document).ready(function () {
    Website.init();
});

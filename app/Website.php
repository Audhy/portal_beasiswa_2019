<?php

namespace App;

use App\Scholarship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Website extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'url',
        'attribute1',
        'attribute2',
        'status',
        'attribute_title',
        'attribute_picture',
        'attribute_description',
        'attribute_registration',
        'attribute_period',
    ];

    /*************************************** RELATIONSHIP ****************************************/

    /**
     * One-to-Many
     *
     * @return mixed
     */
    public function scholarships()
    {
        // This method must have a second parameter as FK column (website_id),
        // so these following error will not thrown:
        // "Too few arguments to function Illuminate\Database\Eloquent\Model::setAttribute()"

        return $this->hasMany(Scholarship::class, 'website_id');
    }

        /**
     * One-to-Many
     *
     * @return mixed
     */
    public function scholarship_attributes()
    {
        // This method must have a second parameter as FK column (website_id),
        // so these following error will not thrown:
        // "Too few arguments to function Illuminate\Database\Eloquent\Model::setAttribute()"

        return $this->hasMany(Scholarship_attribute::class, 'website_id');
    }


}

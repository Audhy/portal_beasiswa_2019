<?php

namespace App\Http\Controllers;

use App\Log;
use App\Period;
use App\Configuration;
use App\Scholarship;
use App\Scholarship_attribute;
use App\Website;
use Goutte\Client;
use Illuminate\Http\Request;

class ScaperController extends Controller
{
    public function year()
    {
        $period = date("Y");

        echo substr($period, 0, 2);

        // abcd

        // $str = 'Please message mpg of jazz to my mobile number +123456789';
        // if (
        //     (!empty($str) && preg_match_all('~\b\d{4}\b\+?~', $str, $matches))
        // ) {
        //     $value = max($matches[0]);
        //     dd($value);

        // } else {
        //     dd(date("Y"));
        // }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function google()
    {
        try {

            $Websites = Website::pluck('url');
            if ($Websites->isEmpty()) {
                $client = new Client();
                $crawler = $client->request('GET', 'https://google.com/search?q=beasiswa');
                $crawler->filter('cite')->each(function ($node) {
                    $web = str_replace(array('https://', 'www.'), '', $node->text());
                    $Website = Website::create([
                        'url' => $web,
                        'status' => '1',
                    ]);
                });
            } else {
                $threshold = Configuration::find(1);
                $threshold = $threshold->value;
                $client = new Client();
                $crawler = $client->request('GET', 'https://google.com/search?q=beasiswa');
                $urls = array();
                $matchs = array();
                $crawler->filter('cite')->each(function ($node) use (&$threshold, &$urls, &$matchs) {
                    $Websites = Website::pluck('url');
                    foreach ($Websites as $Website) {
                        $comparison = new \Atomescrochus\StringSimilarities\Compare();
                        $web = str_replace(array('https://', 'www.'), '', $node->text());
                        $jaroWinkler = $comparison->jaroWinkler($Website, $web);

                        $urls[] = $node->text();
                        if ($jaroWinkler >= $threshold) {
                            $matchs[] = $node->text();
                        }
                    }
                });

                $MatchArray = array_unique($matchs);
                $UrlArray = array_unique($urls);
                $diffs = array_diff($UrlArray, $MatchArray);
                foreach ($diffs as $diff) {
                    $web = str_replace(array('https://', 'www.'), '', $diff);
                    $Website = Website::create([
                        'url' => $web,
                        'status' => '1',
                    ]);
                }
            }

            Log::create([
                'type' => 1,
                'status' => 1,
            ]);

            // return response()->json();
            echo 'Done';

        } catch (\Exception $e) {

            if (strpos($e, 'timed out') !== false) {
                Log::create([
                    'type' => 1,
                    'status' => 0,
                    'message' => 'Time Out, Maybe Website Down or Your connection get trouble',
                ]);

                echo 'Timeout '.'<br>'.$e;
            }
            else{
                Log::create([
                    'type' => 1,
                    'status' => 0,
                    'message' => 'Unknow Error please Check Scrap Website Manually',
                ]);

                echo 'Error - '.$e;

            }

        }

        // echo 'Done';

    }

    public function website()
    {
        try {

            $Scholarship = Scholarship::pluck('description');
            if ($Scholarship->isEmpty()) {
                $attributes = Scholarship_attribute::with('website')->get();
                foreach ($attributes as $attribute) {

                    if ($attribute->website->status == 2) {
                        $client = new Client();

                        $attribute_id = $attribute->id;
                        $website_id = $attribute->website_id;
                        $attribute_url = $attribute->url;
                        $attribute_title = $attribute->attribute_title;
                        $attribute_desc = $attribute->attribute_description;
                        $attribute_picture = $attribute->attribute_picture;
                        $attribute_registration = $attribute->attribute_registration;
                        $attribute_period = $attribute->attribute_period;
                        $attribute_additional1 = $attribute->attribute_additional_1;
                        $attribute_additional2 = $attribute->attribute_additional_2;
                        $attribute_additional3 = $attribute->attribute_additional_3;
                        $attribute_additional4 = $attribute->attribute_additional_4;
                        $attribute_additional5 = $attribute->attribute_additional_5;

                        $guzzleclient = new \GuzzleHttp\Client(array(
                            'timeout' => 90,
                            'verify' => false,
                        ));
                        $client->setClient($guzzleclient);
                        $crawler = $client->request('GET', $attribute->url);

                        $crawler->filter($attribute->attribute1)->each(function ($node) use (&$attribute_desc, &$attribute_title, &$attribute_picture, &$attribute_url, &$attribute_website, &$attribute_id, &$website_id, &$attribute_registration, &$attribute_additional1, &$attribute_additional2, $attribute_additional3, $attribute_additional4, $attribute_additional5) {
                            $link = $node->attr('href');
                            $client = new Client();
                            $crawlerdtl = $client->request('GET', $link);
                            $title = '';
                            if ($attribute_title != null) {
                                $crawlerdtl->filter($attribute_title)->each(function ($node) use (&$title, &$attribute_title) {
                                    $title = $node->text();
                                });
                            }

                            $desc = '';
                            if ($attribute_desc != null) {
                                $crawlerdtl->filter($attribute_desc)->each(function ($node) use (&$desc, &$attribute_desc) {
                                    $desc = $desc . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $regs = '';
                            if ($attribute_registration != null) {
                                $crawlerdtl->filter("")->each(function ($node) use (&$regs, &$attribute_registration) {
                                    $regs = $node->attr('href');
                                });
                            }

                            $picture = '';
                            if ($attribute_picture != null) {
                                $crawlerdtl->filter($attribute_picture)->each(function ($node) use (&$picture, &$attribute_picture) {
                                    $picture = $node->attr('src');
                                });
                            }

                            $additional1 = '';
                            if ($attribute_additional1 != null) {
                                $crawlerdtl->filter($attribute_additional1)->each(function ($node) use (&$additional1, &$attribute_additional1) {
                                    $additional1 = $additional1 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional2 = '';
                            if ($attribute_additional2 != null) {
                                $crawlerdtl->filter($attribute_additional2)->each(function ($node) use (&$additional2, &$attribute_additional2) {
                                    $additional2 = $additional2 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional3 = '';
                            if ($attribute_additional3 != null) {
                                $crawlerdtl->filter($attribute_additional3)->each(function ($node) use (&$additional3, &$attribute_additional3) {
                                    $additional3 = $additional3 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional4 = '';
                            if ($attribute_additional4 != null) {
                                $crawlerdtl->filter($attribute_additional4)->each(function ($node) use (&$additional4, &$attribute_additional4) {
                                    $attribute_additional4 = $attribute_additional4 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional5 = '';
                            if ($attribute_additional5 != null) {
                                $crawlerdtl->filter($attribute_additional5)->each(function ($node) use (&$additional5, &$attribute_additional5) {
                                    $additional5 = $additional5 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $combine = '<p>' . $additional1 . '</p>' . '<p>' . $additional2 . '</p>' . '<p>' . $additional3 . '</p>' . '<p>' . $additional4 . '</p>' . '<p>' . $additional5 . '</p>';

                            if ($attribute_picture == null or $attribute_picture == '') {
                                $path = '/img/no_picture/' . rand(1, 2) . '.jpg';

                                $Scholarship = Scholarship::create([
                                    'name' => $title,
                                    'uri' => $link,
                                    // 'picture' => $path,
                                    'website_id' => $website_id,
                                    'attribute_id' => $attribute_id,
                                    'reg' => $regs,
                                    'description' => $desc,
                                    'additional' => $combine,

                                ]);
                            } else {

                                $scholarship = Scholarship::create([
                                    'name' => $title,
                                    'uri' => $link,
                                    'picture' => $picture,
                                    'website_id' => $website_id,
                                    'attribute_id' => $attribute_id,
                                    'reg' => $regs,
                                    'description' => $desc,
                                    'additional' => $combine,

                                ]);
                            }

                            $str = $Scholarship->description . $Scholarship->additional;
                            if (
                                (!empty($str) && preg_match_all('~\b\d{4}\b\+?~', $str, $matches))
                            ) {
                                $today = date("Y");
                                $today = substr($today, 0, 2);
                                $years = array();
                                foreach($matches[0] as $year){
                                    $test = substr($year, 0, 2);
                                    if($test == $today){
                                        $years[] = $year;
                                    }
                                }
                                if($years == null ){
                                    $period = date("Y");
                                }
                                else{
                                    $value = max($years);
                                    $period = $value;
                                }
                            } else {
                                $period = date("Y");
                            }

                            $period = Period::create([
                                'scholarship_id' => $Scholarship->id,
                                'period' => $period,
                            ]);

                        });

                    } elseif ($attribute->website->status == 3) {
                        if ($attribute->attribute_id != null) {
                            $client = new Client();
                            $attribute_id = $attribute->id;
                            $attribute_title = $attribute->attribute_title;
                            $attribute_desc = $attribute->attribute_description;
                            $attribute_picture = $attribute->attribute_picture;
                            $attribute_registration = $attribute->attribute_registration;
                            $attribute_period = $attribute->attribute_period;
                            $attribute_additional1 = $attribute->attribute_additional_1;
                            $attribute_additional2 = $attribute->attribute_additional_2;
                            $attribute_additional3 = $attribute->attribute_additional_3;
                            $attribute_additional4 = $attribute->attribute_additional_4;
                            $attribute_additional5 = $attribute->attribute_additional_5;

                            $guzzleclient = new \GuzzleHttp\Client(array(
                                'timeout' => 90,
                                'verify' => false,
                            ));
                            $client->setClient($guzzleclient);
                            $crawler = $client->request('GET', $attribute->url);

                            if ($attribute_title != null or $attribute_title != '') {
                                $crawler->filter($attribute_title)->each(function ($node) use (&$title, &$attribute_title) {
                                    $title = $node->text();
                                });
                                $Scholarship = Scholarship::where('attribute_id', $attribute->attribute_id)->first();
                                $Scholarship->name = $title;
                                $Scholarship->save();
                            }

                            if ($attribute_desc != null) {
                                $crawler->filter($attribute_desc)->each(function ($node) use (&$desc, &$attribute_desc) {
                                    $desc = $desc . '<p>' . $node->text() . '</p>';
                                });
                                $Scholarship = Scholarship::where('attribute_id', $attribute->attribute_id)->first();
                                $Scholarship->name = $desc;
                                $Scholarship->save();
                            }

                            if ($attribute_picture != null) {
                                $crawler->filter($attribute_picture)->each(function ($node) use (&$picture, &$attribute_picture) {
                                    $picture = $node->attr('src');
                                });
                                $Scholarship = Scholarship::where('attribute_id', $attribute->attribute_id)->first();
                                $Scholarship->name = $picture;
                                $Scholarship->save();
                            }

                            if ($attribute_registration != null) {
                                $crawler->filter($attribute_registration)->each(function ($node) use (&$reg, &$attribute_registration) {
                                    $reg = $node->attr('href');
                                });
                                $Scholarship = Scholarship::where('attribute_id', $attribute->attribute_id)->first();
                                $Scholarship->name = $reg;
                                $Scholarship->save();
                            }

                            $additional1 = '';
                            if ($attribute_additional1 != null) {
                                $crawler->filter($attribute_additional1)->each(function ($node) use (&$additional1, &$attribute_additional1) {
                                    $additional1 = $additional1 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional2 = '';
                            if ($attribute_additional2 != null) {
                                $crawler->filter($attribute_additional2)->each(function ($node) use (&$additional2, &$attribute_additional2) {
                                    $additional2 = $additional2 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional3 = '';
                            if ($attribute_additional3 != null) {
                                $crawler->filter($attribute_additional3)->each(function ($node) use (&$additional3, &$attribute_additional3) {
                                    $additional3 = $additional3 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional4 = '';
                            if ($attribute_additional4 != null) {
                                $crawler->filter($attribute_additional4)->each(function ($node) use (&$additional4, &$attribute_additional4) {
                                    $attribute_additional4 = $attribute_additional4 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional5 = '';
                            if ($attribute_additional5 != null) {
                                $crawler->filter($attribute_additional5)->each(function ($node) use (&$additional5, &$attribute_additional5) {
                                    $additional5 = $additional5 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $Scholarship = Scholarship::where('attribute_id', $attribute->attribute_id)->first();
                            $combine = $Scholarship->additional . '<p>' . $additional1 . '</p>' . '<p>' . $additional2 . '</p>' . '<p>' . $additional3 . '</p>' . '<p>' . $additional4 . '</p>' . '<p>' . $additional5 . '</p>';

                            $Scholarship->additional = $combine;
                            $Scholarship->save();

                            $str = $Scholarship->description . $Scholarship->additional;
                            if (
                                (!empty($str) && preg_match_all('~\b\d{4}\b\+?~', $str, $matches))
                            ) {
                                $today = date("Y");
                                $today = substr($today, 0, 2);
                                $years = array();
                                foreach($matches[0] as $year){
                                    $test = substr($year, 0, 2);
                                    if($test == $today){
                                        $years[] = $year;
                                    }
                                }
                                if($years == null ){
                                    $period = date("Y");
                                }
                                else{
                                    $value = max($years);
                                    $period = $value;
                                }
                            } else {
                                $period = date("Y");
                            }

                            $periods = Period::where('scholarship_id', $scholarship->id)->first();
                            if ($period > $periods->period) {
                                $periods->period = $period;
                                $periods->save();
                            }

                        } else {
                            $client = new Client();
                            $attribute_id = $attribute->id;
                            $attribute_title = $attribute->attribute_title;
                            $attribute_desc = $attribute->attribute_description;
                            $attribute_picture = $attribute->attribute_picture;
                            $attribute_registration = $attribute->attribute_registration;
                            $attribute_period = $attribute->attribute_period;
                            $attribute_additional1 = $attribute->attribute_additional_1;
                            $attribute_additional2 = $attribute->attribute_additional_2;
                            $attribute_additional3 = $attribute->attribute_additional_3;
                            $attribute_additional4 = $attribute->attribute_additional_4;
                            $attribute_additional5 = $attribute->attribute_additional_5;

                            $guzzleclient = new \GuzzleHttp\Client(array(
                                'timeout' => 90,
                                'verify' => false,
                            ));
                            $client->setClient($guzzleclient);
                            $crawler = $client->request('GET', $attribute->url);

                            $title = '';
                            if ($attribute_title != null or $attribute_title != '') {
                                $crawler->filter($attribute_title)->each(function ($node) use (&$title, &$attribute_title) {
                                    $title = $node->text();
                                });
                            }

                            $desc = '';
                            if ($attribute_desc != null) {
                                $crawler->filter($attribute_desc)->each(function ($node) use (&$desc, &$attribute_desc) {
                                    $desc = $desc . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $picture = '';
                            if ($attribute_picture != null) {
                                $crawler->filter($attribute_picture)->each(function ($node) use (&$picture, &$attribute_picture) {
                                    $picture = $node->attr('src');
                                });
                            }

                            $reg = '';
                            if ($attribute_registration != null) {
                                $crawler->filter($attribute_registration)->each(function ($node) use (&$reg, &$attribute_registration) {
                                    $text = $node->text();
                                    if ($text == "Daftar Online") {
                                        $reg = $node->attr('href');
                                    }
                                });
                            }

                            $additional1 = '';
                            if ($attribute_additional1 != null) {
                                $crawler->filter($attribute_additional1)->each(function ($node) use (&$additional1, &$attribute_additional1) {
                                    $additional1 = $additional1 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional2 = '';
                            if ($attribute_additional2 != null) {
                                $crawler->filter($attribute_additional2)->each(function ($node) use (&$additional2, &$attribute_additional2) {
                                    $additional2 = $additional2 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional3 = '';
                            if ($attribute_additional3 != null) {
                                $crawler->filter($attribute_additional3)->each(function ($node) use (&$additional3, &$attribute_additional3) {
                                    $additional3 = $additional3 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional4 = '';
                            if ($attribute_additional4 != null) {
                                $crawler->filter($attribute_additional4)->each(function ($node) use (&$additional4, &$attribute_additional4) {
                                    $attribute_additional4 = $attribute_additional4 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional5 = '';
                            if ($attribute_additional5 != null) {
                                $crawler->filter($attribute_additional5)->each(function ($node) use (&$additional5, &$attribute_additional5) {
                                    $additional5 = $additional5 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $combine = '<p>' . $additional1 . '</p>' . '<p>' . $additional2 . '</p>' . '<p>' . $additional3 . '</p>' . '<p>' . $additional4 . '</p>' . '<p>' . $additional5 . '</p>';

                            if ($attribute_picture != null or $attribute_picture == '') {
                                $path = '/img/no_picture/' . rand(1, 2) . '.jpg';

                                $scholarship = Scholarship::create([
                                    'name' => $title,
                                    'uri' => $attribute->url,
                                    // 'picture' => $path,
                                    'website_id' => $attribute->website_id,
                                    'attribute_id' => $attribute->id,
                                    'description' => $desc,
                                    'reg' => $reg,
                                    'additional' => $combine,
                                ]);

                            } else {

                                $scholarship = Scholarship::create([
                                    'name' => $title,
                                    'uri' => $attribute->url,
                                    'picture' => $picture,
                                    'website_id' => $attribute->website_id,
                                    'attribute_id' => $attribute->id,
                                    'description' => $desc,
                                    'reg' => $reg,
                                    'additional' => $combine,

                                ]);

                            }
                            $str = $scholarship->description . $scholarship->additional;
                            if (
                                (!empty($str) && preg_match_all('~\b\d{4}\b\+?~', $str, $matches))
                            ) {
                                $today = date("Y");
                                $today = substr($today, 0, 2);
                                $years = array();
                                foreach($matches[0] as $year){
                                    $test = substr($year, 0, 2);
                                    if($test == $today){
                                        $years[] = $year;
                                    }
                                }
                                if($years == null ){
                                    $period = date("Y");
                                }
                                else{
                                    $value = max($years);
                                    $period = $value;
                                }
                            } else {
                                $period = date("Y");
                            }

                            $period = Period::create([
                                'scholarship_id' => $scholarship->id,
                                'period' => $period,
                            ]);

                        }

                    }

                }

                // dd('sukses insert initial data');
            } else {
                $attributes = Scholarship_attribute::with('website')->get();
                foreach ($attributes as $attribute) {

                    if ($attribute->website->status == 2) {
                        $client = new Client();

                        $attribute_id = $attribute->id;
                        $website_id = $attribute->website_id;
                        $attribute_url = $attribute->url;
                        $attribute_title = $attribute->attribute_title;
                        $attribute_desc = $attribute->attribute_description;
                        $attribute_picture = $attribute->attribute_picture;
                        $attribute_registration = $attribute->attribute_registration;
                        $attribute_period = $attribute->attribute_period;
                        $attribute_additional1 = $attribute->attribute_additional_1;
                        $attribute_additional2 = $attribute->attribute_additional_2;
                        $attribute_additional3 = $attribute->attribute_additional_3;
                        $attribute_additional4 = $attribute->attribute_additional_4;
                        $attribute_additional5 = $attribute->attribute_additional_5;
                        $threshold = Configuration::find(2);
                        $threshold = $threshold->value;

                        $guzzleclient = new \GuzzleHttp\Client(array(
                            'timeout' => 90,
                            'verify' => false,
                        ));
                        $client->setClient($guzzleclient);
                        $crawler = $client->request('GET', $attribute->url);

                        $crawler->filter($attribute->attribute1)->each(function ($node) use (&$threshold, &$attribute_desc, &$attribute_title, &$attribute_picture, &$attribute_url, &$attribute_website, &$attribute_id, &$website_id, &$attribute_registration, &$attribute_additional1, &$attribute_additional2, $attribute_additional3, $attribute_additional4, $attribute_additional5) {
                            $dec_list = array();
                            unset($dec_list);
                            $dec_list = array();
                            $web_matchs = array();
                            unset($web_matchs);
                            $web_matchs = array();

                            $link = $node->attr('href');
                            $client = new Client();
                            $crawlerdtl = $client->request('GET', $link);
                            $title = '';
                            if ($attribute_title != null) {
                                $crawlerdtl->filter($attribute_title)->each(function ($node) use (&$title, &$attribute_title) {
                                    $title = $node->text();
                                });
                            }

                            $desc = '';
                            if ($attribute_desc != null) {
                                $crawlerdtl->filter($attribute_desc)->each(function ($node) use (&$desc, &$attribute_desc) {
                                    $desc = $desc . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $regs = '';
                            if ($attribute_registration != null) {
                                $crawlerdtl->filter("")->each(function ($node) use (&$regs, &$attribute_registration) {
                                    $regs = $node->attr('href');
                                });
                            }

                            $picture = '';
                            if ($attribute_picture != null) {
                                $crawlerdtl->filter($attribute_picture)->each(function ($node) use (&$picture, &$attribute_picture) {
                                    $picture = $node->attr('src');
                                });
                            }

                            $additional1 = '';
                            if ($attribute_additional1 != null) {
                                $crawlerdtl->filter($attribute_additional1)->each(function ($node) use (&$additional1, &$attribute_additional1) {
                                    $additional1 = $additional1 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional2 = '';
                            if ($attribute_additional2 != null) {
                                $crawlerdtl->filter($attribute_additional2)->each(function ($node) use (&$additional2, &$attribute_additional2) {
                                    $additional2 = $additional2 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional3 = '';
                            if ($attribute_additional3 != null) {
                                $crawlerdtl->filter($attribute_additional3)->each(function ($node) use (&$additional3, &$attribute_additional3) {
                                    $additional3 = $additional3 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional4 = '';
                            if ($attribute_additional4 != null) {
                                $crawlerdtl->filter($attribute_additional4)->each(function ($node) use (&$additional4, &$attribute_additional4) {
                                    $attribute_additional4 = $attribute_additional4 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional5 = '';
                            if ($attribute_additional5 != null) {
                                $crawlerdtl->filter($attribute_additional5)->each(function ($node) use (&$additional5, &$attribute_additional5) {
                                    $additional5 = $additional5 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $combine = '<p>' . $additional1 . '</p>' . '<p>' . $additional2 . '</p>' . '<p>' . $additional3 . '</p>' . '<p>' . $additional4 . '</p>' . '<p>' . $additional5 . '</p>';

                            $Scholarship = Scholarship::All();
                            foreach ($Scholarship as $Scholarship) {
                                $comparison = new \Atomescrochus\StringSimilarities\Compare();
                                $jaroWinkler = $comparison->jaroWinkler($Scholarship->description, $desc);

                                $dec_list[] = $Scholarship->description;
                                if ($jaroWinkler >= $threshold) {
                                    $scholarship_ID = $Scholarship->id;
                                    $web_matchs[] = $desc;
                                }
                            }
                            $MatchArray = array_unique($web_matchs);

                            if ($MatchArray == null) {
                                if ($attribute_picture == null or $attribute_picture == '') {
                                    $path = '/img/no_picture/' . rand(1, 2) . '.jpg';

                                    $Scholarship = Scholarship::create([
                                        'name' => $title,
                                        'uri' => $link,
                                        // 'picture' => $path,
                                        'website_id' => $website_id,
                                        'attribute_id' => $attribute_id,
                                        'reg' => $regs,
                                        'description' => $desc,
                                        'additional' => $combine,

                                    ]);
                                } else {
                                    $scholarship = Scholarship::create([
                                        'name' => $title,
                                        'uri' => $link,
                                        'picture' => $picture,
                                        'website_id' => $website_id,
                                        'attribute_id' => $attribute_id,
                                        'reg' => $regs,
                                        'description' => $desc,
                                        'additional' => $combine,

                                    ]);
                                }

                                $str = $Scholarship->description . $Scholarship->additional;
                                if (
                                    (!empty($str) && preg_match_all('~\b\d{4}\b\+?~', $str, $matches))
                                ) {
                                    $today = date("Y");
                                    $today = substr($today, 0, 2);
                                    $years = array();
                                    foreach($matches[0] as $year){
                                        $test = substr($year, 0, 2);
                                        if($test == $today){
                                            $years[] = $year;
                                        }
                                    }
                                    if($years == null ){
                                        $period = date("Y");
                                    }
                                    else{
                                        $value = max($years);
                                        $period = $value;
                                    }
                                } else {
                                    $period = date("Y");
                                }

                                $period = Period::create([
                                    'scholarship_id' => $Scholarship->id,
                                    'period' => $period,
                                ]);
                            } else {

                                foreach ($MatchArray as $Match) {
                                    // $scholarship = Scholarship::where('description', $Match)->first();
                                    // $scholarship_period = Period::where('scholarship_id', $scholarship->id)->first();
                                    $scholarship_period = Period::where('scholarship_id', $scholarship_ID)->first();

                                    $str = $Match . $combine;
                                    if (
                                        (!empty($str) && preg_match_all('~\b\d{4}\b\+?~', $str, $matches))
                                    ) {
                                        $today = date("Y");
                                        $today = substr($today, 0, 2);
                                        $years = array();
                                        foreach($matches[0] as $year){
                                            $test = substr($year, 0, 2);
                                            if($test == $today){
                                                $years[] = $year;
                                            }
                                        }
                                        if($years == null ){
                                            $period = date("Y");
                                        }
                                        else{
                                            $value = max($years);
                                            $period = $value;
                                        }
                                    } else {
                                        $period = date("Y");
                                    }

                                    if ($scholarship_period->period < $period) {
                                        $scholarship_period->period = $period;
                                        $scholarship_period->save();
                                    }
                                }

                            }

                        });

                    } elseif ($attribute->website->status == 3) {
                        $threshold = Configuration::find(3);
                        $threshold = $threshold->value;

                        if ($attribute->attribute_id != null) {
                            $client = new Client();
                            $attribute_id = $attribute->id;
                            $attribute_title = $attribute->attribute_title;
                            $attribute_desc = $attribute->attribute_description;
                            $attribute_picture = $attribute->attribute_picture;
                            $attribute_registration = $attribute->attribute_registration;
                            $attribute_period = $attribute->attribute_period;
                            $attribute_additional1 = $attribute->attribute_additional_1;
                            $attribute_additional2 = $attribute->attribute_additional_2;
                            $attribute_additional3 = $attribute->attribute_additional_3;
                            $attribute_additional4 = $attribute->attribute_additional_4;
                            $attribute_additional5 = $attribute->attribute_additional_5;

                            $guzzleclient = new \GuzzleHttp\Client(array(
                                'timeout' => 90,
                                'verify' => false,
                            ));
                            $client->setClient($guzzleclient);
                            $crawler = $client->request('GET', $attribute->url);

                            if ($attribute_title != null or $attribute_title != '') {
                                $crawler->filter($attribute_title)->each(function ($node) use (&$title, &$attribute_title) {
                                    $title = $node->text();
                                });
                                $Scholarship = Scholarship::where('attribute_id', $attribute->attribute_id)->first();
                                $Scholarship->name = $title;
                                $Scholarship->save();
                            }

                            if ($attribute_desc != null) {
                                $crawler->filter($attribute_desc)->each(function ($node) use (&$desc, &$attribute_desc) {
                                    $desc = $desc . '<p>' . $node->text() . '</p>';
                                });
                                $Scholarship = Scholarship::where('attribute_id', $attribute->attribute_id)->first();
                                $Scholarship->name = $desc;
                                $Scholarship->save();
                            }

                            if ($attribute_picture != null) {
                                $crawler->filter($attribute_picture)->each(function ($node) use (&$picture, &$attribute_picture) {
                                    $picture = $node->attr('src');
                                });
                                $Scholarship = Scholarship::where('attribute_id', $attribute->attribute_id)->first();
                                $Scholarship->name = $picture;
                                $Scholarship->save();
                            }

                            if ($attribute_registration != null) {
                                $crawler->filter($attribute_registration)->each(function ($node) use (&$reg, &$attribute_registration) {
                                    $reg = $node->attr('href');
                                });
                                $Scholarship = Scholarship::where('attribute_id', $attribute->attribute_id)->first();
                                $Scholarship->name = $reg;
                                $Scholarship->save();
                            }

                            $additional1 = '';
                            if ($attribute_additional1 != null) {
                                $crawler->filter($attribute_additional1)->each(function ($node) use (&$additional1, &$attribute_additional1) {
                                    $additional1 = $additional1 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional2 = '';
                            if ($attribute_additional2 != null) {
                                $crawler->filter($attribute_additional2)->each(function ($node) use (&$additional2, &$attribute_additional2) {
                                    $additional2 = $additional2 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional3 = '';
                            if ($attribute_additional3 != null) {
                                $crawler->filter($attribute_additional3)->each(function ($node) use (&$additional3, &$attribute_additional3) {
                                    $additional3 = $additional3 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional4 = '';
                            if ($attribute_additional4 != null) {
                                $crawler->filter($attribute_additional4)->each(function ($node) use (&$additional4, &$attribute_additional4) {
                                    $attribute_additional4 = $attribute_additional4 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional5 = '';
                            if ($attribute_additional5 != null) {
                                $crawler->filter($attribute_additional5)->each(function ($node) use (&$additional5, &$attribute_additional5) {
                                    $additional5 = $additional5 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $Scholarship = Scholarship::where('attribute_id', $attribute->attribute_id)->first();
                            $combine = $Scholarship->additional . '<p>' . $additional1 . '</p>' . '<p>' . $additional2 . '</p>' . '<p>' . $additional3 . '</p>' . '<p>' . $additional4 . '</p>' . '<p>' . $additional5 . '</p>';
                            $Scholarship->additional = $combine;
                            $Scholarship->save();

                            $str = $Scholarship->description . $Scholarship->additional;
                            if (
                                (!empty($str) && preg_match_all('~\b\d{4}\b\+?~', $str, $matches))
                            ) {
                                $today = date("Y");
                                $today = substr($today, 0, 2);
                                $years = array();
                                foreach($matches[0] as $year){
                                    $test = substr($year, 0, 2);
                                    if($test == $today){
                                        $years[] = $year;
                                    }
                                }
                                if($years == null ){
                                    $period = date("Y");
                                }
                                else{
                                    $value = max($years);
                                    $period = $value;
                                }
                            } else {
                                $period = date("Y");
                            }
                            $periods = Period::where('scholarship_id', $Scholarship->id)->first();
                            if ($period > $periods->period) {
                                $periods->period = $period;
                                $periods->save();

                            }
                        } else {
                            $dec_list = array();
                            $web_matchs = array();

                            $client = new Client();
                            $attribute_id = $attribute->id;
                            $attribute_title = $attribute->attribute_title;
                            $attribute_desc = $attribute->attribute_description;
                            $attribute_picture = $attribute->attribute_picture;
                            $attribute_registration = $attribute->attribute_registration;
                            $attribute_period = $attribute->attribute_period;
                            $attribute_additional1 = $attribute->attribute_additional_1;
                            $attribute_additional2 = $attribute->attribute_additional_2;
                            $attribute_additional3 = $attribute->attribute_additional_3;
                            $attribute_additional4 = $attribute->attribute_additional_4;
                            $attribute_additional5 = $attribute->attribute_additional_5;

                            $guzzleclient = new \GuzzleHttp\Client(array(
                                'timeout' => 90,
                                'verify' => false,
                            ));
                            $client->setClient($guzzleclient);
                            $crawler = $client->request('GET', $attribute->url);

                            $title = '';
                            if ($attribute_title != null or $attribute_title != '') {
                                $crawler->filter($attribute_title)->each(function ($node) use (&$title, &$attribute_title) {
                                    $title = $node->text();
                                });
                            }

                            $desc = '';
                            if ($attribute_desc != null) {
                                $crawler->filter($attribute_desc)->each(function ($node) use (&$desc, &$attribute_desc) {
                                    $desc = $desc . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $picture = '';
                            if ($attribute_picture != null) {
                                $crawler->filter($attribute_picture)->each(function ($node) use (&$picture, &$attribute_picture) {
                                    $picture = $node->attr('src');
                                });
                            }

                            $reg = '';
                            if ($attribute_registration != null) {
                                $crawler->filter($attribute_registration)->each(function ($node) use (&$reg, &$attribute_registration) {
                                    $text = $node->text();
                                    if ($text == "Daftar Online") {
                                        $reg = $node->attr('href');
                                    }
                                });
                            }

                            $additional1 = '';
                            if ($attribute_additional1 != null) {
                                $crawler->filter($attribute_additional1)->each(function ($node) use (&$additional1, &$attribute_additional1) {
                                    $additional1 = $additional1 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional2 = '';
                            if ($attribute_additional2 != null) {
                                $crawler->filter($attribute_additional2)->each(function ($node) use (&$additional2, &$attribute_additional2) {
                                    $additional2 = $additional2 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional3 = '';
                            if ($attribute_additional3 != null) {
                                $crawler->filter($attribute_additional3)->each(function ($node) use (&$additional3, &$attribute_additional3) {
                                    $additional3 = $additional3 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional4 = '';
                            if ($attribute_additional4 != null) {
                                $crawler->filter($attribute_additional4)->each(function ($node) use (&$additional4, &$attribute_additional4) {
                                    $attribute_additional4 = $attribute_additional4 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $additional5 = '';
                            if ($attribute_additional5 != null) {
                                $crawler->filter($attribute_additional5)->each(function ($node) use (&$additional5, &$attribute_additional5) {
                                    $additional5 = $additional5 . '<p>' . $node->text() . '</p>';
                                });
                            }

                            $combine = '<p>' . $additional1 . '</p>' . '<p>' . $additional2 . '</p>' . '<p>' . $additional3 . '</p>' . '<p>' . $additional4 . '</p>' . '<p>' . $additional5 . '</p>';

                            $Scholarships = Scholarship::All();
                            foreach ($Scholarships as $Scholarship) {
                                $comparison = new \Atomescrochus\StringSimilarities\Compare();
                                $jaroWinkler = $comparison->jaroWinkler($Scholarship->description, $desc);

                                $dec_list[] = $Scholarship->description;
                                if ($jaroWinkler >= $threshold) {
                                    $scholarship_ID = $Scholarship->id;
                                    $web_matchs[] = $desc;
                                }
                            }

                            $MatchArray = array_unique($web_matchs);

                            if ($MatchArray == null) {
                                if ($attribute_picture == null or $attribute_picture == '') {
                                    $path = '/img/no_picture/' . rand(1, 2) . '.jpg';
                                    $Scholarship = Scholarship::create([
                                        'name' => $title,
                                        'uri' => $attribute->url,
                                        // 'picture' => $path,
                                        'website_id' => $attribute->website_id,
                                        'attribute_id' => $attribute->id,
                                        'description' => $desc,
                                        'reg' => $reg,
                                        'additional' => $combine,

                                    ]);
                                } else {
                                    $scholarship = Scholarship::create([
                                        'name' => $title,
                                        'uri' => $attribute->url,
                                        'picture' => $picture,
                                        'website_id' => $attribute->website_id,
                                        'attribute_id' => $attribute->id,
                                        'description' => $desc,
                                        'reg' => $reg,
                                        'additional' => $combine,

                                    ]);
                                }

                                $str = $Scholarship->description . $Scholarship->additional;
                                if (
                                    (!empty($str) && preg_match_all('~\b\d{4}\b\+?~', $str, $matches))
                                ) {
                                    $today = date("Y");
                                    $today = substr($today, 0, 2);
                                    $years = array();
                                    foreach($matches[0] as $year){
                                        $test = substr($year, 0, 2);
                                        if($test == $today){
                                            $years[] = $year;
                                        }
                                    }
                                    if($years == null ){
                                        $period = date("Y");
                                    }
                                    else{
                                        $value = max($years);
                                        $period = $value;
                                    }
                                } else {
                                    $period = date("Y");
                                }

                                $period = Period::create([
                                    'scholarship_id' => $Scholarship->id,
                                    'period' => $period,
                                ]);
                            } else {
                                foreach ($MatchArray as $Match) {
                                    $scholarship_period = Period::where('scholarship_id', $scholarship_ID)->first();
                                    $str = $Match . $combine;
                                    if (
                                        (!empty($str) && preg_match_all('~\b\d{4}\b\+?~', $str, $matches))
                                    ) {
                                        $today = date("Y");
                                        $today = substr($today, 0, 2);
                                        $years = array();
                                        foreach($matches[0] as $year){
                                            $test = substr($year, 0, 2);
                                            if($test == $today){
                                                $years[] = $year;
                                            }
                                        }
                                        if($years == null ){
                                            $period = date("Y");
                                        }
                                        else{
                                            $value = max($years);
                                            $period = $value;
                                        }
                                    } else {
                                        $period = date("Y");
                                    }

                                    if ($scholarship_period->period < $period) {
                                        $scholarship_period->period = $period;
                                        $scholarship_period->save();
                                    }
                                }

                            }

                        }

                    }

                }

            }

            Log::create([
                'type' => 2,
                'status' => 1,
            ]);

            echo 'Done';

        } catch (\Exception $e) {
            if (strpos($e, 'timed out') !== false) {
                Log::create([
                    'type' => 2,
                    'status' => 0,
                    'message' => 'Time Out, Maybe Website Down or Your connection get trouble',
                ]);
                echo 'Timeout '.'<br>'.$e;

            }
            else{
                Log::create([
                    'type' => 2,
                    'status' => 0,
                    'message' => 'Unknow Error please Check Scrap Website Manually',
                ]);

                echo 'Error - '.$e;

            }


        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $comparison = new \Atomescrochus\StringSimilarities\Compare();

        // the functions returns similarity percentage between strings
        // $jaroWinkler = $comparison->jaroWinkler('audhy', 'audi'); // JaroWinkler comparison
        // dd($jaroWinkler);

        $all = $comparison->all('audhy', 'audi');
        dd($all);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Website  $website
     * @return \Illuminate\Http\Response
     */
    public function show(Website $website)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Website  $website
     * @return \Illuminate\Http\Response
     */
    public function edit(Website $website)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Website  $website
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Website $website)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Website  $website
     * @return \Illuminate\Http\Response
     */
    public function destroy(Website $website)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Goutte;
use Goutte\Client;
use App\Website;
use App\Log;
use Guzzle\Http\Exception\ClientErrorResponseException;

class GraptController extends Controller
{
    public function tes(Request $request)
    {
        if($request->attr == null or $request->attr == ''){
            try {
        
                $client = new Client();
                $guzzleclient = new \GuzzleHttp\Client(array(
                    'timeout' => 90,
                    'verify' => false,
                ));
                $client->setClient($guzzleclient);
            
                $crawler = $client->request('GET', $request->url);
                $respones = array();

                $crawler->filter($request->attribute)->each(function ($node) use (&$respones){
                    $respones[] =$node->text();
                });
                return response()->json($respones);
    
            } catch (\Exception $e) {
            $respones = array();
            $respones[] = "failed";
            return response($respones);
           
            }
        }
        else{
            if($request->attribute == null  or $request->attribute == ''){
                // note https://indbeasiswa.com/beasiswa-s1
                // note h2 a
                // note .post-content p a
                    try {
        
                        $client = new Client();
                        $guzzleclient = new \GuzzleHttp\Client(array(
                            'timeout' => 90,
                            'verify' => false,
                        ));
                        $client->setClient($guzzleclient);                    
                        $crawler = $client->request('GET', $request->url);
                        $respones = array();
        
                        $crawler->filter($request->attr)->each(function ($node) use (&$respones){
                            $respones[] =$node->text();
                        });
                        return response()->json($respones);
            
                    } catch (\Exception $e) {
                    $respones = array();
                    $respones[] = "failed";
                    return response($respones);
                   
                    }
                }
                else{
                    try {
                        $client = new Client();
                        $guzzleclient = new \GuzzleHttp\Client(array(
                            'timeout' => 90,
                            'verify' => false,
                        ));
                        $client->setClient($guzzleclient);                    
                        $crawler = $client->request('GET', $request->url);
                        $respones = array();
                        $attribute = $request->attribute;
        
                        $crawler->filter($request->attr)->each(function ($node) use (&$respones,&$attribute){
                            $link = $node->attr('href');
                            $client = new Client();
                            $guzzleclient = new \GuzzleHttp\Client(array(
                                'timeout' => 90,
                                'verify' => false,
                            ));
                            $client->setClient($guzzleclient);                        
                            $crawlerdtl = $client->request('GET', $link);
                            $crawlerdtl->filter($attribute)->each(function ($node) use (&$respones) {
                                $respones[] =$node->text();      
                            });
                    
                        });
                        return response()->json($respones);
        
                    } catch (\Exception $e) {
                    $respones = array();
                    $respones[] = "failed";
                    return response($respones);
        
                    
                }
            }        
        }
    }

    public function ind()
    {

    $client = new Client();
    $crawler = $client->request('GET', 'https://indbeasiswa.com/beasiswa-s1');
    // echo'berhasil';
    // $crawler = Goutte::request('GET', 'https://indbeasiswa.com/beasiswa-s1');
    //grapt gambar daftar beasiswa
    // $crawler->filter('.feat-thumbnail .post-thumb a img')->each(function ($node) {
    //     echo $node->attr('src').'<br>';
    // });
    $crawler->filter('h2 a')->each(function ($node) {
        // echo($node->text());
        // echo $node->attr('href').'<br>';
        $link = $node->attr('href');
        //detail
        $client = new Client();
        $crawlerdtl = $client->request('GET', $link);
        //judul
        // $crawlerdtl->filter('.post-top h1')->each(function ($node) {
        //     echo($node->text()).'<br>';
        //
        // });
        //paragraf
        $crawlerdtl->filter('.post-content p a')->each(function ($node) {
            echo($node->text()).'<br>';

        });

    });
    }

    public function info()
    {

    $crawler = Goutte::request('GET', 'http://www.info-beasiswa.id/search/label/DIPLOMA%2FS1');
    $crawler->filter('h3 a')->each(function ($node) {
        $link = $node->attr('href');
        $crawlerdtl = Goutte::request('GET', $link);
        $crawlerdtl->filter('h3 a')->each(function ($node) {
            echo($node->text()).'<br>';
        });
    });
    }
    public function ayo()
    {
    $crawler = Goutte::request('GET', 'https://ayokuliah.id/artikel/informasi-beasiswa/');
    $crawler->filter('h4 a')->each(function ($node) {
        $link = $node->attr('href');
        $crawlerdtl = Goutte::request('GET', $link);
        $crawlerdtl->filter('.article-detail-title h1')->each(function ($node) {
            echo($node->text()).'<br>';
        });
    });
    }

    public function testing(Request $request)
    {
        $Websites = Website::pluck('url');
        if ($Websites->isEmpty()){
            $client = new Client();
            $crawler = $client->request('GET', 'https://google.com/search?q=beasiswa');
            $crawler->filter('cite')->each(function ($node) {
                $web = str_replace(array('https://','www.'), '',$node->text());
                $comparison = new \Atomescrochus\StringSimilarities\Compare();     
                $Website = Website::create([
                    'url' =>$web,
                    'status' => '1',
                ]);
            });                
        }
        else{
            $client = new Client();
            $crawler = $client->request('GET', 'https://google.com/search?q=beasiswa');
            $urls = array();
            $matchs = array();
            $crawler->filter('cite')->each(function ($node) use (&$urls,&$matchs) {
                $Websites = Website::pluck('url');
                foreach($Websites as $Website){
                    // $web = str_replace(array('https://'), '',$Website);
                    $comparison = new \Atomescrochus\StringSimilarities\Compare();     
                    $web = str_replace(array('https://','www.'), '',$node->text());
                    $jaroWinkler = $comparison->jaroWinkler($Website, $web);
                    // dump($Website." + ".$web." = ".$jaroWinkler);    

                    $urls[] = $node->text();
                    if($jaroWinkler >= 0.8 ){
                        $matchs[] = $node->text();
                        // dump($result);
                    }
                }
            });
            
            $MatchArray = array_unique($matchs);
            // dump($MatchArray);
            $UrlArray = array_unique($urls);
            // dump($UrlArray);
            // $matchs2 = [];
            $diffs=array_diff($UrlArray,$MatchArray);
            // dump($diffs);

            // $urls2 = array();
            // $matchs2 = array();

            // foreach($diffs as $diff){

            //     $Websites = Website::where('status',0)->pluck('url');
            //     foreach($Websites as $Website){
            //         $comparison = new \Atomescrochus\StringSimilarities\Compare();     
            //         $jaroWinkler2 = $comparison->jaroWinkler($diff,$Website);
            //         $urls2[] = $diff;

            //         if($jaroWinkler2 >= 0.9 ){
            //             $matchs2[] = $diff;
            //         }

            //     }
            // }
            // // dump($urls2);

            // $MatchArray2 = array_unique($matchs2);
            // // dump($matchs2);

            // $UrlArray2 = array_unique($urls2);
            // // dump($UrlArray2);

            // $diffs2=array_diff($UrlArray,$MatchArray);
            // // dump($diffs2);

            // $Websites = Website::where('status',0)->pluck('url');
            // foreach($diffs as $diff){
            //     $web = str_replace(array('https://','www.'), '',$diff);
            //     $Website = Website::create([
            //         'url' =>$web,
            //         'status' => '1',
            //     ]);
            // }
        }
    // return response()->json($message);

    // dump('sukses');

    return response()->json($diffs);
        // dd('testing');
        // $type = $request->type;
        // $uri = $request->url;
        // $attribute1 = $request->attribute1;
        // $attribute2 = $request->attribute2;

        // // h2 a
        // // .content-joblist h1
        // if($type == 2){
        //     $client = new Client();
        //     $crawler = $client->request('GET',$uri);
        //     $crawler->filter($attribute1)->each(function ($node) use (&$attribute2) {
        //         $link = $node->attr('href');
        //         $client = new Client();
        //         $crawlerdtl = $client->request('GET', $link);
        //         $crawlerdtl->filter($attribute2)->each(function ($node) {
        //             echo($node->text()).'<br>';
        //         });
        //     });        
        // }
        // elseif($type == 3){
        //     $client = new Client();
        //     $crawler = $client->request('GET',$uri);
        //     $crawler->filter($attribute1)->each(function ($node) {
        //         // $link = $node->attr('href');
        //         // $client = new Client();
        //         // $crawlerdtl = $client->request('GET', $link);
        //         // $crawlerdtl->filter('.content-joblist h1')->each(function ($node) {
        //             echo($node->text()).'<br>';
        //         // });
        //     });        

        // }
    //     $client = new Client(['verify' => '/etc/php/7.2/cli/cacert.pem']);
    // // $client = new Client();
    // $crawler = $client->request('GET', 'https://www.lpdp.kemenkeu.go.id/beasiswa-reguler/');
    // $crawler->filter('div .wpb_wrapper')->each(function ($node) {
    //     echo($node->text()).'<br>';
    //     // $link = $node->attr('href');
    //     // $crawlerdtl = Goutte::request('GET', $link);
    //     // $crawlerdtl->filter('.content-joblist h1')->each(function ($node) {
    //     //     echo($node->text()).'<br>';
    //     // });
    // });
    }

    public function lpdp1()
    {
        $client = new Client(['verify' => '/etc/php/7.2/cli/cacert.pem']);
    // $client = new Client();
    $crawler = $client->request('GET', 'https://www.lpdp.kemenkeu.go.id/beasiswa-reguler/');
    $crawler->filter('div .wpb_wrapper')->each(function ($node) {
        echo($node->text()).'<br>';
        // $link = $node->attr('href');
        // $crawlerdtl = Goutte::request('GET', $link);
        // $crawlerdtl->filter('.content-joblist h1')->each(function ($node) {
        //     echo($node->text()).'<br>';
        // });
    });
    }

    public function top()
    {
    $crawler = Goutte::request('GET', 'https://www.topkarir.com/beasiswa');
    $crawler->filter('h2 a')->each(function ($node) {
        $link = $node->attr('href');
        $crawlerdtl = Goutte::request('GET', $link);
        $crawlerdtl->filter('.content-joblist h1')->each(function ($node) {
            echo($node->text()).'<br>';
        });
    });
    }
    public function id()
    {
    $crawler = Goutte::request('GET', 'https://beasiswa-id.net/');
    $crawler->filter('h2 a')->each(function ($node) {
        $link = $node->attr('href');
        $crawlerdtl = Goutte::request('GET', $link);
        $crawlerdtl->filter('.single_post h1')->each(function ($node) {
            echo($node->text()).'<br>';
        });
    });
    }


    public function plus()
    {
    $crawler = Goutte::request('GET', 'https://djarumbeasiswaplus.org/tentang_kami/tentang-djarum-beasiswa-plus');
    $crawler->filter('.tab-content p')->each(function ($node) {
        echo($node->text()).'<br>';
    });
    }
    public function lpdp()
    {
    $crawler = Goutte::request('GET', 'https://www.lpdp.kemenkeu.go.id/');
    $crawler->filter('.wpb_wrapper p')->each(function ($node) {
            echo($node->text()).'<br>';
    });
    }
    public function unggulan()
    {
        $client = new Client();
        // $client->setHttpClient($guzzleClient);
      $crawler = $client->request('GET', 'https://beasiswaunggulan.kemdikbud.go.id/beasiswa/beasiswa-masyarakat-berprestasi');
      $crawler->filter('.main-content h2')->each(function ($node) {
      //       echo($node->text()).'<br>';
          // $link = $node->attr('href');
          // $crawlerdtl = Goutte::request('GET', $link);
          // $crawlerdtl->filter('h2')->each(function ($node) {
          //     echo($node->text()).'<br>';
          // });
      });
    }
    public function bukalapak()
    {
    $crawler = Goutte::request('GET', 'https://scholarship.bukalapak.com/');
    $crawler->filter('.u-pad--v-2 p')->each(function ($node) {
        echo($node->text()).'<br>';
    });
    }
    public function sampoerna()
    {
    $crawler = Goutte::request('GET', 'https://beasiswa-id.net/');
    $crawler->filter('h2 a')->each(function ($node) {
        $link = $node->attr('href');
        $crawlerdtl = Goutte::request('GET', $link);
        $crawlerdtl->filter('.single_post h1')->each(function ($node) {
            echo($node->text()).'<br>';
        });            // return $node->filter('h2 a');

    });
    }
}

<?php

namespace App\Http\Controllers;

use DB;
use View;
use App\Log;
use App\Message;
use App\Website;
use App\Mail\SendMailable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function mail()
    {
    $name = 'Audhy';
    Mail::to('audhy.vk01@gmail.com')->send(new SendMailable($name));

    return 'Email was sent';
    }

    public function log()
    {
        $logs = Log::orderBy('created_at', 'desc')->get();
        return $logs;
    }
    public function suggestion()
    {
        $suggestions = DB::table('websites')->where('status','1')->get();
        return $suggestions;
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suggestions = $this->suggestion();
        $messages = Message::All();
        $logs = $this->log();
        $total_website = DB::table('websites')->where('status','<>','1')->count();
        $total_scholarship = DB::table('scholarships')->count();
        $total_subscriber = DB::table('subscribers')->count();
        $total_message = DB::table('messages')->where('reply',null)->count();
        return view('home',compact('total_website','total_scholarship','total_subscriber','messages','suggestions','logs','total_message'));
    }

    /**
     * Show the suggestiion website on dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function suggestions()
    {
        $suggestions = DB::table('websites')->where('status','1')->get();
        return View::make('dashboard.suggestion',compact('suggestions'))
        ->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Website  $website
     * @return \Illuminate\Http\Response
     */
    public function ignore($website)
    {
        $website=Website::find($website);
        $website->status = 0;
        $website->save();

        $website->delete();

        return redirect()->route('home')->with('pesan_sukses','Ignore berhasil !');
    }

    /**
     * Show the grapt log all website on dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function graptLogs()
    {
        return View::make('dashboard.grapt-log')
        ->render();
    }
}

<?php

namespace App\Http\Controllers;

use App\Period;
use App\Scholarship;
use App\ListUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\StoreScholarship;
use App\Http\Requests\UpdateScholarship;


class ScholarshipController extends Controller
{
    /**
     * Show data from model for DataTable.
     *
     * @return \Illuminate\Http\Response
     */
    public function getScholarships()
    {
        $scholarships = Scholarship::All();

        $data = $alldata = json_decode($scholarships);

        $datatable = array_merge(['pagination' => [], 'sort' => [], 'query' => []], $_REQUEST);

        $filter = isset($datatable['query']['generalSearch']) && is_string($datatable['query']['generalSearch'])
                    ? $datatable['query']['generalSearch'] : '';

        if (! empty($filter)) {
            $data = array_filter($data, function ($a) use ($filter) {
                return (boolean)preg_grep("/$filter/i", (array)$a);
            });

            unset($datatable['query']['generalSearch']);
        }

        $query = isset($datatable['query']) && is_array($datatable['query']) ? $datatable['query'] : null;

        if (is_array($query)) {
            $query = array_filter($query);

            foreach ($query as $key => $val) {
                $data = $this->list_filter($data, [$key => $val]);
            }
        }

        $sort  = ! empty($datatable['sort']['sort']) ? $datatable['sort']['sort'] : 'asc';
        $field = ! empty($datatable['sort']['field']) ? $datatable['sort']['field'] : 'RecordID';

        $meta    = [];
        $page    = ! empty($datatable['pagination']['page']) ? (int)$datatable['pagination']['page'] : 1;
        $perpage = ! empty($datatable['pagination']['perpage']) ? (int)$datatable['pagination']['perpage'] : -1;

        $pages = 1;
        $total = count($data);

        usort($data, function ($a, $b) use ($sort, $field) {
            if (! isset($a->$field) || ! isset($b->$field)) {
                return false;
            }

            if ($sort === 'asc') {
                return $a->$field > $b->$field ? true : false;
            }

            return $a->$field < $b->$field ? true : false;
        });

        if ($perpage > 0) {
            $pages  = ceil($total / $perpage);
            $page   = max($page, 1);
            $page   = min($page, $pages);
            $offset = ($page - 1) * $perpage;

            if ($offset < 0) {
                $offset = 0;
            }

            $data = array_slice($data, $offset, $perpage, true);
        }

        $meta = [
            'page'    => $page,
            'pages'   => $pages,
            'perpage' => $perpage,
            'total'   => $total,
        ];

        if (isset($datatable['requestIds']) && filter_var($datatable['requestIds'], FILTER_VALIDATE_BOOLEAN)) {
            $meta['rowIds'] = array_map(function ($row) {
                return $row->RecordID;
            }, $alldata);
        }

        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $result = [
            'meta' => $meta + [
                    'sort'  => $sort,
                    'field' => $field,
                ],
            'data' => $data,
        ];

        echo json_encode($result, JSON_PRETTY_PRINT);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('scholarship.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreScholarship $request)
    {
        if( Input::file('fileInput') == null){
            // $path = '/img/no_picture/'.rand(1, 2).'.jpg';


            $scholarship = Scholarship::create([
                'name' => $request->input('name'),
                'uri' => $request->input('uri'),
                // 'picture' => $path,
                'website_id' => $request->input('website_id'),
                'description' => $request->input('description'),
            ]);
            // $periods = Period::where('scholarship_id', $scholarship->id)->first();
            // $periods->period = $request->period;
            // $periods->save();
            $periods = Period::create([
                // ('scholarship_id', $scholarship->id)->first();
                'scholarship_id' => $scholarship->id,
                'period' => $request->period,
            ]);



            return response()->json($scholarship);

        }else{

        // $s->scholarships()->attach(['1'=> ['url' => 'url', 'attribute1' => '1', 'attribute2'=>'2','attribute_title'=>'title','attribute_picture'=>'picture','attribute_description'=>'description','attribute_registration'=>'registration','attribute_period'=>'period']]);
        $file = Input::file('fileInput');
        $ext = $file->getClientOriginalExtension();
        $length = 10;
        $randomString = substr(str_shuffle(md5(time())),0,$length);
        $fileName = $randomString.".$ext";
        $destinationPath = "uploads/".date('Y').'/'.date('m').'/';
        $moved_file = $file->move($destinationPath, $fileName);
        $path = $destinationPath."/".$fileName;

        $scholarship = Scholarship::create([
            'name' => $request->input('name'),
            'uri' => $request->input('uri'),
            'picture' => $path,
            'website_id' => $request->input('website_id'),
            'description' => $request->input('description'),
        ]);

        $periods = Period::create([
            // ('scholarship_id', $scholarship->id)->first();
            'scholarship_id' => $scholarship->id,
            'period' => $request->period,
        ]);


        return response()->json($scholarship);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Scholarship  $scholarship
     * @return \Illuminate\Http\Response
     */
    public function show(Scholarship $scholarship)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Scholarship  $scholarship
     * @return \Illuminate\Http\Response
     */
    public function edit(Scholarship $scholarship)
    {
        return response()->json($scholarship);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Scholarship  $scholarship
     * @return \Illuminate\Http\Response
     */
    public function periodEdit($scholarship)
    {
        $period = Period::where('scholarship_id',$scholarship)->first();
        return response()->json($period);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Scholarship  $scholarship
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateScholarship $request, Scholarship $scholarship)
    {
        // dd($request->all());
        if( Input::file('fileInput') == null){

            $scholarship->name = $request->name;
            $scholarship->uri = $request->uri;
            $scholarship->description = $request->description;
            $scholarship->website_id = $request->website_id;
            $scholarship->picture = '$path';

            $scholarship->save();

            $periods = Period::where('scholarship_id', $scholarship->id)->first();
            $periods->period = $request->period;
            $periods->save();

            return response()->json($scholarship);

        }else{
            $file = Input::file('fileInput');
            $ext = $file->getClientOriginalExtension();
            $length = 10;
            $randomString = substr(str_shuffle(md5(time())),0,$length);
            $fileName = $randomString.".$ext";
            $destinationPath = "uploads/".date('Y').'/'.date('m').'/';
            $moved_file = $file->move($destinationPath, $fileName);
            $path = $destinationPath."/".$fileName;

            $scholarship->name = $request->name;
            $scholarship->uri = $request->uri;
            $scholarship->description = $request->description;
            $scholarship->picture = '$path';
            $scholarship->website_id = $request->website_id;
            $scholarship->save();

            $periods = Period::where('scholarship_id', $scholarship->id)->first();
            $periods->period = $request->period;
            $periods->save();

            return response()->json($scholarship);

        }
    }
    public function updateData(UpdateScholarship $request, Scholarship $scholarship)
    {
        // dd($request->all());
        if( Input::file('fileInput') == null){

            $scholarship->name = $request->name;
            $scholarship->uri = $request->uri;
            $scholarship->description = $request->description;
            $scholarship->website_id = $request->website_id;

            $scholarship->save();

            $periods = Period::where('scholarship_id', $scholarship->id)->first();
            $periods->period = $request->period;
            $periods->save();

            return response()->json($scholarship);

        }else{
            $file = Input::file('fileInput');
            $ext = $file->getClientOriginalExtension();
            $length = 10;
            $randomString = substr(str_shuffle(md5(time())),0,$length);
            $fileName = $randomString.".$ext";
            $destinationPath = "uploads/".date('Y').'/'.date('m').'/';
            $moved_file = $file->move($destinationPath, $fileName);
            $path = $destinationPath."/".$fileName;

            $scholarship->name = $request->name;
            $scholarship->uri = $request->uri;
            $scholarship->description = $request->description;
            $scholarship->picture = $path;
            $scholarship->website_id = $request->website_id;
            $scholarship->save();

            $periods = Period::where('scholarship_id', $scholarship->id)->first();
            $periods->period = $request->period;
            $periods->save();

            return response()->json($scholarship);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Scholarship  $scholarship
     * @return \Illuminate\Http\Response
     */
    public function destroy(Scholarship $scholarship)
    {
        $scholarship->delete();

        return response()->json($scholarship);
    }

    /**
     * Show data from model with flter on datatable.
     *
     * @param $list, $args, $operator
     * @return \Illuminate\Http\Response
     */
    public function list_filter($list, $args = array(), $operator = 'AND')
    {
        if (! is_array($list)) {
            return array();
        }

        $util = new ListUtil($list);

        return $util->filter($args, $operator);
    }
}

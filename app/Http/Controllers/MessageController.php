<?php

namespace App\Http\Controllers;

use Auth;
use View;
use App\Message;
use App\Mail\ReplyContact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\StoreMessage;
use App\Http\Requests\UpdateMessage;


class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcome.contact');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = Message::create([
            'email' => $request->email,
            'message' => $request->message,
        ]);

        return redirect()->route('message.index')->with('pesan_sukses','Send !!!');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //
    }

        /**
     * Show the messages today website on dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function message($id)
    {
        $message = Message::find($id);
        return response()->json($message);

    }

    /**
     * Show the messages today website on dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function messagesToday()
    {
        $messages = Message::where('reply_by',null)->get();
        return View::make('dashboard.message',compact('messages'))
        ->render();
    }

    /**
     * Show the messages all website on dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function messagesAll()
    {
        $messages = Message::All();
        return View::make('dashboard.message',compact('messages'))
        ->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reply(Request $request)
    {
        // dd($request->id);
        $id = $request->id;
        $email = $request->email;
        $message_r = $request->message;
        $reply = $request->reply;


        // return 'Email was sent';

        // $message = Message::create([
        //     'email' => $request->email,
        //     'message' => $request->message,
        // ]);

        // return redirect()->route('message.index')->with('pesan_sukses','Send !!!');

        $message=Message::find($id);
        $message->reply = $reply;
        $message->reply_by = Auth::user()->id;
        $message->save();
        // return redirect()->route('message.index')->with('pesan_sukses','Send !!!');
        Mail::to($email)->send(new ReplyContact($email, $message_r, $reply));
        return response()->json($message);
        // return redirect()->route('home')->with('pesan_sukses','email terkirim !');

    }
}

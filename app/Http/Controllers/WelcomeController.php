<?php

namespace App\Http\Controllers;

use DB;
use App\Website;
use App\Scholarship;
use Illuminate\Http\Request;
class WelcomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $scholarships = Scholarship::where('deleted_at',null)->orderBy('created_at', 'desc')->paginate(9);
        return view('welcome',compact('scholarships'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function portalSource()
    {
        // $scholarships = Scholarship::paginate(3);
        // $scholarships = DB::table('scholarships')->join('websites', 'websites.id', '=', 'scholarships.website_id')->where('websites.status','2')->paginate(9);
        $scholarships = DB::table('scholarships')->join('websites', 'websites.id', '=', 'scholarships.website_id')->where('websites.status','2')->where('scholarships.deleted_at',null)
        ->select('scholarships.*', 'websites.status')->orderBy('created_at', 'desc')->paginate(9);

        // $scholarships = Website::with('scholarships')->where('status','2')->get();
        return view('welcome',compact('scholarships'));

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function instanceSource()
    {
        // $scholarships = Scholarship::paginate(6);
        $scholarships = DB::table('scholarships')->join('websites', 'websites.id', '=', 'scholarships.website_id')->where('websites.status','3')->where('scholarships.deleted_at',null)
        ->select('scholarships.*', 'websites.status')->orderBy('created_at', 'desc')->paginate(9);
        return view('welcome',compact('scholarships'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function quickSearch($query)
    {

        $scholarships = DB::table('scholarships')->join('websites', 'websites.id', '=', 'scholarships.website_id')
        ->where('scholarships.name','like','%'.$query.'%')
        ->orWhere('scholarships.description','like','%'.$query.'%')->orWhere('websites.name','like','%'.$query.'%')->where('scholarships.deleted_at',null)
        ->select('scholarships.*', 'websites.name as website-name', 'websites.url as website-url')->get();
        // return view('welcome',compact('scholarships'));
        // dd($scholarships);
        // dd("sas");
        // $scholarships = Scholarship::paginate(6);
        // $scholarships = DB::table('scholarships')->join('websites', 'websites.id', '=', 'scholarships.website_id')->where('websites.status','3')
        // ->select('scholarships.*', 'websites.status')->paginate(9);
        // return view('welcome',compact('scholarships'));
        echo "<div class='m-list-search__results'>";
        if(!$scholarships->isEmpty()){
            echo "	<span class='m-list-search__result-category'>";
            echo "		Scholarship";
            echo "	</span>";
            foreach($scholarships as $scholarship){
                echo "	<a href='/show/".$scholarship->id."' class='m-list-search__result-item'>";
                echo "		<span class='m-list-search__result-item-icon'><i class='flaticon-lifebuoy m--font-warning'></i></span>";
                echo "		<span class='m-list-search__result-item-text'>".$scholarship->name."</span>";
                echo "	</a>";
            }

        }
        else{
            echo "	<span class='m-list-search__result-message'>";
            echo "		No record found";
            echo "	</span>";


        }
        // echo "	<span class='m-list-search__result-category m-list-search__result-category--first'>";
        // echo "		Documents";
        // echo "	</span>";
        // echo "	<a href='#' class='m-list-search__result-item'>";
        // echo "		<span class='m-list-search__result-item-icon'><i class='flaticon-interface-3 m--font-warning'></i></span>";
        // echo "		<span class='m-list-search__result-item-text'>Annual finance report</span>";
        // echo "	</a>";
        // echo "	<span class='m-list-search__result-category'>";
        // echo "		Customers";
        // echo "	</span>";
        // echo "	<a href='#' class='m-list-search__result-item'>";
        // echo "		<span class='m-list-search__result-item-pic'><img class='m--img-rounded' src='assets/app/media/img/users/user1.jpg' title=''/></span>";
        // echo "		<span class='m-list-search__result-item-text'>Amanda Anderson</span>";
        // echo "	</a>";
        echo "</div>";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Scholarship  $scholarship
     * @return \Illuminate\Http\Response
     */
    public function show(Scholarship $scholarship)
    {
        return view('welcome.show',compact('scholarship'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Scholarship  $scholarship
     * @return \Illuminate\Http\Response
     */
    public function edit(Scholarship $scholarship)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Scholarship  $scholarship
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Scholarship $scholarship)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Scholarship  $scholarship
     * @return \Illuminate\Http\Response
     */
    public function destroy(Scholarship $scholarship)
    {
        //
    }
}

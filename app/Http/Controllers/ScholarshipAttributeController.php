<?php

namespace App\Http\Controllers;

use App\Scholarship_attribute;
use App\ListUtil;
use Illuminate\Http\Request;
use App\Http\Requests\StoreAttribute;
use App\Http\Requests\UpdateAttribute;

class ScholarshipAttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fillScholarshipAttribute()
    {
        $scholarshipattribute = Scholarship_attribute::where('name','<>',null)->pluck('name', 'id');

        return json_encode($scholarshipattribute);

    }

    /**
     * Show data from model for DataTable.
     *
     * @return \Illuminate\Http\Response
     */
    public function getScholarshipAttribute()
    {
        $scholarship_attributes = Scholarship_attribute::All();

        $data = $alldata = json_decode($scholarship_attributes);

        $datatable = array_merge(['pagination' => [], 'sort' => [], 'query' => []], $_REQUEST);

        $filter = isset($datatable['query']['generalSearch']) && is_string($datatable['query']['generalSearch'])
                    ? $datatable['query']['generalSearch'] : '';

        if (! empty($filter)) {
            $data = array_filter($data, function ($a) use ($filter) {
                return (boolean)preg_grep("/$filter/i", (array)$a);
            });

            unset($datatable['query']['generalSearch']);
        }

        $query = isset($datatable['query']) && is_array($datatable['query']) ? $datatable['query'] : null;

        if (is_array($query)) {
            $query = array_filter($query);

            foreach ($query as $key => $val) {
                $data = $this->list_filter($data, [$key => $val]);
            }
        }

        $sort  = ! empty($datatable['sort']['sort']) ? $datatable['sort']['sort'] : 'asc';
        $field = ! empty($datatable['sort']['field']) ? $datatable['sort']['field'] : 'RecordID';

        $meta    = [];
        $page    = ! empty($datatable['pagination']['page']) ? (int)$datatable['pagination']['page'] : 1;
        $perpage = ! empty($datatable['pagination']['perpage']) ? (int)$datatable['pagination']['perpage'] : -1;

        $pages = 1;
        $total = count($data);

        usort($data, function ($a, $b) use ($sort, $field) {
            if (! isset($a->$field) || ! isset($b->$field)) {
                return false;
            }

            if ($sort === 'asc') {
                return $a->$field > $b->$field ? true : false;
            }

            return $a->$field < $b->$field ? true : false;
        });

        if ($perpage > 0) {
            $pages  = ceil($total / $perpage);
            $page   = max($page, 1);
            $page   = min($page, $pages);
            $offset = ($page - 1) * $perpage;

            if ($offset < 0) {
                $offset = 0;
            }

            $data = array_slice($data, $offset, $perpage, true);
        }

        $meta = [
            'page'    => $page,
            'pages'   => $pages,
            'perpage' => $perpage,
            'total'   => $total,
        ];

        if (isset($datatable['requestIds']) && filter_var($datatable['requestIds'], FILTER_VALIDATE_BOOLEAN)) {
            $meta['rowIds'] = array_map(function ($row) {
                return $row->RecordID;
            }, $alldata);
        }

        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $result = [
            'meta' => $meta + [
                    'sort'  => $sort,
                    'field' => $field,
                ],
            'data' => $data,
        ];

        echo json_encode($result, JSON_PRETTY_PRINT);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('attribute.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAttribute $request)
    {
        if($request->attribute_id==0){
            $Scholarship_attribute = Scholarship_attribute::create([
                'website_id' => $request->website_id,
                'name' => $request->name,
                'url' => $request->url,
                'attribute1' => $request->attribute1,
                'attribute_title' => $request->title,
                'attribute_description' => $request->description,
                'attribute_picture' => $request->picture,
                'attribute_registration' => $request->registration,
                'attribute_period' => $request->period,
                'attribute_additional_1' => $request->attribute_additional1,
                'attribute_additional_2' => $request->attribute_additional2,
                'attribute_additional_3' => $request->attribute_additional3,
                'attribute_additional_4' => $request->attribute_additional4,
                'attribute_additional_5' => $request->attribute_additional5,
            ]);

        }
        else{
            $Scholarship_attribute = Scholarship_attribute::create([
                'attribute_id' => $request->attribute_id,
                'website_id' => $request->website_id,
                'url' => $request->url,
                'attribute1' => $request->attribute1,
                'attribute_title' => $request->title,
                'attribute_description' => $request->description,
                'attribute_picture' => $request->picture,
                'attribute_registration' => $request->registration,
                'attribute_period' => $request->period,
                'attribute_additional_1' => $request->attribute_additional1,
                'attribute_additional_2' => $request->attribute_additional2,
                'attribute_additional_3' => $request->attribute_additional3,
                'attribute_additional_4' => $request->attribute_additional4,
                'attribute_additional_5' => $request->attribute_additional5,
            ]);

        }

        return response()->json($Scholarship_attribute);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Scholarship_attribute  $scholarship_attribute
     * @return \Illuminate\Http\Response
     */
    public function show(Scholarship_attribute $scholarship_attribute)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Scholarship_attribute  $scholarship_attribute
     * @return \Illuminate\Http\Response
     */
    public function edit(Scholarship_attribute $scholarship_attribute)
    {
        return response()->json($scholarship_attribute);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Scholarship_attribute  $scholarship_attribute
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAttribute $request, Scholarship_attribute $Scholarship_attribute)
    {
        if($request->attribute_id==0){
            $Scholarship_attribute->website_id = $request->website_id;
            $Scholarship_attribute->attribute_id = null;
            $Scholarship_attribute->name = $request->name;
            $Scholarship_attribute->url = $request->url;
            $Scholarship_attribute->attribute1 = $request->attribute1;
            $Scholarship_attribute->attribute_title = $request->title;
            $Scholarship_attribute->attribute_description = $request->description;
            $Scholarship_attribute->attribute_picture = $request->picture;
            $Scholarship_attribute->attribute_registration = $request->registration;
            $Scholarship_attribute->attribute_period = $request->period;
            $Scholarship_attribute->attribute_additional_1 = $request->attribute_additional1;
            $Scholarship_attribute->attribute_additional_2 = $request->attribute_additional2;
            $Scholarship_attribute->attribute_additional_3 = $request->attribute_additional3;
            $Scholarship_attribute->attribute_additional_4 = $request->attribute_additional4;
            $Scholarship_attribute->attribute_additional_5 = $request->attribute_additional5;

            $Scholarship_attribute->save();
        }
        else{
            $Scholarship_attribute->attribute_id = $request->attribute_id;
            $Scholarship_attribute->website_id = $request->website_id;
            $Scholarship_attribute->name = null;
            $Scholarship_attribute->url = $request->url;
            $Scholarship_attribute->attribute1 = $request->attribute1;
            $Scholarship_attribute->attribute_title = $request->title;
            $Scholarship_attribute->attribute_description = $request->description;
            $Scholarship_attribute->attribute_picture = $request->picture;
            $Scholarship_attribute->attribute_registration = $request->registration;
            $Scholarship_attribute->attribute_period = $request->period;
            $Scholarship_attribute->attribute_additional_1 = $request->attribute_additional1;
            $Scholarship_attribute->attribute_additional_2 = $request->attribute_additional2;
            $Scholarship_attribute->attribute_additional_3 = $request->attribute_additional3;
            $Scholarship_attribute->attribute_additional_4 = $request->attribute_additional4;
            $Scholarship_attribute->attribute_additional_5 = $request->attribute_additional5;

            $Scholarship_attribute->save();

        }
        return response()->json($Scholarship_attribute);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Scholarship_attribute  $scholarship_attribute
     * @return \Illuminate\Http\Response
     */
    public function destroy(Scholarship_attribute $scholarship_attribute)
    {
        $scholarship_attribute->delete();

        return response()->json($scholarship_attribute);
    }

    /**
     * Show data from model with flter on datatable.
     *
     * @param $list, $args, $operator
     * @return \Illuminate\Http\Response
     */
    public function list_filter($list, $args = array(), $operator = 'AND')
    {
        if (! is_array($list)) {
            return array();
        }

        $util = new ListUtil($list);

        return $util->filter($args, $operator);
    }
}

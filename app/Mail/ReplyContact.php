<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReplyContact extends Mailable
{
    use Queueable, SerializesModels;
    public $email,$message_r,$reply;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $message_r, $reply)
    {
        $this->email = $email;
        $this->message_r = $message_r;
        $this->reply = $reply;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.reply');
    }
}

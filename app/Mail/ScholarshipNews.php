<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ScholarshipNews extends Mailable
{
    use Queueable, SerializesModels;
    public $email,$scholarship;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $scholarship)
    {
        $this->email = $email;
        $this->scholarship = $scholarship;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.subscribe');
    }
}

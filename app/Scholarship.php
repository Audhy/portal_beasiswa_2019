<?php

namespace App;

use App\Website;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Scholarship extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'description',
        'uri',
        'picture',
        'reg',
        'additional',
        'website_id',
        'attribute_id',
    ];

    /*************************************** RELATIONSHIP ****************************************/

    /**
     * One-to-Many: A website may have zero or many scholarship.
     *
     * This function will retrieve the header of a general license.
     *
     * @return mixed
     */
    public function website()
    {
        return $this->belongsTo(Website::class);
    }

    /**
     * One-to-Many
     *
     * @return mixed
     */
    public function periods()
    {
        // This method must have a second parameter as FK column (scholarship_id),
        // so these following error will not thrown:
        // "Too few arguments to function Illuminate\Database\Eloquent\Model::setAttribute()"

        return $this->hasMany(Period::class, 'scholarship_id');
    }
}

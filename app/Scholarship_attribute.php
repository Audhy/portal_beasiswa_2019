<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Scholarship_attribute extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'url',
        'name',
        'attribute_id',
        'website_id',
        'attribute1',
        // 'attribute2',
        'attribute_title',
        'attribute_picture',
        'attribute_description',
        'attribute_registration',
        'attribute_period',
        'attribbute_id',
        'attribute_additional_1',
        'attribute_additional_2',
        'attribute_additional_3',
        'attribute_additional_4',
        'attribute_additional_5',
    ];

    /*************************************** RELATIONSHIP ****************************************/


    /**
     * One-to-Many: A website may have zero or many scholarship.
     *
     * This function will retrieve the header of a general license.
     *
     * @return mixed
     */
    public function website()
    {
        return $this->belongsTo(Website::class);
    }

}

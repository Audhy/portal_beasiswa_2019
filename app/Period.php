<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Period extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'period',
        'scholarship_id',
    ];

    /*************************************** RELATIONSHIP ****************************************/

    /**
     * One-to-Many: A period may have zero or many scholarship.
     *
     * This function will retrieve the header of a general license.
     *
     * @return mixed
     */
    public function scholarship()
    {
        return $this->belongsTo(Scholarship::class);
    }
}

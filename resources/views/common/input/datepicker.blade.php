<input
    type="text"
    id="{{ $id or 'm_datepicker_1' }}"
    name="{{ $name or '' }}"
    class="form-control
           {{$class or ''}}"
    style="{{$style or ''}}"
    placeholder="{{ $placeholder or '' }}"
    readonly
>

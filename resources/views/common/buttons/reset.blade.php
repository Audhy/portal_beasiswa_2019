<button
    type="{{ $type or 'reset' }}"
    id="{{ $id or '' }}"
    name="{{ $name or '' }}"
    value="{{ $value or '' }}"
    class="btn
           btn-{{ $color or 'warning' }}
           btn-{{ $size or 'md' }}
           {{ $class or '' }} reset"
    style="{{ $style or '' }}">

    <span>
        <i class="la la-{{ $icon or 'mail-reply' }}"></i>
        <span>{{ $text or 'Reset' }}</span>
    </span>
</button>
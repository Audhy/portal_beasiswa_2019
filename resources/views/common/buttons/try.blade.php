<button
    type="{{ $type or 'button' }}"
    id="{{ $id or '' }}"
    name="{{ $name or 'try' }}"
    class="btn
           btn-{{ $color or 'info' }}
           btn-{{ $size or 'md' }}
               {{ $class or '' }} try"
    style="{{ $style or '' }}"
    value="{{ $value or '' }}"
    target="{{ $target or '' }}"
    data-toggle="{{ $data_toggle or 'modal' }}"
    data-target="{{ $data_target or '#' }}"
    {{ $attribute or '' }}
    >

    <span>
        <i class="fa {{ $icon or 'flaticon-rocket' }}"></i>
        <span>{{ $text or 'Try' }}</span>
    </span>
</button>


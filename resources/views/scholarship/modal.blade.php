<div class="modal fade" id="modal_scholarship" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="TitleModalScholarship">Scholarship</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="ScholarshipForm">
                    <input type="hidden" class="form-control form-control-danger m-input" name="id" id="id">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row ">
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <label class="form-control-label">
                                    Name @include('common.label.required')
                                </label>

                                @component('common.input.text')
                                    @slot('id', 'name')
                                    @slot('text', 'Name')
                                    @slot('name', 'name')
                                    @slot('id_error','name')
                                @endcomponent
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                        <label class="form-control-label">
                                            Website @include('common.label.required')
                                        </label>
                                @component('common.input.select')
                                    @slot('id', 'website2')
                                    @slot('text', 'Website')
                                    @slot('name', 'website2')
                                    @slot('style', 'width:100%')
                                    @slot('id_error','website_id')
                                @endcomponent
                                {{-- @component('common.buttons.create-new')
                                    @slot('size', 'sm')
                                    @slot('text', 'Add Website')
                                    @slot('data_target', '#modal_website')
                                    @slot('style', 'margin-top: 10px;')
                                @endcomponent --}}

                            </div>
                        </div>
                        <div class="form-group m-form__group row ">
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                        <label class="form-control-label">
                                            Uri @include('common.label.required')
                                        </label>

                                        @component('common.input.text')
                                            @slot('id', 'uri')
                                            @slot('text', 'Uri')
                                            @slot('name', 'uri')
                                            @slot('id_error', 'uri')
                                        @endcomponent
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <label class="form-control-label">
                                            Period @include('common.label.required')
                                        </label>

                                        @component('common.input.text')
                                            @slot('id', 'period')
                                            @slot('text', 'period')
                                            @slot('name', 'period')
                                            @slot('id_error','period')
                                        @endcomponent
                                    </div>


                        </div>
                        <div class="form-group m-form__group row ">
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                        <label class="form-control-label">
                                            Poster @include('common.label.optional')
                                        </label>

                                        @component('common.input.upload')
                                            @slot('id', 'poster')
                                            @slot('text', 'Poster')
                                            @slot('name', 'poster')
                                        @endcomponent
                                    </div>
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                        <label class="form-control-label">
                                            Description @include('common.label.optional')
                                        </label>

                                        @component('common.input.textarea')
                                            @slot('rows', '3')
                                            @slot('id', 'description')
                                            @slot('name', 'description')
                                            @slot('text', 'Description')
                                            @slot('description', 'text')
                                        @endcomponent
                                    </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <div id="button-div">
                            @component('common.buttons.submit')
                                @slot('size', 'md')
                                @slot('class', 'add')
                            @endcomponent
                        </div>
                        @component('common.buttons.reset')
                            @slot('size', 'md')
                        @endcomponent

                        @component('common.buttons.close')
                            @slot('size', 'md')
                            @slot('data_dismiss', 'modal')
                        @endcomponent
                    </div>
                </form>

               </div>
            </div>
        </div>
    </div>

@extends('layouts.master')
@section('content')
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title ">Dashboard</h3>
        </div>
    </div>
</div>
<!-- END: Subheader -->

<div class="m-content">
    <!--begin:: Widgets/Stats-->
    <div class="m-portlet ">
        <div class="m-portlet__body  m-portlet__body--no-padding">
            <div class="row m-row--no-padding m-row--col-separator-xl">
                <div class="col-md-12 col-lg-6 col-xl-3">
                    <!--begin::Total Profit-->
                    <div class="m-widget24">
                        <div class="m-widget24__item">
                            <h4 class="m-widget24__title">
                                Subscribers
                            </h4><br>
                            <span class="m-widget24__desc">
                                Total of Subscriber
                            </span>
                            <span class="m-widget24__stats m--font-brand">
                                {{$total_subscriber}}
                            </span>
                            <div class="m--space-10"></div>
                            <div class="progress m-progress--sm">
                                <div class="progress-bar m--bg-brand" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <span class="m-widget24__change">
                                <a href="{{ route('subscriber.index') }}">Detail</a>
                            </span>
                        </div>
                    </div>
                    <!--end::Total Profit-->
                </div>
                <div class="col-md-12 col-lg-6 col-xl-3">
                    <!--begin::New Feedbacks-->
                    <div class="m-widget24">
                        <div class="m-widget24__item">
                            <h4 class="m-widget24__title">
                                Website
                            </h4><br>
                            <span class="m-widget24__desc">
                                Total of Website
                            </span>
                            <span class="m-widget24__stats m--font-info">
                                {{$total_website}}
                            </span>
                            <div class="m--space-10"></div>
                            <div class="progress m-progress--sm">
                                <div class="progress-bar m--bg-info" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <span class="m-widget24__change">
                                <a href="{{ route('website.index') }}">Detail</a>
                            </span>
                        </div>
                    </div>
                    <!--end::New Feedbacks-->
                </div>
                <div class="col-md-12 col-lg-6 col-xl-3">
                    <!--begin::New Orders-->
                    <div class="m-widget24">
                        <div class="m-widget24__item">
                            <h4 class="m-widget24__title">
                                Scholarship
                            </h4><br>
                            <span class="m-widget24__desc">
                                Total of Scholarship
                            </span>
                            <span class="m-widget24__stats m--font-danger">
                                    {{$total_scholarship}}
                            </span>
                            <div class="m--space-10"></div>
                            <div class="progress m-progress--sm">
                                <div class="progress-bar m--bg-danger" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <span class="m-widget24__change">
                                <a href="{{ route('scholarship.index') }}">Detail</a>
                            </span>
                        </div>
                    </div>
                    <!--end::New Orders-->
                </div>
                <div class="col-md-12 col-lg-6 col-xl-3">
                    <!--begin::New Users-->
                    <div class="m-widget24">
                        <div class="m-widget24__item">
                            <h4 class="m-widget24__title">
                                Messages
                            </h4><br>
                            <span class="m-widget24__desc">
                                Messages haven't Reply
                            </span>
                            <span class="m-widget24__stats m--font-success">
                                {{$total_message}}
                            </span>
                            <div class="m--space-10"></div>
                            <div class="progress m-progress--sm">
                                <div class="progress-bar m--bg-success" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                    <!--end::New Users-->
                </div>
            </div>
        </div>
    </div>
    <!--end:: Widgets/Stats-->

    <div class="row">
        <div class="col-xl-4">
            <!--begin:: Widgets/New Users-->
            <div class="m-portlet m-portlet--full-height ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Suggestions
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_widget4_tab1_content" role="tab">
                                Web
                              </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="m_widget4_tab1_content">
                            <div class="m-scrollable" data-scrollable="true" data-height="400" style="height: 400px; overflow: hidden;">
                                <!--begin::Widget 14-->
                                <div class="m-widget4 suggestion">
                                    <div id="suggestion-div">
                                        @foreach($suggestions as $suggestion)
                                        <div class="m-widget4__item">

                                            <div class="m-widget4__info">
                                                <span class="m-widget4__title">
                                                              {{str_limit($suggestion->url, 15)}}
                                                        </span><br>
                                                <span class="m-widget4__sub">
                                                          {{$suggestion->created_at}}
                                                      </span>
                                            </div>
                                            <div class="m-widget4__ext">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <button data-toggle="modal" data-target="#modal_website" type="button" href="#" class="m-btn m-btn--pill m-btn--hover-accent btn btn-sm btn-secondary edit"
                                                                title="Add" data-id='{{$suggestion->id}}' data-idweb='{{$suggestion->id}}'><i class="la la-pencil"><span>Add</span></i></button>
                                                        </td>
                                                        <td>
                                                            <a href="{{route('ignore',$suggestion->id)}}"><button  type="button" href="" class="m-btn m-btn--pill m-btn--hover-secondary btn btn-sm btn-danger ignore" title="Ignore" data-id='{{$suggestion->id}}'><span>Ignore</span></button></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>


                                        </div>
                                        @endforeach
    @include('website.modal')
                                    </div>{{-- ajax div --}}
                                    <!--end::Widget 14 Item-->
                                </div>
                                <!--end::Widget 14-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end:: Widgets/New Users-->
        </div>

        <div class="col-xl-4">
            <!--begin:: Widgets/New Users-->
            <div class="m-portlet m-portlet--full-height ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Messages
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_widget4_tab2_content3" role="tab">
                            All
                            </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="tab-content">
                        <div class="tab-pane " id="m_widget4_tab1_content2">
                        </div>
                        <div class="tab-pane active" id="m_widget4_tab2_content3">
                            <div class="m-scrollable" data-scrollable="true" data-height="400" style="height: 400px; overflow: hidden;">

                                <!--begin::Widget 14-->
                                <div class="m-widget4">
                                    <div id="massage-all-div">
                                        @foreach($messages as $message)
                                        <div class="m-widget4__item">
                                            <div class="m-widget4__img m-widget4__img--pic">
                                            </div>
                                            <div class="m-widget4__info">
                                                <span class="m-widget4__title">
                                                        {{$message->email}}
                                                </span><br>
                                                <span class="m-widget4__sub">
                                                        {{str_limit($message->message, 50,' ...')}}
                                                </span>
                                            </div>
                                            <div class="m-widget4__ext">
                                                <table>
                                                    <tr>
                                                        <td class="reply">
                                                            @if($message->reply== null)
                                                            <button data-toggle="modal" data-target="#modal_reply" type="button" href="#" class="m-btn m-btn--pill m-btn--hover-accent btn btn-sm btn-secondary mail-reply"
                                                                title="reply" data-id='{{$message->id}}'><i class="la la-mail-reply"><span>Reply</span></i></button>                                                            @else
                                                            <button data-toggle="modal" data-target="#modal_show" type="button" href="#" class="m-btn m-btn--pill m-btn--hover-accent btn btn-sm btn-secondary mail-show"
                                                                title="show" data-id='{{$message->id}}'><i class="la la-mail-reply"><span>Show</span></i></button>                                                            @endif
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        @endforeach
    @include('dashboard.reply')
    @include('dashboard.show')
                                    </div>
                                    <!--end::Widget 14 Item-->
                                </div>
                            </div>
                            <!--end::Widget 14-->
                        </div>
                    </div>
                </div>
            </div>
            <!--end:: Widgets/New Users-->
        </div>



        <div class="col-xl-4">
            <!--begin:: Widgets/Audit Log-->
            <div class="m-portlet m-portlet--full-height ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Log
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
                            <table>
                                <tr>
                                    <td>
                                        <a class="nav-link m-tabs__link active" href="/scraper-suggestion" target="_blank">
                                            Grap Suggestion
                                        </a>

                                    </td>
                                    <td>
                                        <a class="nav-link m-tabs__link active" href="/scraper-web" target="_blank">
                                            Grap Scholarship
                                        </a>
                                    </td>

                                </tr>
                                {{-- <tr>
                                </tr> --}}
                            </table>
                            {{-- <li class="nav-item m-tabs__item">
                            </li>
                            <br>
                            <li class="nav-item m-tabs__item">
                            </li> --}}
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="m_widget4_tab1_content">
                            <div class="m-scrollable" data-scrollable="true" data-height="400" style="height: 400px; overflow: hidden;">
                                <div class="m-list-timeline m-list-timeline--skin-light">
                                    <div class="m-list-timeline__items">
                                        @foreach($logs as $log)
                                        @if($log->status == 1)
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--success"></span>
                                            @if($log->type == 1)
                                            <span href="" class="m-list-timeline__text">Grap Suggestion Success</span>
                                            @elseif($log->type == 2)
                                            <span href="" class="m-list-timeline__text">Grap Website Success</span>
                                            @else
                                            <span href="" class="m-list-timeline__text">Broadcast Success</span>
                                            @endif
                                            <span class="m-list-timeline__time">{{$log->created_at}}</span>
                                        </div>

                                        @else
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--danger"></span>
                                            @if($log->type == 1)
                                            <span href="" class="m-list-timeline__text">Grap Suggestion Failed
                                            @component('common.buttons.create-new')
                                                @slot('size', 'sm')
                                                @slot('class', 'error')
                                                @slot('data_toggle', $log->message)
                                                @slot('color', 'danger')
                                                @slot('text', 'Message')
                                                @slot('icon', 'envelope')
                                            @endcomponent

                                            </span>
                                            @elseif($log->type == 2)
                                            <span href="" class="m-list-timeline__text">Grap Website Failed
                                            @component('common.buttons.create-new')
                                                @slot('size', 'sm')
                                                @slot('class', 'error')
                                                @slot('data_toggle', $log->message)
                                                @slot('color', 'danger')
                                                @slot('text', 'Message')
                                                @slot('icon', 'envelope')
                                            @endcomponent

                                            </span>
                                            @else
                                            <span href="" class="m-list-timeline__text">Grap Broadcast
                                            @component('common.buttons.create-new')
                                                @slot('size', 'sm')
                                                @slot('class', 'error')
                                                @slot('data_toggle', $log->message)
                                                @slot('color', 'danger')
                                                @slot('text', 'Message')
                                                @slot('icon', 'envelope')
                                            @endcomponent

                                            </span>
                                            @endif
                                            <span class="m-list-timeline__time">{{$log->created_at}}</span>
                                        </div>
                                        @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end:: Widgets/Audit Log-->
        </div>

    </div>
    <!--End::Section-->








</div>
@endsection
 @push('footer-scripts')

 <script>
    $('.error').on('click', function () {
        var trigger = $(this).data('toggle');
        alert(trigger);
    });

 </script>
<script src="{{ asset('js/dashboard.js') }}"></script>



@endpush

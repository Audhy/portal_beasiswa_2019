<div class="m-list-timeline__item">
        <span class="m-list-timeline__badge m-list-timeline__badge--success"></span>
        <span class="m-list-timeline__text">12 new users registered</span>
        <span class="m-list-timeline__time">Just now</span>
    </div>
    <div class="m-list-timeline__item">
        <span class="m-list-timeline__badge m-list-timeline__badge--info"></span>
        <span class="m-list-timeline__text">System shutdown <span class="m-badge m-badge--success m-badge--wide">pending</span></span>
        <span class="m-list-timeline__time">14 mins</span>
    </div>
    <div class="m-list-timeline__item">
        <span class="m-list-timeline__badge m-list-timeline__badge--danger"></span>
        <span class="m-list-timeline__text">New invoice received</span>
        <span class="m-list-timeline__time">20 mins</span>
    </div>
    <div class="m-list-timeline__item">
        <span class="m-list-timeline__badge m-list-timeline__badge--accent"></span>
        <span class="m-list-timeline__text">DB overloaded 80% <span class="m-badge m-badge--info m-badge--wide">settled</span></span>
        <span class="m-list-timeline__time">1 hr</span>
    </div>
    <div class="m-list-timeline__item">
        <span class="m-list-timeline__badge m-list-timeline__badge--warning"></span>
        <span class="m-list-timeline__text">System error - <a href="#" class="m-link">Check</a></span>
        <span class="m-list-timeline__time">2 hrs</span>
    </div>
    <div class="m-list-timeline__item">
        <span class="m-list-timeline__badge m-list-timeline__badge--brand"></span>
        <span class="m-list-timeline__text">Production server down</span>
        <span class="m-list-timeline__time">3 hrs</span>
    </div>
    <div class="m-list-timeline__item">
        <span class="m-list-timeline__badge m-list-timeline__badge--info"></span>
        <span class="m-list-timeline__text">Production server up</span>
        <span class="m-list-timeline__time">5 hrs</span>
    </div>
    <div class="m-list-timeline__item">
        <span class="m-list-timeline__badge m-list-timeline__badge--success"></span>
        <span href="" class="m-list-timeline__text">New order received <span class="m-badge m-badge--danger m-badge--wide">urgent</span></span>
        <span class="m-list-timeline__time">7 hrs</span>
    </div>
    <div class="m-list-timeline__item">
        <span class="m-list-timeline__badge m-list-timeline__badge--success"></span>
        <span class="m-list-timeline__text">12 new users registered</span>
        <span class="m-list-timeline__time">Just now</span>
    </div>
    <div class="m-list-timeline__item">
        <span class="m-list-timeline__badge m-list-timeline__badge--info"></span>
        <span class="m-list-timeline__text">System shutdown <span class="m-badge m-badge--success m-badge--wide">pending</span></span>
        <span class="m-list-timeline__time">14 mins</span>
    </div>
    <div class="m-list-timeline__item">
        <span class="m-list-timeline__badge m-list-timeline__badge--danger"></span>
        <span class="m-list-timeline__text">New invoice received</span>
        <span class="m-list-timeline__time">20 mins</span>
    </div>
    <div class="m-list-timeline__item">
        <span class="m-list-timeline__badge m-list-timeline__badge--accent"></span>
        <span class="m-list-timeline__text">DB overloaded 80% <span class="m-badge m-badge--info m-badge--wide">settled</span></span>
        <span class="m-list-timeline__time">1 hr</span>
    </div>
    <div class="m-list-timeline__item">
        <span class="m-list-timeline__badge m-list-timeline__badge--danger"></span>
        <span class="m-list-timeline__text">New invoice received</span>
        <span class="m-list-timeline__time">20 mins</span>
    </div>
    <div class="m-list-timeline__item">
        <span class="m-list-timeline__badge m-list-timeline__badge--accent"></span>
        <span class="m-list-timeline__text">DB overloaded 80% <span class="m-badge m-badge--info m-badge--wide">settled</span></span>
        <span class="m-list-timeline__time">1 hr</span>
    </div>
    <div class="m-list-timeline__item">
        <span class="m-list-timeline__badge m-list-timeline__badge--warning"></span>
        <span class="m-list-timeline__text">System error - <a href="#" class="m-link">Check</a></span>
        <span class="m-list-timeline__time">2 hrs</span>
    </div>
    <div class="m-list-timeline__item">
        <span class="m-list-timeline__badge m-list-timeline__badge--brand"></span>
        <span class="m-list-timeline__text">Production server down</span>
        <span class="m-list-timeline__time">3 hrs</span>
    </div>
    <div class="m-list-timeline__item">
        <span class="m-list-timeline__badge m-list-timeline__badge--info"></span>
        <span class="m-list-timeline__text">Production server up</span>
        <span class="m-list-timeline__time">5 hrs</span>
    </div>
    <div class="m-list-timeline__item">
        <span class="m-list-timeline__badge m-list-timeline__badge--success"></span>
        <span href="" class="m-list-timeline__text">New order received <span class="m-badge m-badge--danger m-badge--wide">urgent</span></span>
        <span class="m-list-timeline__time">7 hrs</span>
    </div>
@foreach($suggestions as $suggestion)
<div class="m-widget4__item">
                    <div class="m-widget4__info">
                            <span class="m-widget4__title">
                                  {{str_limit($suggestion->url, 15)}} 
                            </span><br> 
                            <span class="m-widget4__sub">
                              {{$suggestion->created_at}}
                          </span>							 		 
                        </div>
                    <div class="m-widget4__ext">
                            <table>
                                <tr>
                                    <td>
                                          <button data-toggle="modal" data-target="#modal_website" type="button" href="#" class="m-btn m-btn--pill m-btn--hover-accent btn btn-sm btn-secondary edit" title="Add" data-id='{{$suggestion->id}}'><i class="la la-pencil"><span>Add</span></i></button>
                                    </td>
                                    <td>
                                          <a href="{{route('ignore',$suggestion->id)}}"><button  type="button" href="" class="m-btn m-btn--pill m-btn--hover-secondary btn btn-sm btn-danger ignore" title="Ignore" data-id='{{$suggestion->id}}'><span>Ignore</span></button></a>                                                  
                                    </td>
                                </tr>
                            </table>
                        </div>


                              </div>

@endforeach


@include('website.modal')

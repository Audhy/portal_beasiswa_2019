<div class="modal fade" id="modal_reply" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="TitleModalReply">Reply</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="ReplyForm">
                    <input type="hidden" class="form-control form-control-danger m-input" name="id" id="id">
                    <div class="m-portlet__body">
                        <input type="hidden" name="id" id="id">
                        <div class="form-group m-form__group row ">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <label class="form-control-label">
                                    Email @include('common.label.required')
                                </label>

                                @component('common.input.text')
                                    @slot('id', 'email')
                                    @slot('text', 'Email')
                                    @slot('name', 'email')
                                    @slot('editable','readonly')
                                @endcomponent
                            </div>
                        </div>
                        <div class="form-group m-form__group row ">
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <label class="form-control-label">
                                    Message @include('common.label.required')
                                </label>
                                <br>
                                @component('common.input.textarea')
                                @slot('rows', '4')
                                @slot('id', 'message')
                                @slot('name', 'message')
                                @slot('text', 'Message')
                                @slot('description', 'text')
                                @slot('editable','readonly')
                            @endcomponent
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <label class="form-control-label">
                                    Reply @include('common.label.required')
                                </label>

                                @component('common.input.textarea')
                                    @slot('rows', '4')
                                    @slot('id', 'reply')
                                    @slot('name', 'reply')
                                    @slot('text', 'Reply')
                                    @slot('description', 'text')
                                @endcomponent
                                            </div>
                        </div>
                        
                    </div>
                    <div class="modal-footer sent-mail">

                        <div id="button-div">
                            @component('common.buttons.submit')
                                @slot('size', 'md')
                                @slot('class', 'sent')
                                @slot('text', 'sent')
                                @slot('icon', 'fa-location-arrow')
                            @endcomponent
                        </div>
                    </div>
                </form>

               </div>
            </div>
        </div>
    </div>

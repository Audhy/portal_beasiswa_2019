<div class="modal fade" id="modal_show" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="TitleModalshow">Reply</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="showForm">
                    <input type="hidden" class="form-control form-control-danger m-input" name="id" id="id">
                    <div class="m-portlet__body">
                        <input type="hidden" name="id" id="id">
                        <div class="form-group m-form__group row ">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <label class="form-control-label">
                                    Email @include('common.label.required')
                                </label>

                                @component('common.input.text')
                                    @slot('id', 'email_show')
                                    @slot('text', 'Email_show')
                                    @slot('name', 'email')
                                    @slot('editable','readonly')
                                @endcomponent
                            </div>
                        </div>
                        <div class="form-group m-form__group row ">
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <label class="form-control-label">
                                    Message @include('common.label.required')
                                </label>
                                <br>
                                @component('common.input.textarea')
                                @slot('rows', '4')
                                @slot('id', 'message_show')
                                @slot('name', 'message_show')
                                @slot('text', 'Message')
                                @slot('description', 'text')
                                @slot('editable','readonly')
                            @endcomponent
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <label class="form-control-label">
                                    Reply @include('common.label.required')
                                </label>

                                @component('common.input.textarea')
                                    @slot('rows', '4')
                                    @slot('id', 'reply_show')
                                    @slot('name', 'reply_show')
                                    @slot('text', 'Reply')
                                    @slot('editable','readonly')
                                    @slot('description', 'text')
                                @endcomponent
                                            </div>
                        </div>
                        
                    </div>
                    <div class="modal-footer sent-mail">

                        <div id="button-div">
                                @component('common.buttons.close')
                                @slot('size', 'md')
                                @slot('data_dismiss', 'modal')
                            @endcomponent
                            </div>
                    </div>
                </form>

               </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="modal_administrator" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="TitleModalAdministrator">Administrator</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="AdministratorForm">
                    <input type="hidden" class="form-control form-control-danger m-input" name="id" id="id">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row ">
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <label class="form-control-label">
                                    Name @include('common.label.required')
                                </label>

                                @component('common.input.text')
                                    @slot('id', 'name')
                                    @slot('text', 'Name')
                                    @slot('name', 'name')
                                    @slot('id_error', 'name')
                                @endcomponent
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <label class="form-control-label">
                                    Email @include('common.label.required')
                                </label>

                                @component('common.input.email')
                                    @slot('id', 'email')
                                    @slot('text', 'Email')
                                    @slot('name', 'email')
                                    @slot('id_error', 'email')
                                @endcomponent
                            </div>
                        </div>
                        <div class="form-group m-form__group row ">
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <label class="form-control-label">
                                    Password @include('common.label.required')
                                </label>

                                @component('common.input.password')
                                    @slot('id', 'password')
                                    @slot('text', 'Password')
                                    @slot('name', 'password')
                                    @slot('id_error', 'password')
                                @endcomponent
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <label class="form-control-label">
                                    Password Confirm @include('common.label.required')
                                </label>

                                @component('common.input.password')
                                    @slot('id', 'confirm')
                                    @slot('text', 'confirm')
                                    @slot('name', 'confirm')
                                @endcomponent
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div id="button-div">
                            @component('common.buttons.submit')
                                @slot('size', 'md')
                            @endcomponent
                        </div>
                        @component('common.buttons.reset')
                            @slot('size', 'md')
                        @endcomponent

                        @component('common.buttons.close')
                            @slot('size', 'md')
                            @slot('data_dismiss', 'modal')
                        @endcomponent
                    </div>
                </form>

               </div>
            </div>
        </div>
    </div>

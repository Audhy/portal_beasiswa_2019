<div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                        <i class="flaticon-statistics"></i>
                    </span>
                <h3 class="m-portlet__head-text">
                    Register your email to follow news information
                </h3>
                <h2 class="m-portlet__head-label m-portlet__head-label--primary">
                    <span>About Us</span>
                </h2>
            </div>
        </div>
        <div class="m-portlet__head-tools">

        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-md-6 col-lg-6">

            <div class="m-portlet__body">
                <form method="POST" action="{{route('subscriber.store')}}" class="form-horizontal">

                    {!! csrf_field() !!} @if (session('pesan_sukses'))
                    <div class="alert alert-success">
                        {{ session('pesan_sukses') }}
                    </div>
                    @endif

                    <label class="form-control-label">
                Email
            </label>
                    <div class="row">
                        <div class="col-sm-8 col-md-8 col-lg-8">

                            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                                    @component('common.input.email') @slot('id', 'email') @slot('text', 'Email') @slot('name', 'email') @endcomponent

                                    <?php
                                echo $errors->first('email','<span class="help-block">:message</span>');
                                ?>

                            </div>

                        </div>
                        <div class="col-sm-4 col-md-4 col-lg-4">

                            @component('common.buttons.submit') @slot('size', 'md') @slot('text', 'Subsribe') @slot('type', 'submit') @slot('icon', 'fa-location-arrow')
                            @endcomponent
                        </div>
                    </div>
                </form>

            </div>
        </div>
        <div class="col-sm-6 col-md-6 col-lg-6">

            <div class="m-portlet__body">
                <label class="form-control-label">
                    Address :
                </label>

                <div class="row">
                    <div class="col-sm-8 col-md-8 col-lg-8">
                        <label class="form-control-label">
                                        Perum XXX kecamatan Krian, Sidoarjo
                                    </label>
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <a href="https://web.facebook.com/audhykun"><span  class="la la-facebook"></span>Facebook</a>
                    </div>
                </div>

            </div>
            <div class="m-portlet__body">
                <label class="form-control-label">
                    Phone :
                </label>

                <div class="row">
                    <div class="col-sm-8 col-md-8 col-lg-8">
                        <label class="form-control-label">
                                        081232353834
                                    </label>
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <a href="https://twitter.com/virabri"><span  class="la la-twitter"></span>Twitter</a>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

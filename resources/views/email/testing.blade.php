<style>
    h1{
        /*@editable*/ color:#202020 !important;
        display:block;
        /*@editable*/ font-family:Arial;
        /*@editable*/ font-size:34px;
        /*@editable*/ font-weight:bold;
        /*@editable*/ line-height:100%;
        /*@editable*/ text-align:left;
    }
    </style>
    
    <table border="0" cellpadding="0" cellspacing="0" width="100%" mc:repeatable="image_with_content" mc:variant="content with left image">
        <tr>
            {{-- <td align="center" valign="top"> --}}
                {{-- <img src="..." mc:edit="left_image" /> --}}
            {{-- </td> --}}
            <td valign="top">
                <div mc:edit="right_content">
                <h4 class="h4">Hai {{$email}} </h4>
                   We recive your question ->  {{$message_r}}<br> 
                   We think that {{$reply}} <br>
                   Thank you for your question
                </div>
            </td>
        </tr>
    </table>
    <!-- // End Module: Left Image with Right Content \\ -->
    
    <!-- // Begin Module: Right Image with Left Content \\ -->
    {{-- <table border="0" cellpadding="0" cellspacing="0" width="100%" mc:repeatable="image_with_content" mc:variant="content with right image">
        <tr>
            <td valign="top">
                <div mc:edit="left_content">
                   <h1 class="h4">Heading 1</h4>
                   Lorem ipsum dolor sit amet.
                </div>
            </td>
            <td align="center" valign="top">
                <img src="..." mc:edit="right_image" />
            </td>
        </tr>
    </table> --}}
    
    {{-- <div>
        Hi, This is : {{ $name }}
    </div> --}}
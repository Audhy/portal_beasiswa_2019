<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Internal_email-29</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<style type="text/css">
			* {
				-ms-text-size-adjust:100%;
				-webkit-text-size-adjust:none;
				-webkit-text-resize:100%;
				text-resize:100%;
			}
			a{
				outline:none;
				color:#40aceb;
				text-decoration:underline;
			}
			a:hover{text-decoration:none !important;}
			.nav a:hover{text-decoration:underline !important;}
			.title a:hover{text-decoration:underline !important;}
			.title-2 a:hover{text-decoration:underline !important;}
			.btn:hover{opacity:0.8;}
			.btn a:hover{text-decoration:none !important;}
			.btn{
				-webkit-transition:all 0.3s ease;
				-moz-transition:all 0.3s ease;
				-ms-transition:all 0.3s ease;
				transition:all 0.3s ease;
			}
			table td {border-collapse: collapse !important;}
			.ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div{line-height:inherit;}
			@media only screen and (max-width:500px) {
				table[class="flexible"]{width:100% !important;}
				table[class="center"]{
					float:none !important;
					margin:0 auto !important;
				}
				*[class="hide"]{
					display:none !important;
					width:0 !important;
					height:0 !important;
					padding:0 !important;
					font-size:0 !important;
					line-height:0 !important;
				}
				td[class="img-flex"] img{
					width:100% !important;
					height:auto !important;
				}
				td[class="aligncenter"]{text-align:center !important;}
				th[class="flex"]{
					display:block !important;
					width:100% !important;
				}
				td[class="wrapper"]{padding:0 !important;}
				td[class="holder"]{padding:30px 15px 20px !important;}
				td[class="nav"]{
					padding:20px 0 0 !important;
					text-align:center !important;
				}
				td[class="h-auto"]{height:auto !important;}
				td[class="description"]{padding:30px 20px !important;}
				td[class="i-120"] img{
					width:120px !important;
					height:auto !important;
				}
				td[class="footer"]{padding:5px 20px 20px !important;}
				td[class="footer"] td[class="aligncenter"]{
					line-height:25px !important;
					padding:20px 0 0 !important;
				}
				tr[class="table-holder"]{
					display:table !important;
					width:100% !important;
				}
				th[class="thead"]{display:table-header-group !important; width:100% !important;}
				th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}
			}
		</style>
	</head>
	<body style="margin:0; padding:0;" bgcolor="#eaeced">
		{{-- @php
		var_dump($scholarship);
		@endphp --}}

		<table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
			<!-- fix for gmail -->
			<tr>
				<td class="hide">
					<table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
						<tr>
							<td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="wrapper" style="padding:0 10px;">
					<!-- module 1 -->
					<!-- module 4 -->
					<table data-module="module-4" data-thumb="thumbnails/04.png" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td data-bgcolor="bg-module" bgcolor="#eaeced">
								<table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
									<tr>
										<td data-bgcolor="bg-block" class="holder" style="padding:64px 60px 10px;" bgcolor="#f9f9f9">
											<table width="100%" cellpadding="0" cellspacing="0">
												<tr>
													<td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:30px/33px Arial, Helvetica, sans-serif; color:#292c34; padding:0 0 23px;">
														Portal Beasiswa
													</td>
												</tr>
												<tr>
													<td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:16px/29px Arial, Helvetica, sans-serif; color:#888; padding:0 0 21px;">
														Berikut adalah informasi beasiswa terbaru
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td>
											<table width="100%" cellpadding="0" cellspacing="0">
												<tr>
													<th class="flex" width="300" align="left" style="vertical-align:top; padding:0;">
													<table width="100%" cellpadding="0" cellspacing="0">

@php
															$i = 0;
														@endphp
														@foreach($scholarship as $data)
														@if($i == 0)
														<!-- post -->
														<tr>
																{{-- <td class="img-flex"><img src="data:image/jpg;base64,{{ asset('images/img-03.jpg') }}" style="vertical-align:top;" width="300" height="203" alt="" /></td> --}}
															</tr>
															<tr>
																<td data-bgcolor="bg-inner-block-01" class="h-auto" height="297" bgcolor="#a4d3f3">
																	<table width="100%" cellpadding="0" cellspacing="0">
																		<tr>
																			<td class="description" style="padding:10px 40px 10px 70px;">
																				<table width="100%" cellpadding="0" cellspacing="0">
																					<tr>
																						<td data-color="title-2" data-size="size title-2" data-min="10" data-max="25" data-link-color="link title-2 color" data-link-style="text-decoration:none; color:#fff;" class="title-2" align="right" style="font:bold 15px/23px Arial, Helvetica, sans-serif; color:#fff; padding:0 0 6px;">
																							{!!$data->name!!}
																						</td>
																					</tr>
																					<tr>
																						<td data-color="text-2" data-size="size text-2" data-min="10" data-max="25" data-link-color="link text-2 color" data-link-style="font-weight:bold; text-decoration:underline; color:#fff;" align="right" style="font:15px/23px Arial, Helvetica, sans-serif; color:#fff;">
																							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														@elseif($i == 1)
																													<!-- post -->
															<tr>
																<td data-bgcolor="bg-inner-block-02" class="h-auto" height="297" bgcolor="#76b3dd">
																	<table width="100%" cellpadding="0" cellspacing="0">
																		<tr>
																			<td class="description" style="padding:10px 40px 10px 70px;">
																				<table width="100%" cellpadding="0" cellspacing="0">
																					<tr>
																						<td data-color="title-2" data-size="size title-2" data-min="10" data-max="25" data-link-color="link title-2 color" data-link-style="text-decoration:none; color:#fff;" class="title-2" align="right" style="font:bold 15px/23px Arial, Helvetica, sans-serif; color:#fff; padding:0 0 6px;">
																							{!!$data->name!!}
																						</td>
																					</tr>
																					<tr>
																						<td data-color="text-2" data-size="size text-2" data-min="10" data-max="25" data-link-color="link text-2 color" data-link-style="font-weight:bold; text-decoration:underline; color:#fff;" align="right" style="font:15px/23px Arial, Helvetica, sans-serif; color:#fff;">
																							{!!str_limit($data->description, 355,' ...')!!}
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														@elseif($i == 2)
																													<!-- post -->
															<tr>
																<td data-bgcolor="bg-inner-block-03" class="h-auto" height="300" bgcolor="#0071bd">
																	<table width="100%" cellpadding="0" cellspacing="0">
																		<tr>
																			<td class="description" style="padding:10px 70px 10px 40px;">
																				<table width="100%" cellpadding="0" cellspacing="0">
																					<tr>
																						<td data-color="title-2" data-size="size title-2" data-min="10" data-max="25" data-link-color="link title-2 color" data-link-style="text-decoration:none; color:#fff;" class="title-2" style="font:bold 15px/23px Arial, Helvetica, sans-serif; color:#fff; padding:0 0 6px;">
																							{!!$data->name!!}

																						</td>
																					</tr>
																					<tr>
																						<td data-color="text-2" data-size="size text-2" data-min="10" data-max="25" data-link-color="link text-2 color" data-link-style="font-weight:bold; text-decoration:underline; color:#fff;" style="font:15px/23px Arial, Helvetica, sans-serif; color:#fff;">
																							{!!str_limit($data->description, 355,' ...')!!}
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
															<tr>
																{{-- <td class="img-flex"><img src="data:image/jpg;base64,{{ asset('images/img-03.jpg') }}" style="vertical-align:top;" width="300" height="200" alt="" /></td> --}}
															</tr>

														@elseif($i == 3)
														<tr>
																<td data-bgcolor="bg-inner-block-04" class="h-auto" height="300" bgcolor="#333c4d">
																	<table width="100%" cellpadding="0" cellspacing="0">
																		<tr>
																			<td class="description" style="padding:10px 70px 10px 40px;">
																				<table width="100%" cellpadding="0" cellspacing="0">
																					<tr>
																						<td data-color="title-2" data-size="size title-2" data-min="10" data-max="25" data-link-color="link title-2 color" data-link-style="text-decoration:none; color:#fff;" class="title-2" style="font:bold 15px/23px Arial, Helvetica, sans-serif; color:#fff; padding:0 0 6px;">
																							{!!$data->name!!}

																						</td>
																					</tr>
																					<tr>
																						<td data-color="text-2" data-size="size text-2" data-min="10" data-max="25" data-link-color="link text-2 color" data-link-style="font-weight:bold; text-decoration:underline; color:#fff;" style="font:15px/23px Arial, Helvetica, sans-serif; color:#fff;">
																							{!!str_limit($data->description, 355,' ...')!!}
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
															<tr>
																{{-- <td class="img-flex"><img src="{{ asset('images/img-06.jpg') }}" style="vertical-align:top;" width="300" height="200" alt="" /></td> --}}
															</tr>
														@elseif($i == 4)
														<tr>
																<td data-bgcolor="bg-inner-block-03" class="h-auto" height="300" bgcolor="#0071bd">
																	<table width="100%" cellpadding="0" cellspacing="0">
																		<tr>
																			<td class="description" style="padding:10px 70px 10px 40px;">
																				<table width="100%" cellpadding="0" cellspacing="0">
																					<tr>
																						<td data-color="title-2" data-size="size title-2" data-min="10" data-max="25" data-link-color="link title-2 color" data-link-style="text-decoration:none; color:#fff;" class="title-2" style="font:bold 15px/23px Arial, Helvetica, sans-serif; color:#fff; padding:0 0 6px;">
																							{!!$data->name!!}

																						</td>
																					</tr>
																					<tr>
																						<td data-color="text-2" data-size="size text-2" data-min="10" data-max="25" data-link-color="link text-2 color" data-link-style="font-weight:bold; text-decoration:underline; color:#fff;" style="font:15px/23px Arial, Helvetica, sans-serif; color:#fff;">
																							{!!str_limit($data->description, 355,' ...')!!}
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
															<tr>
																{{-- <td class="img-flex"><img src="data:image/jpg;base64,{{ asset('images/img-03.jpg') }}" style="vertical-align:top;" width="300" height="200" alt="" /></td> --}}
															</tr>
														@elseif($i == 5)
																																											<!-- post -->
															<tr>
																<td data-bgcolor="bg-inner-block-03" class="h-auto" height="300" bgcolor="#0071bd">
																	<table width="100%" cellpadding="0" cellspacing="0">
																		<tr>
																			<td class="description" style="padding:10px 70px 10px 40px;">
																				<table width="100%" cellpadding="0" cellspacing="0">
																					<tr>
																						<td data-color="title-2" data-size="size title-2" data-min="10" data-max="25" data-link-color="link title-2 color" data-link-style="text-decoration:none; color:#fff;" class="title-2" style="font:bold 15px/23px Arial, Helvetica, sans-serif; color:#fff; padding:0 0 6px;">
																							{!!$data->name!!}

																						</td>
																					</tr>
																					<tr>
																						<td data-color="text-2" data-size="size text-2" data-min="10" data-max="25" data-link-color="link text-2 color" data-link-style="font-weight:bold; text-decoration:underline; color:#fff;" style="font:15px/23px Arial, Helvetica, sans-serif; color:#fff;">
																							{!!str_limit($data->description, 355,' ...')!!}
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
															<tr>
																{{-- <td class="img-flex"><img src="data:image/jpg;base64,{{ asset('images/img-03.jpg') }}" style="vertical-align:top;" width="300" height="200" alt="" /></td> --}}
															</tr>
														@elseif($i == 6)
																													<!-- post -->
															<tr>
																{{-- <td class="img-flex"><img src="data:image/jpg;base64,{{ asset('images/img-03.jpg') }}" style="vertical-align:top;" width="300" height="203" alt="" /></td> --}}
															</tr>
															<tr>
																<td data-bgcolor="bg-inner-block-02" class="h-auto" height="297" bgcolor="#76b3dd">
																	<table width="100%" cellpadding="0" cellspacing="0">
																		<tr>
																			<td class="description" style="padding:10px 40px 10px 70px;">
																				<table width="100%" cellpadding="0" cellspacing="0">
																					<tr>
																						<td data-color="title-2" data-size="size title-2" data-min="10" data-max="25" data-link-color="link title-2 color" data-link-style="text-decoration:none; color:#fff;" class="title-2" align="right" style="font:bold 15px/23px Arial, Helvetica, sans-serif; color:#fff; padding:0 0 6px;">
																							{!!$data->name!!}

																						</td>
																					</tr>
																					<tr>
																						<td data-color="text-2" data-size="size text-2" data-min="10" data-max="25" data-link-color="link text-2 color" data-link-style="font-weight:bold; text-decoration:underline; color:#fff;" align="right" style="font:15px/23px Arial, Helvetica, sans-serif; color:#fff;">
																							{!!str_limit($data->description, 355,' ...')!!}
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														@elseif($i == 7)
															<!-- post -->
															<tr>
																	{{-- <td class="img-flex"><img src="data:image/jpg;base64,{{ asset('images/img-03.jpg') }}" style="vertical-align:top;" width="300" height="203" alt="" /></td> --}}
																</tr>
																<tr>
																	<td data-bgcolor="bg-inner-block-01" class="h-auto" height="297" bgcolor="#a4d3f3">
																		<table width="100%" cellpadding="0" cellspacing="0">
																			<tr>
																				<td class="description" style="padding:10px 40px 10px 70px;">
																					<table width="100%" cellpadding="0" cellspacing="0">
																						<tr>
																							<td data-color="title-2" data-size="size title-2" data-min="10" data-max="25" data-link-color="link title-2 color" data-link-style="text-decoration:none; color:#fff;" class="title-2" align="right" style="font:bold 15px/23px Arial, Helvetica, sans-serif; color:#fff; padding:0 0 6px;">
																								{!!$data->name!!}

																							</td>
																						</tr>
																						<tr>
																							<td data-color="text-2" data-size="size text-2" data-min="10" data-max="25" data-link-color="link text-2 color" data-link-style="font-weight:bold; text-decoration:underline; color:#fff;" align="right" style="font:15px/23px Arial, Helvetica, sans-serif; color:#fff;">
																								{!!str_limit($data->description, 355,' ...')!!}
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
														@else
														<tr>
																<td data-bgcolor="bg-inner-block-03" class="h-auto" height="300" bgcolor="#0071bd">
																	<table width="100%" cellpadding="0" cellspacing="0">
																		<tr>
																			<td class="description" style="padding:10px 70px 10px 40px;">
																				<table width="100%" cellpadding="0" cellspacing="0">
																					<tr>
																						<td data-color="title-2" data-size="size title-2" data-min="10" data-max="25" data-link-color="link title-2 color" data-link-style="text-decoration:none; color:#fff;" class="title-2" style="font:bold 15px/23px Arial, Helvetica, sans-serif; color:#fff; padding:0 0 6px;">
																							{!!$data->name!!}

																						</td>
																					</tr>
																					<tr>
																						<td data-color="text-2" data-size="size text-2" data-min="10" data-max="25" data-link-color="link text-2 color" data-link-style="font-weight:bold; text-decoration:underline; color:#fff;" style="font:15px/23px Arial, Helvetica, sans-serif; color:#fff;">
																							{!!str_limit($data->description, 355,' ...')!!}
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
															<tr>
																{{-- <td class="img-flex"><img src="data:image/jpg;base64,{{ asset('images/img-03.jpg') }}" style="vertical-align:top;" width="300" height="200" alt="" /></td> --}}
															</tr>
														@endif
														{{-- <table width="100%" cellpadding="0" cellspacing="0"> --}}


													{{-- <th class="flex" width="300" align="left" style="vertical-align:top; padding:0;"> --}}
														{{-- <table width="100%" cellpadding="0" cellspacing="0"> --}}

															<!-- post -->
															@php
																$i++;
															@endphp
															@endforeach
														{{-- </table> --}}
													{{-- </th> --}}
												</table>
											</th>

												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td data-bgcolor="bg-block" class="holder" style="padding:30px 60px 50px;" bgcolor="#f9f9f9">
											<table width="100%" cellpadding="0" cellspacing="0">
												<tr>
													<td style="padding:0 0 20px;">
														<table width="134" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
															<tr>
																<td data-bgcolor="bg-button" data-size="size button" data-min="10" data-max="16" class="btn" align="center" style="font:12px/14px Arial, Helvetica, sans-serif; color:#f8f9fb; text-transform:uppercase; mso-padding-alt:12px 10px 10px; border-radius:2px;" bgcolor="#40aceb">
																	<a target="_blank" style="text-decoration:none; color:#f8f9fb; display:block; padding:12px 10px 10px;" href="http://206.189.145.239">More news</a>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr><td height="28"></td></tr>
								</table>
							</td>
						</tr>
					</table>
					<!-- module 5 -->
				</td>
			</tr>
			<!-- fix for gmail -->
			<tr>
				<td style="line-height:0;"><div style="display:none; white-space:nowrap; font:15px/1px courier;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></td>
			</tr>
		</table>
	</body>
</html>

<div class="modal fade" id="modal_configuration" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="TitleModalconfiguration">Configuration</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="ConfigurationForm">
                    <input type="hidden" class="form-control form-control-danger m-input" name="id" id="id">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row ">
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <label class="form-control-label">
                                    Name @include('common.label.required')
                                </label>

                                @component('common.input.text')
                                    @slot('id', 'name')
                                    @slot('text', 'Name')
                                    @slot('name', 'name')
                                    @slot('id_error', 'name')
                                @endcomponent
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <label class="form-control-label">
                                    value @include('common.label.required')
                                </label>

                                @component('common.input.number')
                                    @slot('id', 'value')
                                    @slot('text', 'Value')
                                    @slot('name', 'value')
                                    @slot('id_error', 'value')
                                @endcomponent
                            </div>
                        </div>
                        <div class="form-group m-form__group row ">
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <label class="form-control-label">
                                    Description @include('common.label.optional')
                                </label>

                                @component('common.input.textarea')
                                    @slot('rows', '4')
                                    @slot('id', 'description')
                                    @slot('name', 'description')
                                    @slot('text', 'Description')
                                    @slot('description', 'text')
                                    @slot('id_error', 'description')

                                @endcomponent
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div id="button-div">
                            @component('common.buttons.submit')
                                @slot('size', 'md')
                                @slot('class', 'add')
                            @endcomponent
                        </div>
                        @component('common.buttons.reset')
                            @slot('size', 'md')
                        @endcomponent

                        @component('common.buttons.close')
                            @slot('size', 'md')
                            @slot('data_dismiss', 'modal')
                        @endcomponent
                    </div>
                </form>

               </div>
            </div>
        </div>
    </div>

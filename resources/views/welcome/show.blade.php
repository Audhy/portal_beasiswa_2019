@extends('frontend')
@section('content')
<div id="content-div">
    <div class="row">
        <div class="col-md-4">
            <h3 class="m-widget19__title m--font-dark">
                {!!$scholarship->name!!}
            </h3>
            @if($scholarship->picture != null)
                <img src="{{ asset($scholarship->picture) }}" height="300px" alt="">
            @endif
        <br>
        </div>
        <div class="col-md-8">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-tools">
                        <ul class="nav nav-tabs m-tabs-line m-tabs-line--primary" role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_12_1" role="tab">
											<i class="la la-cog"></i> Descripton
										</a>
                            </li>
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link forum" data-toggle="tab" href="#m_tabs_12_2" role="tab" data-id="{{$scholarship->id}}">
											<i class="la la-briefcase"></i> Forum
										</a>
                            </li>
                        </ul>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a href="{{$scholarship->uri}}" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--pill m-btn--air">
                                        Orininali Link
                                    </a>
                            </li>


                            <li class="m-portlet__nav-item">
                                @if($scholarship->reg == null or $scholarship->reg == '')
                                <a href="{{$scholarship->reg}}" class="m-portlet__nav-link btn btn-success m-btn m-btn--hover-brand m-btn--pill m-btn--air disabled">
								<i ><span class="la la-send-o">Join</span></i>
							</a> @else
                                <a href="{{$scholarship->reg}}" class="m-portlet__nav-link btn btn-success m-btn m-btn--hover-brand m-btn--pill m-btn--air">
								<i ><span class="la la-send-o">Join</span></i>
							</a> @endif
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="m_tabs_12_1" role="tabpanel">
                            <div>
                                {!!$scholarship->description!!}
                            </div>
                            <div>
                                {!!$scholarship->additional!!}
                            </div>
                        </div>


                        <div class="tab-pane" id="m_tabs_12_2" role="tabpanel">


                            <div class="m-portlet__body">
                                <div id="div-chat" class="m-scrollable" data-scrollable="true" data-height="380" data-mobile-height="300">
                                    <div class="m-scrollable" data-scrollable="true" data-height="380" data-mobile-height="300" id="div-forum2">
                                        <!--Begin::Timeline 2 -->
                                        <div class="m-timeline-2">
                                            <div class="m-timeline-2__items  m--padding-top-25 m--padding-bottom-30" id="div-forum">

                                                {{--
                                                <div class="m-timeline-2__item m--margin-top-30">
                                                    <span class="m-timeline-2__item-time">12:45</span>
                                                    <div class="m-timeline-2__item-cricle">
                                                        <i class="fa fa-genderless m--font-success"></i>
                                                    </div>
                                                    <div class="m-timeline-2__item-text m-timeline-2__item-text--bold">
                                                        AEOL Meeting With
                                                    </div>
                                                </div> --}}
                                            </div>
                                            <div class="m-timeline-2__items  m--padding-top-25 m--padding-bottom-30" id="div-forum-footer"></div>


                                        </div>
                                        <!--End::Timeline 2 -->
                                    </div>
                                </div>

                                <div id="div-chat2">

                                </div>
                                <div class="chat" id="div-chat-footer"></div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 @push('footer-scripts')
<script src="{{ asset('js/welcome.js')}}"></script>


@endpush

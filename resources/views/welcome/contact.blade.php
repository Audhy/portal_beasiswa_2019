@extends('frontend')

@section('content')
<div id="content-div">
		{{-- <div class="container">
                <div class="col-md-12"> --}}

                	<!--begin::Portlet-->
		<div class="m-portlet m-portlet--responsive-mobile">
                <form method="POST" action="{{route('message.store')}}" class="form-horizontal">

                        {!! csrf_field() !!}
                        @if (session('pesan_sukses'))
                        <div class="alert alert-success">
                            {{ session('pesan_sukses') }}
                        </div>
                      @endif

                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="flaticon-placeholder-2 m--font-brand"></i>
                            </span>
                            <h3 class="m-portlet__head-text m--font-brand">
                                    Contact US
                                </h3>                        </div>			
                    </div>

                </div>
                <div class="m-portlet__body">
                    <div class="container">
                        <div class="row">
                                <div class="col-md-6">
                                        <div class="form-group m-form__group row ">
                                                <div class="col-sm-12 col-md-12 col-lg-12">
                                                    <label class="form-control-label">
                                                        Email 
                                                    </label>
                    
                                                    @component('common.input.email')
                                                        @slot('id', 'email')
                                                        @slot('text', 'Email')
                                                        @slot('name', 'email')
                                                        @slot('editable', 'required')
                                                    @endcomponent
                                                                    </div>
                                            </div>        
                                        <div class="form-group m-form__group row ">
                                                <div class="col-sm-12 col-md-12 col-lg-12">
                                                    <label class="form-control-label">
                                                        Message 
                                                    </label>
                    
                                                    @component('common.input.textarea')
                                                        @slot('rows', '3')
                                                        @slot('id', 'message')
                                                        @slot('name', 'message')
                                                        @slot('text', 'Message')
                                                        @slot('description', 'text')
                                                        @slot('editable', 'required')
                                                    @endcomponent
                                                </div>
                                            </div>        
                                </div>
                                <div class="col-md-6">
                                        <img src="img/pbfix.png" alt="logo" height="100px" style="margin-left:20%">
                                </div>
                            </div>
                    </div>
                </div>
                <div class="m-portlet__foot" >
								@component('common.buttons.submit')
									@slot('size', 'md')
                                    @slot('size', 'md')
                                    @slot('type', 'submit')
									@slot('style', 'margin-left:40%')
								@endcomponent
                </div>
            </form>
            </div>	
            <!--end::Portlet-->
        </div>
    
			{{-- </div>
		</div> --}}

@endsection
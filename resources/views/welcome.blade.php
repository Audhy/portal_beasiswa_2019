@extends('frontend')

@section('content')
<div id="content-div">
		<div class="container">
				<div class="row">
					@foreach($scholarships as $scholarship)
				  <div class="col-md-4">
						<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height  m-portlet--rounded-force">
								<div class="m-portlet__head m-portlet__head--fit">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-action">
											{{-- <button type="button" class="btn btn-sm m-btn--pill  btn-brand">Blog</button> --}}
										</div>
									</div>
								</div>
								<div class="m-portlet__body">
									<div class="m-widget19">
                                        @if($scholarship->picture != null)
                                        <div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides" style="min-height-: 286px">
                                            <img src="{{ asset($scholarship->picture) }}" height="300px" alt="">
                                        </div>

                                        @endif

										{{-- <div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides" style="min-height-: 286px">
                                                <img src="{{ asset('img/no_picture/1.jpg') }}" alt="">

                                            <img src="{{ asset($scholarship->picture) }}" alt="">
											<h3 class="m-widget19__title m--font-light">
												{!!$scholarship->name!!}
											</h3>
											<div class="m-widget19__shadow"></div>
										</div> --}}
										<div class="m-widget19__content">
											{{-- <div class="m-widget19__header">
												<div class="m-widget19__user-img">
													<img class="m-widget19__img" src="{{ asset('assets/metronic/app/media/img/users/user1.jpg') }}"
														alt="">
												</div>
												<div class="m-widget19__info">
													<span class="m-widget19__username">
														Anna Krox
													</span><br>
													<span class="m-widget19__time">
														UX/UI Designer, Google
													</span>
												</div>
												<div class="m-widget19__stats">
													<span class="m-widget19__number m--font-brand">
														18
													</span>
													<span class="m-widget19__comment">
														Comments
													</span>
												</div>
											</div> --}}
											<div class="m-widget19__body">
                                                <h3>{!!$scholarship->name!!}</h3>
												{!!str_limit($scholarship->description, 355,' ...')!!}
											</div>
										</div>
										<div class="m-widget19__action">
												<a href="{{route('show',$scholarship->id)}}" class="btn m-btn--pill btn-secondary m-btn m-btn--hover-brand m-btn--custom">Read
													More
												</a>
										</div>
									</div>
								</div>
							</div>
				</div>
				@endforeach




				</div>
				<div class="pull-right">{!! $scholarships->links() !!}</div>

			</div>
		</div>

@endsection

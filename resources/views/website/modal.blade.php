<div class="modal fade" id="modal_website"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="TitleModalWebsite">Website</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="WebsiteForm">
                    <input type="hidden" class="form-control form-control-danger m-input" name="id" id="id">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row ">
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <label class="form-control-label">
                                    Name @include('common.label.required')
                                </label>

                                @component('common.input.text')
                                    @slot('id', 'name')
                                    @slot('text', 'Name')
                                    @slot('name', 'name')
                                    @slot('id_error','name')
                                    {{-- @slot('editable', 'disabled') --}}
                                @endcomponent

                                    
                            </div>
                        </div>
                        <div class="form-group m-form__group row ">
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                        <label class="form-control-label">
                                            Url @include('common.label.required')
                                        </label>
        
                                        @component('common.input.text')
                                            @slot('id', 'url')
                                            @slot('text', 'Url')
                                            @slot('name', 'url')
                                            @slot('id_error', 'url')
                                        @endcomponent
                                    </div>        
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <label class="form-control-label">
                                    Type @include('common.label.required')
                                </label>
                                <br>
                                @component('common.input.radio')
                                    @slot('id', 'type2')
                                    @slot('name','type')
                                    @slot('text', 'Portal')
                                    @slot('value', '2')
                                @endcomponent
                                <br>
                                @component('common.input.radio')
                                    @slot('id', 'type3')
                                    @slot('name','type')
                                    @slot('text', 'Instansi')
                                    @slot('value', '3')
                                @endcomponent

                                <div class="form-control-feedback text-danger" id="type-error"></div>

                            </div>
                        </div>
                                </div>
                    </div>
                    <div class="modal-footer website">

                        <div id="button-div">
                            @component('common.buttons.submit')
                                @slot('size', 'md')
                                @slot('class', 'add')
                            @endcomponent
                        </div>
                        {{-- @component('common.buttons.try')
                        @endcomponent --}}

                        @component('common.buttons.reset')
                            @slot('size', 'md')
                        @endcomponent

                        @component('common.buttons.close')
                            @slot('size', 'md')
                            @slot('data_dismiss', 'modal')
                        @endcomponent
                    </div>
                </form>

               </div>
            </div>
        </div>

<div class="modal fade" id="modal_attribute"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="TitleModalWebsite">Attribute</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="WebsiteForm">
                    <input type="hidden" class="form-control form-control-danger m-input" name="id" id="id">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row ">

                                <div class="col-sm-6 col-md-6 col-lg-6">
                                        <label class="form-control-label">
                                            Child Of @include('common.label.optional')
                                        </label>
                                @component('common.input.select')
                                    @slot('id', 'attribute_id')
                                    @slot('text', 'attribute_id')
                                    @slot('name', 'attribute_id')
                                    @slot('style', 'width:100%')
                                    @slot('id_error','attribute_id')
                                @endcomponent
                            </div>

                            <div class="col-sm-6 col-md-6 col-lg-6">
                                    <label class="form-control-label">
                                        Website @include('common.label.required')
                                    </label>
                            @component('common.input.select')
                                @slot('id', 'website2')
                                @slot('text', 'Website Id')
                                @slot('name', 'website2')
                                @slot('style', 'width:100%')
                                @slot('id_error','website_id')
                            @endcomponent
                        </div>


                        </div>
                        <div class="form-group m-form__group row ">
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                        <label class="form-control-label">
                                            Name @include('common.label.required')
                                        </label>

                                        @component('common.input.text')
                                            @slot('id', 'name')
                                            {{-- @slot('text', 'Name') --}}
                                            @slot('name', 'name')
                                            @slot('id_error','name')
                                            {{-- @slot('editable', 'disabled') --}}
                                        @endcomponent


                                    </div>
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                        <label class="form-control-label">
                                            Url @include('common.label.required')
                                        </label>

                                        @component('common.input.text')
                                            @slot('id', 'url')
                                            {{-- @slot('text', 'Url') --}}
                                            @slot('name', 'url')
                                            @slot('id_error', 'url')
                                        @endcomponent
                                    </div>
                        </div>
                        <div class="form-group m-form__group row ">
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                        <label class="form-control-label">
                                            Attribute @include('common.label.optional')
                                        </label>

                                        @component('common.input.text')
                                            @slot('id', 'attribute1')
                                            @slot('name', 'attribute1')
                                            @slot('id_error', 'attribute1')
                                        @endcomponent

                            </div>

                            {{-- <div class="col-sm-6 col-md-6 col-lg-6">
                            <label class="form-control-label">
                                    Attribute 2 @include('common.label.optional')
                                </label>

                                @component('common.input.text')
                                    @slot('id', 'attribute2')
                                    @slot('name', 'attribute2')
                                    @slot('id_error', 'attribute2')
                                @endcomponent
                                @component('common.input.radio')
                                    @slot('id', 'try1')
                                    @slot('name','try')
                                    @slot('text', 'Try This')
                                    @slot('value', '1')
                                @endcomponent
                            </div> --}}

                        </div>
                        <div class="form-group m-form__group row ">
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                        <label class="form-control-label">
                                                Attribute Title @include('common.label.optional')
                                            </label>

                                            @component('common.input.text')
                                                @slot('id', 'title')
                                                @slot('name', 'title')
                                                @slot('id_error', 'title')
                                            @endcomponent
                                            @component('common.input.radio')
                                            @slot('id', 'try2')
                                                @slot('name','try')
                                                @slot('text', 'Try This')
                                                @slot('value', '2')
                                            @endcomponent

                                        </div>
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                <label class="form-control-label">
                                        Attribute Description @include('common.label.optional')
                                    </label>

                                    @component('common.input.text')
                                        @slot('id', 'description')
                                        @slot('name', 'description')
                                        @slot('id_error', 'description')
                                    @endcomponent
                                    @component('common.input.radio')
                                        @slot('id', 'try3')
                                        @slot('name','try')
                                        @slot('text', 'Try This')
                                        @slot('value', '3')
                                    @endcomponent

                                </div>

                            </div>
                            <div class="form-group m-form__group row ">
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                            <label class="form-control-label">
                                                    Attribute Picture @include('common.label.optional')
                                                </label>

                                                @component('common.input.text')
                                                    @slot('id', 'picture')
                                                    @slot('name', 'picture')
                                                @endcomponent
                                                @component('common.input.radio')
                                                    @slot('id', 'try4')
                                                    @slot('name','try')
                                                    @slot('text', 'Try This')
                                                    @slot('value', '4')
                                                @endcomponent

                                            </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                    <label class="form-control-label">
                                            Attribute Period @include('common.label.optional')
                                        </label>

                                        @component('common.input.text')
                                            @slot('id', 'period')
                                            @slot('name', 'period')
                                        @endcomponent
                                        @component('common.input.radio')
                                            @slot('id', 'try5')
                                            @slot('name','try')
                                            @slot('text', 'Try This')
                                            @slot('value', '5')
                                        @endcomponent

                                    </div>

                                </div>
                                <div class="form-group m-form__group row ">
                                        <div class="col-sm-6 col-md-6 col-lg-6">
                                                <label class="form-control-label">
                                                        Attribute Registration @include('common.label.optional')
                                                    </label>

                                                    @component('common.input.text')
                                                        @slot('id', 'registration')
                                                        @slot('name', 'registration')
                                                    @endcomponent
                                                    @component('common.input.radio')
                                                        @slot('id', 'try6')
                                                        @slot('name','try')
                                                        @slot('text', 'Try This')
                                                        @slot('value', '6')
                                                    @endcomponent

                                                </div>
                                                <div class="col-sm-6 col-md-6 col-lg-6">
                                                    <label class="form-control-label">
                                                            Attribute Addtional 1 @include('common.label.optional')
                                                        </label>

                                                        @component('common.input.text')
                                                            @slot('id', 'additional1')
                                                            @slot('name', 'additional1')
                                                        @endcomponent
                                                        @component('common.input.radio')
                                                            @slot('id', 'try7')
                                                            @slot('name','try')
                                                            @slot('text', 'Try This')
                                                            @slot('value', '7')
                                                        @endcomponent

                                                    </div>
                                            </div>
                                            <div class="form-group m-form__group row ">
                                                <div class="col-sm-6 col-md-6 col-lg-6">
                                                        <label class="form-control-label">
                                                             Attribute Addtional 2 @include('common.label.optional')
                                                            </label>

                                                            @component('common.input.text')
                                                                @slot('id', 'additional2')
                                                                @slot('name', 'additional2')
                                                            @endcomponent
                                                            @component('common.input.radio')
                                                                @slot('id', 'try8')
                                                                @slot('name','try')
                                                                @slot('text', 'Try This')
                                                                @slot('value', '8')
                                                            @endcomponent

                                                        </div>
                                                        <div class="col-sm-6 col-md-6 col-lg-6">
                                                            <label class="form-control-label">
                                                                    Attribute Addtional 3 @include('common.label.optional')
                                                                </label>

                                                                @component('common.input.text')
                                                                    @slot('id', 'additional3')
                                                                    @slot('name', 'additional3')
                                                                @endcomponent
                                                                @component('common.input.radio')
                                                                    @slot('id', 'try9')
                                                                    @slot('name','try')
                                                                    @slot('text', 'Try This')
                                                                    @slot('value', '9')
                                                                @endcomponent

                                                            </div>
                                                    </div>
                                                    <div class="form-group m-form__group row ">
                                                        <div class="col-sm-6 col-md-6 col-lg-6">
                                                                <label class="form-control-label">
                                                                    Attribute Addtional 4 @include('common.label.optional')
                                                                    </label>

                                                                    @component('common.input.text')
                                                                        @slot('id', 'additional4')
                                                                        @slot('name', 'additional4')
                                                                    @endcomponent
                                                                    @component('common.input.radio')
                                                                        @slot('id', 'try10')
                                                                        @slot('name','try')
                                                                        @slot('text', 'Try This')
                                                                        @slot('value', '10')
                                                                    @endcomponent

                                                                </div>
                                                                <div class="col-sm-6 col-md-6 col-lg-6">
                                                                    <label class="form-control-label">
                                                                            Attribute Addtional 5 @include('common.label.optional')
                                                                        </label>

                                                                        @component('common.input.text')
                                                                            @slot('id', 'additional5')
                                                                            @slot('name', 'additional5')
                                                                        @endcomponent
                                                                        @component('common.input.radio')
                                                                            @slot('id', 'try11')
                                                                            @slot('name','try')
                                                                            @slot('text', 'Try This')
                                                                            @slot('value', '11')
                                                                        @endcomponent

                                                                    </div>
                                                            </div>
                                </div>
                            </form>

                    </div>
                    <div class="modal-footer">
                        <div id="foo">
                                @include('common.buttons.submit')

                                @component('common.buttons.try')
                                    @slot('data_target', '#modal_try')
                                @endcomponent


                                @component('common.buttons.reset')
                                    @slot('size', 'md')
                                @endcomponent
                                @component('common.buttons.close')
                                    @slot('size', 'md')
                                    @slot('data_dismiss', 'modal')
                                @endcomponent

                        </div>

                </div>

               </div>
            </div>
        </div>

<?php

// Route::get('/scraper', 'ScaperController@index');
Route::get('/jaro', 'ScaperController@create');
Route::get('/year', 'ScaperController@year');

Route::get('/lpdp1', 'GraptController@lpdp1');

Route::post('/testing', 'GraptController@testing');

Route::post('/tes', 'GraptController@tes');

Route::get('/grapt', 'GraptController@ind');
Route::get('/grapt2', 'GraptController@info');
Route::get('/grapt3', 'GraptController@ayo');
Route::get('/grapt4', 'GraptController@top');
Route::get('/grapt5', 'GraptController@id');
Route::get('/grapt6', 'GraptController@plus');
Route::get('/grapt7', 'GraptController@lpdp');
Route::get('/grapt8', 'GraptController@unggulan');
Route::get('/grapt9', 'GraptController@bukalapak');
Route::get('/grapt10', 'GraptController@sampoerna');

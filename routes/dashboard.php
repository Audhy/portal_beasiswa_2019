<?php
Route::get('/suggestion', 'HomeController@suggestions')->name('suggestion');
Route::get('/message-today', 'MessageController@messagesToday')->name('message-today');
Route::get('/message-all', 'MessageController@messagesAll')->name('message-all');
Route::get('/message-reply/{id}', 'MessageController@message')->name('message.reply');

Route::get('/grapt-log', 'HomeController@graptLogs')->name('grapt-log');
<?php
Route::get('update-button', function(){
    return View::make('common.buttons.update')
    ->render();

});

Route::get('submit-button', function(){
    return View::make('common.buttons.submit')
    ->render();

});

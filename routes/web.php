<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
// require_once('frontend.php');
require_once('grapt.php');
require_once('component.php');
// require_once('auth.php');

Route::group(['middleware'=>['auth']],function() {

    require_once('dashboard.php');


    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/log', 'HomeController@log')->name('log');
    Route::get('/suggestion', 'HomeController@suggestion')->name('suggestion');
    Route::get('/ignore/{website}', 'HomeController@ignore')->name('ignore');

    Route::resource('/administrator', 'AdministratorController');
    Route::get('/get-administrators', 'AdministratorController@getAdministrators')->name('get-administrators');
    Route::resource('/configuration', 'ConfigurationController');
    Route::get('/get-configurations', 'ConfigurationController@getConfigurations')->name('get-configurations');
    Route::resource('/scholarship', 'ScholarshipController');
    Route::post('/scholarship-edit/{scholarship}', 'ScholarshipController@updateData')->name('update-data.scholarship-edit');
    Route::get('/period/{scholarship}/edit/', 'ScholarshipController@periodEdit')->name('edit.period');

    Route::get('/get-scholarships', 'ScholarshipController@getScholarships')->name('get-scholarships');
    Route::get('/subscriber', 'SubscriberController@index')->name('subscriber.index');
    Route::delete('/subscriber/{subscriber}', 'SubscriberController@destroy')->name('subscriber.destroy');
    Route::delete('/forum/{forum}', 'ForumController@destroy')->name('forum.destroy');
    Route::get('/get-forum', 'ForumController@getForums')->name('get-forum');
    Route::delete('/chat/{chat}', 'ChatController@destroy')->name('chat.destroy');
    Route::get('/get-chat/{forum_id}', 'ChatController@getChats')->name('get-chat');
    Route::get('/get-subscribers', 'SubscriberController@getSubscribers')->name('get-subscribers');
    Route::resource('/website', 'WebsiteController');
    Route::get('/get-websites', 'WebsiteController@getWebsites')->name('get-websites');

    Route::put('/message/{message}', 'MessageController@update')->name('message.put');
    Route::get('/message/{message}/edit', 'MessageController@edit')->name('message.edit');
    Route::get('get-fill-websites', 'WebsiteController@fillWebsite')->name('get-fill-websites');
    Route::get('get-fill-attributes', 'ScholarshipAttributeController@fillScholarshipAttribute')->name('get-fill-attributes');


    Route::resource('/scholarship-attribute', 'ScholarshipAttributeController');
    Route::get('/get-scholarshipattribute', 'ScholarshipAttributeController@getScholarshipAttribute')->name('get-scholarshipattribute');



    Route::get('/send/email', 'HomeController@mail');

    Route::get('/scraper-suggestion', 'ScaperController@google');
    Route::get('/year', 'ScaperController@year');
    Route::get('/scraper-web', 'ScaperController@website');
    Route::get('/subscriber-mail', 'SubscriberController@subscriberMail');


    });

    Route::view('/reply', 'email.name')->name('reply');
    Route::post('/reply', 'MessageController@reply')->name('reply');



    Route::get('/portal-source', 'WelcomeController@portalSource')->name('portal-source');
    Route::get('/quick-search/{query}', 'WelcomeController@quickSearch')->name('quick-search');

    Route::get('/instance-source', 'WelcomeController@instanceSource')->name('instance-source');
    Route::get('/wel/{scholarship}', 'WelcomeController@show')->name('show');
    Route::post('/subscriber', 'SubscriberController@store')->name('subscriber.store');

    Route::resource('/chat', 'ChatController')->except(['destroy']);;
    Route::get('/chat-wel/{chat}', 'ChatController@chatWel')->name('chat-wel');
    Route::resource('/forum', 'ForumController')->except(['destroy']);;
    Route::get('/forum-wel/{forum}', 'ForumController@forumWel')->name('forum-wel');
    Route::get('/message', 'MessageController@index')->name('message.index');
    Route::post('/message', 'MessageController@store')->name('message.store');
    Route::get('/', 'WelcomeController@index')->name('index');
    Route::get('show/{scholarship}', 'WelcomeController@show')->name('show');

<?php
Route::get('/portal-source', 'WelcomeController@portalSource')->name('portal-source');
Route::get('/instance-source', 'WelcomeController@instanceSource')->name('instance-source');
Route::get('/wel/{scholarship}', 'WelcomeController@show')->name('show');
Route::post('/subscriber', 'SubscriberController@store')->name('subscriber.store');

Route::resource('/chat', 'ChatController')->except(['destroy']);;
Route::get('/chat-wel/{chat}', 'ChatController@chatWel')->name('chat-wel');
Route::resource('/forum', 'ForumController')->except(['destroy']);;
Route::get('/forum-wel/{forum}', 'ForumController@forumWel')->name('forum-wel');
Route::resource('/topic', 'TopicController');
Route::get('/message', 'MessageController@index')->name('message.index');
Route::post('/message', 'MessageController@store')->name('message.store');
Route::get('/', 'WelcomeController@index')->name('index');
Route::get('show/{scholarship}', 'WelcomeController@show')->name('show');

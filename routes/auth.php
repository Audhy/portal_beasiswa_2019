<?php
Route::group(['middleware'=>['auth']],function() {

    require_once('dashboard.php');
    
    
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/ignore/{website}', 'HomeController@ignore')->name('ignore');
    
    Route::resource('/administrator', 'AdministratorController');
    Route::get('/get-administrators', 'AdministratorController@getAdministrators')->name('get-administrators');
    Route::resource('/configuration', 'ConfigurationController');
    Route::get('/get-configurations', 'ConfigurationController@getConfigurations')->name('get-configurations');
    Route::resource('/scholarship', 'ScholarshipController');
    Route::get('/get-scholarships', 'ScholarshipController@getScholarships')->name('get-scholarships');
    Route::get('/subscriber', 'SubscriberController@index')->name('subscriber.index');
    Route::delete('/subscriber/{subscriber}', 'SubscriberController@destroy')->name('subscriber.destroy');
    Route::delete('/forum/{forum}', 'ForumController@destroy')->name('forum.destroy');
    Route::get('/get-forum', 'ForumController@getForums')->name('get-forum');
    Route::delete('/chat/{chat}', 'ChatController@destroy')->name('chat.destroy');
    Route::get('/get-chat/{forum_id}', 'ChatController@getChats')->name('get-chat');
    Route::get('/get-subscribers', 'SubscriberController@getSubscribers')->name('get-subscribers');
    Route::resource('/website', 'WebsiteController');
    Route::get('/get-websites', 'WebsiteController@getWebsites')->name('get-websites');
    Route::put('/message/{message}', 'MessageController@update')->name('message.put');
    Route::get('/message/{message}/edit', 'MessageController@edit')->name('message.edit');
    Route::get('get-fill-websites', 'WebsiteController@fillWebsite')->name('get-fill-websites');


    Route::resource('/scholarship-attribute', 'ScholarshipAttributeController');
    Route::get('/get-scholarshipattribute', 'ScholarshipAttributeController@getScholarshipAttribute')->name('get-scholarshipattribute');

    
    
    Route::get('/send/email', 'HomeController@mail');
    
    });
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScholarshipAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scholarship_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('attribute_id')->nullable();
            $table->unsignedInteger('website_id')->nullable();
            $table->string('name')->nullable();
            $table->string('url')->nullable();
            $table->string('attribute1')->nullable();
            // $table->string('attribute2')->nullable();
            $table->string('attribute_title')->nullable();
            $table->string('attribute_picture')->nullable();
            $table->string('attribute_description')->nullable();
            $table->string('attribute_registration')->nullable();
            $table->string('attribute_period')->nullable();
            $table->string('attribute_additional_1')->nullable();
            $table->string('attribute_additional_2')->nullable();
            $table->string('attribute_additional_3')->nullable();
            $table->string('attribute_additional_4')->nullable();
            $table->string('attribute_additional_5')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('attribute_id')
            ->references('id')->on('scholarship_attributes')
            ->onUpdate('cascade')
            ->onDelete('restrict');

            $table->foreign('website_id')
            ->references('id')->on('websites')
            ->onUpdate('cascade')
            ->onDelete('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scholarship_attributes');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->increments('id');
            $table->text('email');
            $table->text('chat');
            $table->unsignedInteger('forum_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('forum_id')
            ->references('id')->on('forums')
            ->onUpdate('cascade')
            ->onDelete('restrict');          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chats');
    }
}

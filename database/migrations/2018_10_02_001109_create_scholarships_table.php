<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScholarshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scholarships', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('picture')->nullable();
            $table->text('description')->nullable();
            $table->string('uri');
            $table->string('reg')->nullable();
            $table->longtext('additional')->nullable();
            $table->unsignedInteger('website_id');
            $table->unsignedInteger('attribute_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('website_id')
            ->references('id')->on('websites')
            ->onUpdate('cascade')
            ->onDelete('restrict');

            $table->foreign('attribute_id')
            ->references('id')->on('scholarship_attributes')
            ->onUpdate('cascade')
            ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scholarships');
    }
}

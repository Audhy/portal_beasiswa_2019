<?php

use Illuminate\Database\Seeder;

class WebsiteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('websites')->insert([
            'name' => 'lpdp',
            'status' => '3',
            'url' => 'https://www.lpdp.kemenkeu.go.id/',
        ]);

        DB::table('websites')->insert([
            'name' => 'Djarum Beasiswa Plus',
            'status' => '3',
            'url' => 'https://djarumbeasiswaplus.org/',
        ]);

        DB::table('websites')->insert([
            'name' => 'BCA',
            'status' => '3',
            'url' => 'https://www.bca.co.id/id/tentang-bca/korporasi/csr/solusi-cerdas-bca/',
        ]);

        DB::table('websites')->insert([
            'name' => 'Dikti',
            'status' => '3',
            'url' => 'http://beasiswa.ristekdikti.go.id',
        ]);

        DB::table('websites')->insert([
            'name' => 'IndBeasiswa',
            'status' => '2',
            'url' => 'https://indbeasiswa.com/',
        ]);

        DB::table('websites')->insert([
            'name' => 'Topkarir',
            'status' => '2',
            'url' => 'https://www.topkarir.com/beasiswa',
        ]);

    }
}

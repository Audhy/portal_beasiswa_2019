<?php

use Illuminate\Database\Seeder;

class ConfigurationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configurations')->insert([
            'name' => 'google sugestion',
            'description' => 'threshold of sraping google',
            'value' => '0.8',
            'of' => 'threshold',
        ]);
        DB::table('configurations')->insert([
            'name' => 'scholarship portal website',
            'description' => 'threshold of sraping web portal',
            'value' => '0.95',
            'of' => 'threshold',
        ]);
        DB::table('configurations')->insert([
            'name' => 'scholarship institute website',
            'description' => 'threshold of sraping intitute web',
            'value' => '0.95',
            'of' => 'threshold',
        ]);

    }
}

<?php

use Illuminate\Database\Seeder;

class AttributeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('scholarship_attributes')->insert([
        //     'website_id' => '',
        //     'name' => '',
        //     'url' => '',
        //     'attribute1' => '',
        //     'attribute_title' => '',
        //     'attribute_picture' => '',
        //     'attribute_description' => '',
        //     'attribute_registration' => '',
        //     'attribute_period' => '',
        // ]);

        DB::table('scholarship_attributes')->insert([
            'website_id' => '1',
            'name' => 'BEASISWA REGULER LPDP',
            'url' => 'https://www.lpdp.kemenkeu.go.id/beasiswa-reguler/',
            'attribute1' => '',
            'attribute_title' => '.h2',
            'attribute_picture' => '',
            'attribute_description' => 'div .wpb_text_column li',
            'attribute_registration' => '',
            'attribute_period' => '',
            'attribute_additional_1'=> '',
            'attribute_additional_2'=> '',
            'attribute_additional_3'=> '',
            'attribute_additional_4'=> '',
            'attribute_additional_5'=> '',
        ]);

        DB::table('scholarship_attributes')->insert([
            'website_id' => '1',
            'name' => 'BEASISWA AFIRMASI',
            'url' => 'https://www.lpdp.kemenkeu.go.id/beasiswa-afirmasi/',
            'attribute1' => '',
            'attribute_title' => '.h2',
            'attribute_picture' => '',
            'attribute_description' => 'div .wpb_text_column li',
            'attribute_registration' => '',
            'attribute_period' => '',
            'attribute_additional_1'=> '',
            'attribute_additional_2'=> '',
            'attribute_additional_3'=> '',
            'attribute_additional_4'=> '',
            'attribute_additional_5'=> '',
        ]);

        DB::table('scholarship_attributes')->insert([
            'website_id' => '1',
            'name' => 'BEASISWA AFIRMASI PNS, TNI, DAN POLRI',
            'url' => 'https://www.lpdp.kemenkeu.go.id/beasiswa-afirmasi-pns-tni-dan-polri/',
            'attribute1' => '',
            'attribute_title' => '.h2',
            'attribute_picture' => '',
            'attribute_description' => 'div .wpb_text_column li',
            'attribute_registration' => '',
            'attribute_period' => '',
            'attribute_additional_1'=> '',
            'attribute_additional_2'=> '',
            'attribute_additional_3'=> '',
            'attribute_additional_4'=> '',
            'attribute_additional_5'=> '',
        ]);

        DB::table('scholarship_attributes')->insert([
            'website_id' => '1',
            'name' => 'BEASISWA PENDIDIKAN INDONESIA DOKTER SPESIALIS',
            'url' => 'https://www.lpdp.kemenkeu.go.id/beasiswa-pendidikan-indonesia-dokter-spesialis/',
            'attribute1' => '',
            'attribute_title' => '.h2',
            'attribute_picture' => '',
            'attribute_description' => 'div .wpb_text_column li',
            'attribute_registration' => '',
            'attribute_period' => '',
            'attribute_additional_1'=> '',
            'attribute_additional_2'=> '',
            'attribute_additional_3'=> '',
            'attribute_additional_4'=> '',
            'attribute_additional_5'=> '',
        ]);

        DB::table('scholarship_attributes')->insert([
            'website_id' => '1',
            'name' => 'BEASISWA DISERTASI',
            'url' => 'https://www.lpdp.kemenkeu.go.id/beasiswa-disertasi/',
            'attribute1' => '',
            'attribute_title' => '.h2',
            'attribute_picture' => '',
            'attribute_description' => 'div .wpb_text_column li',
            'attribute_registration' => '',
            'attribute_period' => '',
            'attribute_additional_1'=> '',
            'attribute_additional_2'=> '',
            'attribute_additional_3'=> '',
            'attribute_additional_4'=> '',
            'attribute_additional_5'=> '',
        ]);

        DB::table('scholarship_attributes')->insert([
            'website_id' => '1',
            'name' => 'BUDI',
            'url' => 'https://www.lpdp.kemenkeu.go.id/budi/',
            'attribute1' => '',
            'attribute_title' => '.h2',
            'attribute_picture' => '',
            'attribute_description' => 'div .wpb_text_column li',
            'attribute_registration' => '',
            'attribute_period' => '',
            'attribute_additional_1'=> '',
            'attribute_additional_2'=> '',
            'attribute_additional_3'=> '',
            'attribute_additional_4'=> '',
            'attribute_additional_5'=> '',
        ]);

        DB::table('scholarship_attributes')->insert([
            'website_id' => '1',
            'name' => 'BEASISWA SANTRI',
            'url' => 'https://www.lpdp.kemenkeu.go.id/beasiswa-santri/',
            'attribute1' => '',
            'attribute_title' => '.h2',
            'attribute_picture' => '',
            'attribute_description' => 'div .wpb_text_column li',
            'attribute_registration' => '',
            'attribute_period' => '',
            'attribute_additional_1'=> '',
            'attribute_additional_2'=> '',
            'attribute_additional_3'=> '',
            'attribute_additional_4'=> '',
            'attribute_additional_5'=> '',
        ]);

        DB::table('scholarship_attributes')->insert([
            'website_id' => '1',
            'name' => 'BEASISWA INDONESIA TIMUR',
            'url' => 'https://www.lpdp.kemenkeu.go.id/beasiswa-indonesia-timur/',
            'attribute1' => '',
            'attribute_title' => '.h2',
            'attribute_picture' => '',
            'attribute_description' => 'div .wpb_text_column li',
            'attribute_registration' => '',
            'attribute_period' => '',
            'attribute_additional_1'=> '',
            'attribute_additional_2'=> '',
            'attribute_additional_3'=> '',
            'attribute_additional_4'=> '',
            'attribute_additional_5'=> '',
        ]);

        DB::table('scholarship_attributes')->insert([
            'website_id' => '1',
            'name' => 'PROGRAM CO-FUNDING',
            'url' => 'https://www.lpdp.kemenkeu.go.id/program-co-funding/',
            'attribute1' => '',
            'attribute_title' => '.h2',
            'attribute_picture' => '',
            'attribute_description' => 'div .wpb_text_column li',
            'attribute_registration' => '',
            'attribute_period' => '',
            'attribute_additional_1'=> '',
            'attribute_additional_2'=> '',
            'attribute_additional_3'=> '',
            'attribute_additional_4'=> '',
            'attribute_additional_5'=> '',
        ]);

        DB::table('scholarship_attributes')->insert([
            'website_id' => '3',
            'name' => 'Program Pendidikan Akuntansi (PPA) Non Gelar',
            'url' => 'https://www.bca.co.id/id/tentang-bca/korporasi/csr/solusi-cerdas-bca/program-pendidikan-akuntansi-non-gelar',
            'attribute1' => '',
            'attribute_title' => '.row p strong span',
            'attribute_picture' => '',
            'attribute_description' => '.content p',
            'attribute_registration' => '.content span a',
            'attribute_period' => '',
            'attribute_additional_1'=> '',
            'attribute_additional_2'=> '',
            'attribute_additional_3'=> '',
            'attribute_additional_4'=> '',
            'attribute_additional_5'=> '',
        ]);

        DB::table('scholarship_attributes')->insert([
            'website_id' => '3',
            'name' => 'Program Pendidikan Tteknik Informatika (PPTI) Non Gelar',
            'url' => 'https://www.bca.co.id/id/Tentang-BCA/Korporasi/csr/solusi-cerdas-bca/program-pendidikan-ti-non-gelar',
            'attribute1' => '',
            'attribute_title' => '.row p strong',
            'attribute_picture' => '',
            'attribute_description' => '.content p',
            'attribute_registration' => '',
            'attribute_period' => '',
            'attribute_additional_1'=> '',
            'attribute_additional_2'=> '',
            'attribute_additional_3'=> '',
            'attribute_additional_4'=> '',
            'attribute_additional_5'=> '',
        ]);

        DB::table('scholarship_attributes')->insert([
            'website_id' => '4',
            'name' => 'Beasiswa Pendidikan Pascasarjana Dalam Negeri',
            'url' => 'http://beasiswa.dikti.go.id/bppdn/',
            'attribute1' => '',
            'attribute_title' => '',
            'attribute_picture' => '',
            'attribute_description' => '.panel-body p',
            'attribute_registration' => '',
            'attribute_period' => '',
            'attribute_additional_1'=> '.col-lg-5',
            'attribute_additional_2'=> '',
            'attribute_additional_3'=> '',
            'attribute_additional_4'=> '',
            'attribute_additional_5'=> '',
        ]);

        DB::table('scholarship_attributes')->insert([
            'website_id' => '4',
            'name' => 'Beasiswa Pendidikan Pascasarjana Luar Negeri',
            'url' => 'http://beasiswa.ristekdikti.go.id/bppln/',
            'attribute1' => '',
            'attribute_title' => '',
            'attribute_picture' => '',
            'attribute_description' => '.panel-body p',
            'attribute_registration' => '',
            'attribute_period' => '',
            'attribute_additional_1'=> '.col-lg-5',
            'attribute_additional_2'=> '',
            'attribute_additional_3'=> '',
            'attribute_additional_4'=> '',
            'attribute_additional_5'=> '',
        ]);

        DB::table('scholarship_attributes')->insert([
            'website_id' => '2',
            'name' => 'DJARUM BEASISWA PLUS',
            'url' => 'https://djarumbeasiswaplus.org/tentang_kami/tentang-djarum-beasiswa-plus',
            'attribute1' => '',
            'attribute_title' => '.tab-content h1',
            'attribute_picture' => '',
            'attribute_description' => '.tab-content p',
            'attribute_registration' => '',
            'attribute_period' => '',
            'attribute_additional_1'=> '',
            'attribute_additional_2'=> '',
            'attribute_additional_3'=> '',
            'attribute_additional_4'=> '',
            'attribute_additional_5'=> '',
        ]);

        DB::table('scholarship_attributes')->insert([
            'attribute_id' => '14',
            'website_id' => '2',
            'url' => 'https://djarumbeasiswaplus.org/tentang_kami/persyaratan-untuk-menjadi-penerima-program-djarum-beasiswa-plus-tahun-2018-2019',
            'attribute1' => '',
            'attribute_title' => '',
            'attribute_picture' => '',
            'attribute_description' => '',
            'attribute_registration' => '',
            'attribute_period' => '',
            'attribute_additional_1'=> '.tab-content p',
            'attribute_additional_2'=> '',
            'attribute_additional_3'=> '',
            'attribute_additional_4'=> '',
            'attribute_additional_5'=> '',
        ]);

        DB::table('scholarship_attributes')->insert([
            'website_id' => '5',
            'name' => '',
            'url' => 'https://indbeasiswa.com/beasiswa-s1',
            'attribute1' => 'h2 a',
            'attribute_title' => '.post-top h1',
            'attribute_picture' => '',
            'attribute_description' => '.entry-content p',
            'attribute_registration' => '',
            'attribute_period' => '',
            'attribute_additional_1'=> '',
            'attribute_additional_2'=> '',
            'attribute_additional_3'=> '',
            'attribute_additional_4'=> '',
            'attribute_additional_5'=> '',
        ]);

        DB::table('scholarship_attributes')->insert([
            'website_id' => '6',
            'name' => '',
            'url' => 'https://www.topkarir.com/beasiswa',
            'attribute1' => '.title a',
            'attribute_title' => '.title span',
            'attribute_picture' => '',
            'attribute_description' => '.detail p',
            'attribute_registration' => '',
            'attribute_period' => '',
            'attribute_additional_1'=> '',
            'attribute_additional_2'=> '',
            'attribute_additional_3'=> '',
            'attribute_additional_4'=> '',
            'attribute_additional_5'=> '',

        ]);


    }
}
